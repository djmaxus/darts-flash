// #ifdef PYBIND11_ENABLED
#include <pybind11/pybind11.h>
#include <pybind11/stl_bind.h>
#include <pybind11/stl.h>

#include "dartsflash/flash/flash.hpp"
#include "dartsflash/flash/analysis.hpp"

namespace py = pybind11;

template <class FlashBase = Flash> class PyFlash : public FlashBase {
public:
    /* Inherit the constructors */
    using FlashBase::FlashBase;

    /* Trampoline (need one for each virtual function) */
    int evaluate(double p, double T, std::vector<double>& z_) override {
        PYBIND11_OVERRIDE_PURE(
            int,  /* Return type */
            Flash, /* Parent class */
            evaluate,   /* Name of function in C++ (must match Python name) */
            p, T, z_    /* Argument(s) */
        );
    }
};

void pybind_flash(py::module& m) 
{
    using namespace pybind11::literals;  // bring in '_a' literal

    py::class_<Flash, PyFlash<>> flash(m, "Flash", R"pbdoc(
            This is a base class for Flash.

            Each Flash child class overrides the methods for `evaluate(p, T, z)`.
            )pbdoc");
    
    flash.def(py::init<FlashParams&, int>(), R"pbdoc(
            This is the constructor of the EoS base class for multicomponent phases.

            :param flashparams: Flash parameters object
            :type flashparams: FlashParams
            :param np_max: Maximum number of phases, default is 6
            :type np_max: int
            )pbdoc", py::arg("flashparams"), py::arg("np_max")=6)
        
        .def("evaluate", &Flash::evaluate, R"pbdoc(
            Evaluate flash at (P, T, z)

            :param p: Pressure
            :type p: double
            :param T: Temperature
            :type T: double
            :param z: Feed composition
            :type z: list
            :returns: Error output of flash procedure
            :rtype: int
            )pbdoc", "p"_a, "T"_a, "z"_a)
        
        .def("get_flash_results", &Flash::get_flash_results, R"pbdoc(
            :returns: Results of flash
            :rtype: Flash::Results
            )pbdoc")
        .def("getnu", &Flash::getnu)
        .def("getx", &Flash::getx)
        // using array = py::array_t<double, pybind11::array::c_style | pybind11::array::forcecast>;
        ;

    py::class_<Flash::Results>(flash, "Results")
        .def(py::init<std::vector<double>&, std::vector<double>&, std::vector<std::string>&>())
        .def_property_readonly("nu", &Flash::Results::get_phase_fractions)
        .def_property_readonly("X", &Flash::Results::get_phase_compositions)
        .def_property_readonly("eos", &Flash::Results::get_eos)
        ;

    py::class_<NegativeFlash, PyFlash<NegativeFlash>, Flash>(m, "NegativeFlash", R"pbdoc(
            This is the two-phase implementation of negative flash.

            It evaluates two-phase negative flash and returns single-phase composition if either phase fraction is negative.
            )pbdoc")
        .def(py::init<FlashParams&, const std::vector<std::string>&, const std::vector<int>&>(), R"pbdoc(
            This is the constructor of the NegativeFlash class.

            :param flashparams: Flash parameters object
            :type flashparams: FlashParams
            :param eos_used: List of EoS names
            :type eos_used: list
            :param initial_guesses: List of initial guesses for K-values
            :type initial_guesses: list
            )pbdoc", "flashparams"_a, "eos_used"_a, "initial_guesses"_a)
        ;

    py::class_<StabilityFlash, PyFlash<StabilityFlash>, Flash>(m, "StabilityFlash", R"pbdoc(
            This is the N-phase implementation of the sequential stability test-phase split algorithm.

            It will repeat the sequence until each of the phases is found to be stable.
            )pbdoc")
        .def(py::init<FlashParams&, int>(), R"pbdoc(
            This is the constructor of the StabilityFlash class.

            :param flashparams: Flash parameters object
            :type flashparams: FlashParams
            :param np_max: Maximum number of phases, default is 6
            :type np_max: int
            )pbdoc", py::arg("flashparams"), py::arg("np_max")=6)
        ;

    py::class_<FlashParams> flash_params(m, "FlashParams", R"pbdoc(
            This is a class that contains all required parameters that can be passed to a flash algorithm.
            )pbdoc");

    flash_params.def(py::init<CompData&>(), R"pbdoc(
            :param comp_data: Component data object
            :type comp_data: CompData
            )pbdoc", "comp_data"_a)

        // Flash-related parameters
        .def_readwrite("min_z", &FlashParams::min_z, "Minimum value for composition")
        .def_readwrite("y_pure", &FlashParams::y_pure, "Mole fraction for preferred EoS range")

        .def_readwrite("rr2_tol", &FlashParams::rr2_tol, "Tolerance for two-phase Rachford-Rice norm")
        .def_readwrite("rrn_tol", &FlashParams::rrn_tol, "Tolerance for N-phase Rachford-Rice norm")
        .def_readwrite("rr_max_iter", &FlashParams::rr_max_iter, "Maximum number of iterations for Rachford-Rice procedure")

        .def_readwrite("split_tol", &FlashParams::split_tol, "Tolerance for phase split norm")
        .def_readwrite("split_switch_tol", &FlashParams::split_switch_tol, "Tolerance for switch to Newton in phase split")
        .def_readwrite("split_line_tol", &FlashParams::split_line_tol, "Tolerance for line search in phase split")
        .def_readwrite("split_max_iter", &FlashParams::split_max_iter, "Maximum number of iterations for phase split")
        .def_readwrite("split_line_iter", &FlashParams::split_line_iter, "Maximum number of iterations for line search in phase split")
        .def_readwrite("split_variables", &FlashParams::split_variables, "Variables for phase split: 0) n_ik, 1) lnK, 2) lnK-chol")
        
        .def_readwrite("stability_variables", &FlashParams::stability_variables, "Variables for stability: 0) Y, 1) lnY, 2) alpha")
        .def_readwrite("tpd_tol", &FlashParams::tpd_tol, "Tolerance for comparing tpd; also used to determine limit for tpd that is considered stable")
        .def_readwrite("comp_tol", &FlashParams::comp_tol, "Tolerance for comparing compositions")
        
        .def_readwrite("verbose", &FlashParams::verbose, "Verbose level")

        // EoS-related parameters
        .def_readwrite("eos_params", &FlashParams::eos_params, "Map of EoSParams object associated with each EoS object")
        .def("add_eos", &FlashParams::add_eos, R"pbdoc(
            Add EoSParams object to map. This function creates a copy of the EoS object inside the EoSParams struct.

            :param name: EoS name
            :type name: str
            :param eos: EoS object
            :type eos: EoS
            )pbdoc", "name"_a, "eos"_a)
        .def("init_eos", py::overload_cast<double, double>(&FlashParams::init_eos), R"pbdoc(
            Initialize EoS parameters at (P,T)

            :param p: Pressure
            :type p: double
            :param T: Temperature
            :type T: double
            )pbdoc", "p"_a, "T"_a)
        .def("init_eos", py::overload_cast<double, double, std::vector<double>&, int, bool>(&FlashParams::init_eos), R"pbdoc(
            Initialize EoS parameters at state (P,T,n)

            :param p: Pressure
            :type p: float
            :param T: Temperature
            :type T: float
            :param n: List of compositions
            :type n: list
            :param start_idx: Index of n[0]
            :type start_idx: int
            :param second_order: Switch for second-order properties
            :type second_order: bool
            )pbdoc", "p"_a, "T"_a, "n"_a, "start_idx"_a, "second_order"_a)
        .def("find_ref_comp", &FlashParams::find_ref_comp, R"pbdoc(
            Find stable phase at state (P,T,n)

            :param p: Pressure
            :type p: float
            :param T: Temperature
            :type T: float
            :param n: List of compositions
            :type n: list
            )pbdoc", "p"_a, "T"_a, "n"_a)
        .def("G_TP_pure", &FlashParams::G_TP_pure)

        // Timers
        .def("start_timer", &FlashParams::start_timer, R"pbdoc(
            :param key: Timer name
            :type key: str
            )pbdoc", "key"_a)
        .def("stop_timer", &FlashParams::stop_timer, R"pbdoc(
            :param key: Timer name
            :type key: str
            )pbdoc", "key"_a)
        .def("print_timers", &FlashParams::print_timers, R"pbdoc(
            This method prints all timers tracked in FlashParams.
            )pbdoc")
        ;

    py::enum_<FlashParams::SplitVars>(flash_params, "SplitVars", "Primary variables for phase split")
        .value("nik", FlashParams::SplitVars::nik)
        .value("lnK", FlashParams::SplitVars::lnK)
        .value("lnK_chol", FlashParams::SplitVars::lnK_chol)
        .export_values()
        ;

    py::enum_<FlashParams::StabilityVars>(flash_params, "StabilityVars", "Primary variables for stability")
        .value("Y", FlashParams::StabilityVars::Y)
        .value("lnY", FlashParams::StabilityVars::lnY)
        .value("alpha", FlashParams::StabilityVars::alpha)
        .export_values()
        ;

    py::class_<EoSParams> eos_params(m, "EoSParams", R"pbdoc(
            This is a class that contains all required flash/stability parameters for each EoS object.
            )pbdoc");
    
    eos_params.def_readwrite("eos", &EoSParams::eos, "EoS object")
        .def_readwrite("root_flag", &EoSParams::root_flag, "RootFlag enum for roots to be selected in stability test. 0) STABLE, 1) MIN, 2) MAX, 3) MINMAX")
        .def_readwrite("initial_guesses", &EoSParams::initial_guesses, "Set of initial guesses for EoS")
        .def_readwrite("stability_tol", &EoSParams::stability_tol, "Tolerance for stability norm")
        .def_readwrite("stability_switch_tol", &EoSParams::stability_switch_tol, "Tolerance for switch to Newton in stability")
        .def_readwrite("stability_line_tol", &EoSParams::stability_line_tol, "Tolerance for line search in stability")
        .def_readwrite("stability_max_iter", &EoSParams::stability_max_iter, "Maximum number of iterations for stability")
        .def_readwrite("stability_line_iter", &EoSParams::stability_line_iter, "Maximum number of iterations for line search in stability")
        ;

    py::enum_<EoSParams::RootFlag>(eos_params, "RootFlag", "Flag for roots to be selected in stability test")
        .value("STABLE", EoSParams::RootFlag::STABLE)
        .value("MIN", EoSParams::RootFlag::MIN)
        .value("MAX", EoSParams::RootFlag::MAX)
        .value("MINMAX", EoSParams::RootFlag::MINMAX)
        .export_values()
        ;

    py::class_<Analysis>(m, "Analysis", R"pbdoc(
            This is a class for thermodynamic analysis such as stability, Gibbs energy and tangent plane criteria.
            )pbdoc")
        .def(py::init<FlashParams&>(), R"pbdoc(
            :param flash_params: FlashParams object
            :type flash_params: FlashParams
            )pbdoc", "flash_params"_a)
        
        .def("init_stability", &Analysis::init_stability, R"pbdoc(
            :param p: Pressure
            :type p: double
            :param T: Temperature
            :type T: double
            :param x: Reference composition
            :type x: list
            :param ref_eos: Name of reference EoS
            :type ref_eos: str
            )pbdoc", "p"_a, "T"_a, "x"_a, "ref_eos"_a)
        .def("calc_stability_path", &Analysis::calc_stability_path, R"pbdoc(
            :param Y: Trial composition
            :type Y: list
            :param eos_name: Name of EoS
            :type eos_name: str
            :returns: List of compositions that make up solution path
            :rtype: list
            )pbdoc", "Y"_a, "eos_name"_a)
        
        .def("find_stationary_points", &Analysis::find_stationary_points, R"pbdoc(
            Determine stationary points of TPD function at (multiphase) composition X

            :param p: Pressure
            :type p: double
            :param T: Temperature
            :type T: double
            :param X: List of reference compositions
            :type X: list
            
            :returns: Set of stationary points
            :rtype: list
            )pbdoc", "p"_a, "T"_a, "X"_a)
        
        ;

};

// #endif //PYBIND11_ENABLED