import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

from dartsflash.libflash import EoS, InitialGuess, FlashParams, EoSParams
from dartsflash.libflash import CubicEoS, AQEoS, PureSolid
from dartsflash.pyflash import PyFlash
from dartsflash.mixtures import Mixture


if 0:
    mix = Mixture(components=["H2O", "CO2", "C1"], name="H2O-CO2-C1", np_max=2)
    a = PyFlash(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])
    a.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012}))
    a.init_flash(stabilityflash=False, eos=["AQ", "PR"], initial_guess=[InitialGuess.Henry_AV])

    state_spec = ["pressure", "temperature"]
    zrange = np.concatenate([np.array([1e-14, 1e-12, 1e-10, 1e-8]),
                             np.linspace(1e-6, 1. - 1e-6, 10),
                             np.array([1. - 1e-8, 1. - 1e-10, 1. - 1e-12, 1. - 1e-14])])
    dimensions = {"pressure": np.array([1., 2., 5., 10., 50., 100., 200., 400.]),
                  "temperature": np.arange(273.15, 373.15, 10),
                  "H2O": zrange,
                  "CO2": zrange,
                  }
    constants = {"C1": 1.}
    dims_order = ["H2O",
                  "CO2",
                  "pressure",
                  "temperature"]
    fname = mix.name

    ref = xr.open_dataset("../../tests/python/data/ref_flash_brine_vapour2.h5", engine='h5netcdf')

    for i, t in enumerate(dimensions['temperature']):
        for j, p in enumerate(dimensions['pressure']):
            for k, zc1 in enumerate(dimensions['CO2']):
                for m, zh2o in enumerate(dimensions['H2O']):
                    refdata = ref.isel(pressure=j, temperature=i, H2O=m, CO2=k)
                    state_ref = [ref.pressure[j].values, ref.temperature[i].values, ref.H2O[m].values, ref.CO2[k].values, 1.-ref.H2O[m].values-ref.CO2[k].values]
                    state = [p, t, zh2o, zc1, 1.-zh2o-zc1]
                    # print(state)
                    if state[-1] > 1e-15:
                        res = a.evaluate_single_flash(state=state)
                        # print(res['X'])
                        if not np.allclose(res['nu'], refdata.nu.values[:2], rtol=1e-10):
                            print(state)
                            print("nu", res['nu'])
                            print("nuref", refdata.nu.values[:2])
                        elif not np.allclose(res['X'], refdata.X.values[:6], rtol=1e-10):
                            print(state, state_ref)
                            print("x", res['X'])
                            print("xref", refdata.X.values[:6])

    results = a.evaluate_flash(state_spec=state_spec, dimensions=dimensions, constants=constants,
                               mole_fractions=True, dims_order=dims_order)

if 1:
    mix = Mixture(components=["H2O", "CO2"], name="H2O-CO2", np_max=2)
    a = PyFlash(mixture=mix)

    ceos = CubicEoS(mix.comp_data, CubicEoS.PR)
    ceos.set_preferred_roots(i=0, x=0.6, root_flag=EoS.MAX)
    a.add_eos("CEOS", ceos, initial_guesses=[InitialGuess.Yi.Wilson, 0, 1], switch_tol=1e-1, stability_tol=1e-20, max_iter=10)
    a.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012}),
              initial_guesses=[0], eos_range={0: [0.6, 1.]}, max_iter=10)
    a.flash_params.stability_variables = FlashParams.alpha
    a.flash_params.split_tol = 1e-20
    a.flash_params.split_switch_tol = 1e-1
    # a.flash_params.verbose = True
    a.flash_params.eos_params["CEOS"].root_flag = EoSParams.MINMAX

    a.init_flash(stabilityflash=True)

    state_spec = ["pressure", "temperature"]
    zrange = np.concatenate([
        # np.array([1e-14, 1e-12, 1e-10, 1e-8]),
        np.linspace(1e-6, 1. - 1e-6, 20),
        # np.array([1. - 1e-8, 1. - 1e-10, 1. - 1e-12, 1. - 1e-14])
    ])
    dimensions = {"pressure": np.linspace(1, 200, 200),
                  "temperature": np.array([273.15, 373.15]),
                  "H2O": zrange,
                  }
    constants = {"CO2": 1.}
    dims_order = ["H2O",
                  "pressure",
                  "temperature"]

    # dimensions["pressure"] = np.array([35.])
    # dimensions["temperature"] = np.array([273.15])
    # dimensions["H2O"] = np.array([1.05263947e-01])

    results = a.evaluate_flash(state_spec=state_spec, dimensions=dimensions, constants=constants,
                               mole_fractions=True, dims_order=dims_order)
    print(results.isel(temperature=0).nu.values[:, :, 0])

    if 1:
        from dartsflash.diagram import PhaseDiagram
        pd = PhaseDiagram(ncols=2, figsize=(12, 6))

        pd.subplot_idx = 0
        pd.Px(pressure=dimensions['pressure'], Xi=dimensions['H2O'], data=results.isel(temperature=0).nu.values[:, :, 0], levels=np.arange(0, 1.01, 0.1))
        pd.add_attributes(colorbar=True)

        pd.subplot_idx = 1
        pd.Px(pressure=dimensions['pressure'], Xi=dimensions['H2O'], data=results.isel(temperature=1).nu.values[:, :, 0], levels=np.arange(0, 1.01, 0.1))

    # a.output_to_file(results, csv_filename="df.csv")

    # df = results.to_series().unstack(level=[1, 2])
    # with pd.ExcelWriter("main.xlsx") as writer:
    #     df.to_excel(writer)

if 0:
    from dartsflash.mixtures import Y8
    mix = Y8()
    a = PyFlash(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])

    state_spec = ["pressure", "temperature"]
    components = mix.comp_data.components[:-1]
    dimensions = {"pressure": np.arange(10., 250., 5.),
                  "temperature": np.arange(190., 450., 10.),
                  }
    constants = {component: mix.composition[i] for i, component in enumerate(mix.comp_data.components[:-1])}
    dims_order = [
                  "pressure",
                  "temperature"]
    fname = "Y8"

if 0:
    from dartsflash.mixtures import MaljamarSep
    mix = MaljamarSep()
    a = PyFlash(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson, InitialGuess.Yi.Wilson13, 0], switch_tol=1e-2)
    a.flash_params.stability_variables = FlashParams.alpha
    a.flash_params.split_variables = FlashParams.nik
    a.flash_params.split_switch_tol = 1e-5
    # a.flash_params.verbose = True

    state_spec = ["pressure", "temperature"]
    dimensions = {"pressure": np.arange(64., 80., 0.1),
                  # "temperature": np.arange(190., 450., 10.),
                  "CO2": np.arange(0.65, 1., 0.002),
                  }
    constants = {component: mix.composition[i+1] for i, component in enumerate(mix.comp_data.components[1:])}
    constants.update({"temperature": 305.35})
    dims_order = ["CO2",
                  "pressure"]
    fname = mix.name

    a.init_flash()

    # flash_output = a.evaluate_single_flash(state=[68.5, 305.35, 0.9, 0.02354, 0.03295, 0.01713, 0.01099, 0.00574, 0.00965])
    # flash_output = a.evaluate_single_flash(state=[64, 305.35, 0.68, 0.075328, 0.10544, 0.054816, 0.035168, 0.018368, 0.03088])
    flash_output = a.evaluate_single_flash(state=[64, 305.35, 0.99, 0.002354, 0.003295, 0.001713, 0.001099, 0.000574, 0.000965])

    # print(flash_output)

    flash_data: xr.DataArray
    flash_data = a.evaluate_flash(state_spec, dimensions, constants, mole_fractions=True, dims_order=dims_order)
    print(flash_data)
    flash_data.np.plot()
    print(flash_data.nu[:, :, 0])

if 0:
    mix = Mixture(components=["H2O"])

    pr = CubicEoS(mix.comp_data, CubicEoS.PR)
    pr.set_root_flag(EoS.MAX)
    prL = CubicEoS(mix.comp_data, CubicEoS.PR)
    prL.set_root_flag(EoS.MIN)
    aq = AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                               AQEoS.solute: AQEoS.Ziabakhsh2012})
    ice = PureSolid(mix.comp_data, "Ice")

    # Calculate triple point (V-Aq-I)
    triplePT = np.array([6e-3, 273.15])
    tol = 1e-12
    max_iter = 500
    it = 0
    while True:
        aq.solve_TP(triplePT[0], triplePT[1], [1.])
        gA = aq.G_TP(triplePT[0], triplePT[1], [1.])
        dgA_dT = aq.dlnphi_dT()
        dgA_dP = aq.dlnphi_dP()

        pr.solve_TP(triplePT[0], triplePT[1], [1.])
        gV = pr.G_TP(triplePT[0], triplePT[1], [1.])
        dgV_dT = pr.dlnphi_dT()
        dgV_dP = pr.dlnphi_dP()

        ice.solve_TP(triplePT[0], triplePT[1], [1.])
        gS = ice.G_TP(triplePT[0], triplePT[1], [1.])
        dgS_dT = ice.dlnphi_dT()
        dgS_dP = ice.dlnphi_dP()

        res = np.array([gA - gV, gV - gS])
        Jac = np.array([[dgA_dP[0] - dgV_dP[0], dgA_dT[0] - dgV_dT[0]],
                        [dgV_dP[0] - dgS_dP[0], dgV_dT[0] - dgS_dT[0]]])
        triplePT -= 0.1 * np.linalg.solve(Jac, res)

        if np.linalg.norm(res) < tol:
            print(gA, gV, gS)
            break
        elif it > max_iter:
            print("max iter")
            break
    print("triple point", triplePT)

    pres_SV = np.arange(1e-5, triplePT[0], 1e-5)
    pres_SV = np.array(list(reversed(pres_SV)))
    T_sub = np.empty(np.shape(pres_SV))
    T_sub0 = triplePT[1]

    for i, p in enumerate(pres_SV):
        # Calculate sublimation curve
        it = 0

        T_sub[i] = T_sub0
        while True:
            pr.solve_TP(p, T_sub[i], [1.])
            gV = pr.G_TP(p, T_sub[i], [1.])
            dgV_dT = pr.dlnphi_dT()

            ice.solve_TP(p, T_sub[i], [1.])
            gS = ice.G_TP(p, T_sub[i], [1.])
            dgS_dT = ice.dlnphi_dT()

            res = gV - gS
            dres_dT = dgV_dT[0] - dgS_dT[0]

            T_sub[i] -= 0.1 * res / dres_dT

            if np.abs(res) < tol:
                T_sub0 = T_sub[i]
                break
            elif it > max_iter:
                print("TP it >", max_iter)
                T_sub0 = T_sub[i]
                break
            it += 1

    pres = 2 ** np.arange(np.log2(triplePT[0]), 8, 0.1)
    pres = pres[pres < 180]
    arr_shape = np.shape(pres)
    T_sol, T_vap, T_vapPR = np.empty(arr_shape), np.empty(arr_shape), np.empty(arr_shape)
    T_sol0, T_vap0, T_vapPR0 = 273.15, 273.15, 273.15

    for i, p in enumerate(pres):
        # Calculate evaporation curve Aq-V
        it = 0

        T_vap[i] = T_vap0
        while True:
            aq.solve_TP(p, T_vap[i], [1.])
            gA = aq.G_TP(p, T_vap[i], [1.])
            dgA_dT = aq.dlnphi_dT()

            pr.solve_TP(p, T_vap[i], [1.])
            gV = pr.G_TP(p, T_vap[i], [1.])
            dgV_dT = pr.dlnphi_dT()

            res = gA - gV
            dres_dT = dgA_dT[0] - dgV_dT[0]

            T_vap[i] -= 0.1 * res/dres_dT

            if np.abs(res) < tol:
                T_vap0 = T_vap[i]
                break
            elif it > max_iter:
                print("A-V it >", max_iter)
                T_vap0 = T_vap[i]
                break
            it += 1

        # Calculate evaporation curve L-V full PR
        it = 0

        T_vapPR[i] = T_vapPR0
        while True:
            prL.solve_TP(p, T_vapPR[i], [1.])
            gA = prL.G_TP(p, T_vapPR[i], [1.])
            dgA_dT = prL.dlnphi_dT()

            pr.solve_TP(p, T_vapPR[i], [1.])
            gV = pr.G_TP(p, T_vapPR[i], [1.])
            dgV_dT = pr.dlnphi_dT()

            res = gA - gV
            dres_dT = dgA_dT[0] - dgV_dT[0]

            T_vapPR[i] -= 0.1 * res / dres_dT

            if np.abs(res) < tol:
                T_vapPR0 = T_vapPR[i]
                break
            elif it > max_iter:
                print("PR-VL it >", max_iter)
                T_vapPR0 = T_vapPR[i]
                break
            it += 1

        # Calculate solid curve
        it = 0

        T_sol[i] = T_sol0
        while True:
            aq.solve_TP(p, T_sol[i], [1.])
            gA = aq.G_TP(p, T_sol[i], [1.])
            dgA_dT = aq.dlnphi_dT()

            ice.solve_TP(p, T_sol[i], [1.])
            gS = ice.G_TP(p, T_sol[i], [1.])
            dgS_dT = ice.dlnphi_dT()

            res = gA - gS
            dres_dT = dgA_dT[0] - dgS_dT[0]

            T_sol[i] -= 0.1 * res / dres_dT

            if np.abs(res) < tol:
                T_sol0 = T_sol[i]
                break
            elif it > max_iter:
                print("V-S it >", max_iter)
                T_sol0 = T_sol[i]
                break
            it += 1

    from dartsflash.diagram import Plot
    plot = Plot()
    plot.add_attributes(suptitle=r"Phase diagram of H$_2$O", ax_labels=["Temperature [K]", "Pressure [bar]"], grid=True)

    plot.draw_plot(xdata=[T_sub, T_sol, T_vap], ydata=[pres_SV, pres, pres], number_of_curves=3, logy=True)
    plot.draw_plot(xdata=T_vapPR, ydata=pres, style='dotted', colour=plot.colours[2])
    plot.draw_plot(xdata=[triplePT[1]], ydata=[triplePT[0]], plot_type="scatter")

plt.show()
