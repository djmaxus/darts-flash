//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_IDEAL_H
#define OPENDARTS_FLASH_EOS_IDEAL_H
//--------------------------------------------------------------------------

#include <unordered_map>
#include <vector>
#include <string>
#include "dartsflash/global/global.hpp"

namespace ideal_gas {
	extern double T_0, T_01, P_0;

	extern std::unordered_map<std::string, double> gi_00, hi_00;

	extern std::unordered_map<std::string, std::vector<double>> hi_a;
}

struct IdealGas
{
	int nc, ni, ns;
	double R, T_0;
	std::vector<double> gi_00, hi_00;
	std::vector<std::vector<double>> hi_a;

	IdealGas() { }
	IdealGas(const std::vector<std::string>& components, const std::vector<std::string>& ions={});

	// PV = nRT
	double P(double v, double T, double N) { return N * this->R * T / v; }
	double V(double p, double T, double N) { return N * this->R * T / p; }

	// Ideal Gibbs energy, enthalpy and heat capacities
	double gi(int i) { return gi_00[i] / (M_R * T_0); };  // ideal gas Gibbs energy at p0 = 1 bar, T0 = 298.15 K
	double cpi(double T, int i);
	double cvi(double T, int i) { return this->cpi(T, i) - 1.; }
	double hi(double T, int i);
	double Hi(double T, int i);
	double dHidT(double T, int i);
	double si(double p, double T, double N, int i);

	// Ideal mixture properties
	double G(std::vector<double>::iterator x_it);
	double H(double T, std::vector<double>::iterator x_it);
	double S(double T, std::vector<double>::iterator x_it);
	// double A(double T, std::vector<double>::iterator x_it);
	// double U(double T, std::vector<double>::iterator x_it);
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_IDEAL_H
//--------------------------------------------------------------------------
