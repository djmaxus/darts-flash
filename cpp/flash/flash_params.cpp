#include <cmath>
#include "dartsflash/global/timer.hpp"
#include "dartsflash/flash/flash_params.hpp"

EoSParams::EoSParams(CompData& comp_data)
{
	this->initial_guess = InitialGuess(comp_data);
}

FlashParams::FlashParams()
{
	for (int i = 0; i < timer::TOTAL; i++)
	{
		timers[i] = new Timer();
	}
}

FlashParams::FlashParams(CompData& compdata) : FlashParams()
{
	this->comp_data = compdata;
	this->nc = compdata.nc;
	this->ni = compdata.ni;
	this->ns = nc + ni;

	this->initial_guess = InitialGuess(compdata);
}

void FlashParams::add_eos(std::string name, EoS* eos)
{
	eos_params[name] = EoSParams(this->comp_data);
	eos_params[name].eos = eos->getCopy();
	return;
}

void FlashParams::init_eos(double p, double T)
{
	// Initialise EoS component parameters at p, T
    this->start_timer(timer::EOS);
	for (auto& it: this->eos_params) {
		it.second.eos->init_TP(p, T);
		it.second.initial_guess.init(p, T);
	}
	this->initial_guess.init(p, T);
    this->stop_timer(timer::EOS);
}

void FlashParams::init_eos(double p, double T, std::vector<double>& n, int start_idx, bool second_order)
{
	// Initialise EoS component parameters at p, T, n
    this->start_timer(timer::EOS);
	for (auto& it: this->eos_params) {
		it.second.eos->solve_TP(p, T, n, start_idx, second_order);
		it.second.initial_guess.init(p, T);
	}
	this->initial_guess.init(p, T);
    this->stop_timer(timer::EOS);
}

TrialPhase FlashParams::find_ref_comp(double p, double T, std::vector<double>& z)
{
    // Find reference compositions - hypothetical single phase
    double gmin = NAN;
    std::string ref_eos = "";
    EoS::RootFlag ref_root = EoS::RootFlag::STABLE;
    for (auto it: this->eos_params)
    {
        std::string eosname = it.first;
        if (it.second.eos->eos_in_range(z.begin()))
        {
            std::vector<EoS::RootFlag> root_flags = {};
            if (it.second.root_flag < EoSParams::RootFlag::MINMAX)
            {
                root_flags = {static_cast<EoS::RootFlag>(it.second.root_flag)};
            }
            else
            {
                root_flags = {EoS::RootFlag::MIN, EoS::RootFlag::MAX};
            }

            for (EoS::RootFlag root_flag: root_flags)
            {
                it.second.eos->set_root_flag(root_flag);
                double gr = it.second.eos->Gr_TP(p, T, z);

                if (it.second.eos->select_root(z.begin()) >= EoS::RootSelect::ACCEPT  // root should be selected or not
                            && (std::isnan(gmin) // gmin not initialized
                            || (gr < gmin))) // Gibbs energy of EoS is lower
                {
                    ref_root = root_flag;
                    ref_eos = eosname;
                    gmin = gr;
                }
            }
        }
    }
    TrialPhase ref_comp = TrialPhase(ref_eos, z);
    ref_comp.root = ref_root;

    if (this->verbose)
    {
        ref_comp.print_point("Reference phase");
    }
    return ref_comp;
}

std::vector<double> FlashParams::G_TP_pure(double p, double T)
{
	std::vector<double> G_pure(nc, NAN);

	for (auto it: this->eos_params)
	{
		std::vector<double> gpure = it.second.eos->G_TP_pure(p, T);

		for (int i = 0; i < nc; i++)
		{
			if (std::isnan(G_pure[i]) || gpure[i] < G_pure[i])
			{
				G_pure[i] = gpure[i];
			}
		}
	}
	return G_pure;
}

void FlashParams::print_timers()
{
	for (size_t i = 0; i < timers.size(); i++)
	{
		print("time", timers[i]->elapsedMicroseconds());
		std::cout << timer_names[i] << ": " << timers[i]->elapsedMicroseconds() << " microseconds\n";
	}
	return;
}

std::vector<TrialPhase> EoSParams::evaluate_initial_guesses(std::string eos_name, std::vector<TrialPhase>& ref_comps)
{
	std::vector<TrialPhase> trial_comps = this->initial_guess.evaluate(eos_name, this->initial_guesses, ref_comps);

	if (this->root_flag == EoSParams::RootFlag::MINMAX)
	{
		trial_comps.reserve(trial_comps.size() * 2);
		int j = 0;
		for (TrialPhase trial_comp: trial_comps)
		{
			trial_comps.push_back(trial_comp);
			trial_comps[j].root = EoS::RootFlag::MIN;
			trial_comps.back().root = EoS::RootFlag::MAX;
			j++;
		}
	}
	else
	{
		for (TrialPhase trial_comp: trial_comps)
		{
			trial_comp.root = static_cast<EoS::RootFlag>(this->root_flag);
		}
	}
	return trial_comps;
}
