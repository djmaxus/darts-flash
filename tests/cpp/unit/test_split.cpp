#include "dartsflash/phase-split/twophasesplit.hpp"
#include "dartsflash/phase-split/multiphasesplit.hpp"
#include "dartsflash/eos/helmholtz/cubic.hpp"
#include "dartsflash/eos/aq/jager.hpp"
#include "dartsflash/eos/aq/ziabakhsh.hpp"
#include "dartsflash/global/global.hpp"
#include "dartsflash/eos/initial_guess.hpp"

int test_split2_cubics();
int test_splitN_cubics();
int test_split2_aq_cubics();

struct Reference
{
	double pressure, temperature, tol{ 1e-5 };
	std::vector<double> composition, lnK, nu_ref, X_ref;
	int np, nc;

	Reference(const double p, const double T, const std::vector<double>& z, const std::vector<double>& lnk, const std::vector<double>& nu, const std::vector<double>& x) 
	: pressure(p), temperature(T), composition(z), lnK(lnk), nu_ref(nu), X_ref(x), np(static_cast<int>(nu.size())), nc(static_cast<int>(z.size())) {}

	int test(BaseSplit* split, FlashParams& flash_params, std::vector<std::string>& eos, bool verbose)
	{
		if (verbose)
		{
			std::cout << "==================================\n";
			print("p, T", std::vector<double>{pressure, temperature});
			print("z", composition);
		}

		// Initialize EOS at P, T
		flash_params.init_eos(pressure, temperature);
		flash_params.initial_guess.init(pressure, temperature);
		
		// Run split and return nu and x
		std::vector<TrialPhase> trial_comps = {};
    	for (size_t j = 0; j < eos.size(); j++)
    	{
	        trial_comps.push_back(TrialPhase{eos[j], composition});
    	}
		int error = split->run(composition, lnK, trial_comps);  // Run multiphase split algorithm at P, T, z with initial guess lnK
		std::vector<double> nu = split->getnu();
		std::vector<double> X = split->getx();

		if (error > 0)
		{
			if (!this->negativeflash(nu))
			{
				error = 0;
			}
			else
			{
				print("Error in Flash", error);
				print("p, T", std::vector<double>{pressure, temperature});
				print("z", composition);
				print("nu", nu);
				print("X", X);
				return error;
			}
		}
		
		// Compare results
		if (verbose)
		{
			std::cout << "\nResults:\n";
			print("nu", nu);
			print("x", X, np);
			std::cout << " Number of flash iterations ssi = " << split->get_ssi_iter() << std::endl;
			std::cout << " Number of flash iterations newton = " << split->get_newton_iter() << std::endl;
			std::cout << " Number of flash total iterations = " << split->get_ssi_iter() + split->get_newton_iter() << std::endl;
		}

		for (int j = 0; j < np; j++)
		{
			if (std::sqrt(std::pow(nu_ref[j]-nu[j], 2)) > tol)
			{
				std::cout << "Different values for nu\n";
				print("p, T", std::vector<double>{pressure, temperature});
				print("z", composition);
				print("nu", nu);
				print("nu_ref", nu_ref);
				print("X", X);
				return 1;
			}
			if (X_ref.size() > 0)
			{
				for (int i = 0; i < nc; i++)
				{
					if (std::sqrt(std::pow(X_ref[j*nc + i]-X[j*nc + i], 2)) > tol)
					{
						std::cout << "Different values for X\n";
						print("p, T", std::vector<double>{pressure, temperature});
						print("z", composition);
						print("X", X);
						print("X_ref", X_ref);
						return 1;
					}
				}
			}
		}
		if (verbose)
		{
			std::cout << "Output is correct!\n";
		}

		// Check if matrix inverse is correct
		if (split->test_matrices())
		{
			return 1;
		}
		return 0;
	}

	bool negativeflash(std::vector<double>& nu)
	{
		// Check if there are any negative phase fractions
		for (double nu_j: nu)
		{
			if (nu_j < 0.) { return true; }
		}
		return false;
	}
};

int main()
{
	int error_output = 0;

	error_output += test_split2_cubics();
	error_output += test_splitN_cubics();
	error_output += test_split2_aq_cubics();

    return error_output;
}

int test_split2_cubics()
{
	// Test stationary points for Vapour-Liquid mixture with PR
	bool verbose = false;
    int error_output = 0;

   	std::vector<std::string> comp{"C1", "C2", "C3", "nC5", "nC7", "nC10"};
	CompData comp_data(comp);
	comp_data.Pc = {45.99, 48.72, 42.48, 33.70, 27.40, 21.10};
	comp_data.Tc = {190.56, 305.32, 369.83, 469.70, 540.20, 617.70};
	comp_data.ac = {0.011, 0.099, 0.152, 0.252, 0.350, 0.490};
	comp_data.kij = std::vector<double>(6*6, 0.);

	std::vector<double> z = {0.8097, 0.0566, 0.0306, 0.0457, 0.0330, 0.0244};

	CubicEoS pr(comp_data, CubicEoS::PR);

	FlashParams flash_params(comp_data);
    flash_params.split_switch_tol = 1e-3;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);
	std::vector<std::string> eos{"PR", "PR"};

	std::vector<Reference> references = {
		// Wilson K-values
		Reference(220., 335., z, {-0.776924666, 0.9843790174, 2.288137786, 4.580984519, 6.526154563, 9.100268473}, {0.8884390842, 0.1115609158},
								 {0.8221423089, 0.05614761493, 0.02979141223, 0.04262333649, 0.0293377959, 0.01995753152, 
								  0.7106130261, 0.06020266471, 0.0370393607, 0.07020166435, 0.06216474137, 0.05977854273}),
		// Stationary point K-values
		Reference(220., 335., z, {-0.1534524683, 0.05750830524, 0.2004291414, 0.4707291258, 0.7106977244, 1.037107362}, {0.8884390842, 0.1115609158},
								 {0.8221423089, 0.05614761493, 0.02979141223, 0.04262333649, 0.0293377959, 0.01995753152, 
								  0.7106130261, 0.06020266471, 0.0370393607, 0.07020166435, 0.06216474137, 0.05977854273})
	};

	std::vector<bool> modcholesky = {false, true};
	std::vector<FlashParams::SplitVars> vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		for (FlashParams::SplitVars var: vars)
		{
			flash_params.split_variables = var;
			TwoPhaseSplit split(flash_params);
			for (Reference condition: references)
			{
				error_output += condition.test(&split, flash_params, eos, verbose);
			}
		}

		for (FlashParams::SplitVars var: vars)
		{
			flash_params.split_variables = var;
			MultiPhaseSplit split(flash_params, 2);
			for (Reference condition: references)
			{
				error_output += condition.test(&split, flash_params, eos, verbose);
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_split2_cubics()", error_output);
	}
	else
	{
		print("No errors occurred in test_split2_cubics()", error_output);
	}
    return error_output;
}

int test_splitN_cubics()
{
	// Test Maljamar separator mixture (Orr, 1981), data from (Li, 2012)
	bool verbose = false;
	std::cout << (verbose ? "TESTING NP STABILITYFLASH\n" : "");
	int error_output = 0;

    int np_max = 3;
    std::vector<std::string> comp = {"CO2", "C5-7", "C8-10", "C11-14", "C15-20", "C21-28", "C29+"};
	int nc = 7;

    CompData comp_data(comp);
    comp_data.Pc = {73.9, 28.8, 23.7, 18.6, 14.8, 12.0, 8.5};
    comp_data.Tc = {304.2, 516.7, 590.0, 668.6, 745.8, 812.7, 914.9};
    comp_data.ac = {0.225, 0.265, 0.364, 0.499, 0.661, 0.877, 1.279};
    comp_data.kij = std::vector<double>(7*7, 0.);
    comp_data.set_binary_coefficients(0, {0.0, 0.115, 0.115, 0.115, 0.115, 0.115, 0.115});

	std::vector<double> z_init = {0.0, 0.2354, 0.3295, 0.1713, 0.1099, 0.0574, 0.0965};

	FlashParams flash_params(comp_data);
    flash_params.split_switch_tol = 1e-5;
	flash_params.stability_variables = FlashParams::alpha;
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
    flash_params.eos_params["PR"].stability_switch_tol = 1e-1;
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson, 
													 InitialGuess::Yi::Wilson13,
													 0};  // pure CO2 initial guess
	std::vector<std::string> eos = {"PR", "PR", "PR"};

	std::vector<std::pair<Reference, double>> references = {
		// Stationary point K-values
		{Reference(68.5, 305.35, z_init, {0.3834965448, -2.785241313, -4.104610373, -5.900809482, -8.210331241, -11.22148172, -17.70290258,
										  0.3396862351, -1.174919545, -1.652213599, -2.301283461, -3.135738997, -4.22438433, -6.566479715}, 
										 {0.2738025267, 0.5856075977, 0.1405898757}, {}), 0.90},
	};
    
	for (size_t j = 0; j < references.size(); j++)
	{
		references[j].first.composition[0] = references[j].second;
    	for (int i = 1; i < nc; i++)
    	{
        	references[j].first.composition[i] = z_init[i]*(1.0-references[j].second);
    	}
	}

	std::vector<FlashParams::SplitVars> vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	for (FlashParams::SplitVars var: vars)
	{
		flash_params.split_variables = var;
		MultiPhaseSplit split(flash_params, np_max);
		for (std::pair<Reference, double> condition: references)
		{
			error_output += condition.first.test(&split, flash_params, eos, verbose);
		}
	}
	
	if (error_output > 0)
	{
		print("Errors occurred in test_splitN_cubics()", error_output);
	}
	else
	{
		print("No errors occurred in test_splitN_cubics()", error_output);
	}
    return error_output;
}

int test_split2_aq_cubics()
{
	// Test stationary points for Brine-Vapour mixture with PR and AQ
	bool verbose = false;
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};
	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};

	CubicEoS pr(comp_data, CubicEoS::PR);

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);
	aq.set_eos_range(0, std::vector<double>{0.9, 1.});

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.split_tol = 1e-10;
	flash_params.rr2_tol = 1e-15;
	flash_params.min_z = 1e-13;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);
	flash_params.add_eos("AQ", &aq);
	std::vector<std::string> eos{"AQ", "PR"};

	std::vector<std::vector<double>> zero = {{1.-1e-11, 1e-11, 0.}, {1e-11, 1.-1e-11, 0.}};
	std::vector<double> min_z = {1e-10, 1e-5};
	std::vector<Reference> references = {
		// Henry's law K-values
		// Freezing point
		Reference(1., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {-5.101313553, 6.786366766, 10.09988994}, {-0.006583882668, 1.006583883}, {}),
		Reference(1., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {-5.101313553, 6.786366766, 10.09988994}, {-0.006573810312, 1.00657381}, {}),
		Reference(1., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {-5.101313553, 6.786366766, 10.09988994}, {1.000045058, -4.505820337e-05}, {}),
		Reference(1., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {-5.101313553, 6.786366766, 10.09988994}, {1.000040005, -4.000548299e-05}, {}),
		Reference(1., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {-5.101313553, 6.786366766, 10.09988994}, {-0.006604414111, 1.006604414}, {}),
		Reference(1., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {-5.101313553, 6.786366766, 10.09988994}, {-0.006599374337, 1.006599374}, {}),
		Reference(1., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {-5.101313553, 6.786366766, 10.09988994}, {-0.006557653417, 1.006557653}, {}),
		Reference(1., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {-5.101313553, 6.786366766, 10.09988994}, {-0.006552620749, 1.006552621}, {}),
		Reference(1., 273.15, {0.5, 0.25, 0.25}, {-5.101313553, 6.786366766, 10.09988994}, {0.4970505533, 0.5029494467}, {}),
		Reference(1., 273.15, {0.8, 0.1, 0.1}, {-5.101313553, 6.786366766, 10.09988994}, {0.7992302375, 0.2007697625}, {}),
		Reference(1., 273.15, {0.9, 0.05, 0.05}, {-5.101313553, 6.786366766, 10.09988994}, {0.8999551701, 0.1000448299}, {}),
		// Reference(1., 273.15, zero[0], {-5.101313553, 6.786366766, 10.09988994}, {1., 0.}, {}),
		// Reference(1., 273.15, zero[1], {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		Reference(10., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {-7.403898646, 4.483781673, 7.797304845}, {-0.0007125855095, 1.000712586}, {}),
		Reference(10., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {-7.403898646, 4.483781673, 7.797304845}, {-0.0007025147103, 1.000702515}, {}),
		Reference(10., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {-7.403898646, 4.483781673, 7.797304845}, {1.00043245, -0.0004324496362}, {}),
		Reference(10., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {-7.403898646, 4.483781673, 7.797304845}, {1.000427442, -0.0004274420395}, {}),
		Reference(10., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {-7.403898646, 4.483781673, 7.797304845}, {-0.0007372768553, 1.000737277}, {}),
		Reference(10., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {-7.403898646, 4.483781673, 7.797304845}, {-0.0007322110879, 1.000732211}, {}),
		Reference(10., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {-7.403898646, 4.483781673, 7.797304845}, {-0.0006834610518, 1.000683461}, {}),
		Reference(10., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {-7.403898646, 4.483781673, 7.797304845}, {-0.0006784558336, 1.000678456}, {}),
		Reference(10., 273.15, {0.5, 0.25, 0.25}, {-7.403898646, 4.483781673, 7.797304845}, {0.5028281496, 0.4971718504}, {}),
		Reference(10., 273.15, {0.8, 0.1, 0.1}, {-7.403898646, 4.483781673, 7.797304845}, {0.8048675531, 0.1951324469}, {}),
		Reference(10., 273.15, {0.9, 0.05, 0.05}, {-7.403898646, 4.483781673, 7.797304845}, {0.9054062135, 0.0945937865}, {}),
		// Reference(10., 273.15, zero[0], {-7.403898646, 4.483781673, 7.797304845}, {1., 0.}, {}),
		// Reference(10., 273.15, zero[1], {-7.403898646, 4.483781673, 7.797304845}, {0., 1.}, {}),
		Reference(100., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {-9.706483739, 2.18119658, 5.494719752}, {-0.0002366764693, 1.000236676}, {}),
		Reference(100., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {-9.706483739, 2.18119658, 5.494719752}, {-0.0002264068539, 1.000226407}, {}),
		Reference(100., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {-9.706483739, 2.18119658, 5.494719752}, {1.002927342, -0.002927342422}, {}),
		Reference(100., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {-9.706483739, 2.18119658, 5.494719752}, {1.002922327, -0.002922326914}, {}),
		Reference(100., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {-9.706483739, 2.18119658, 5.494719752}, {-0.001868533976, 1.001868534}, {}),
		Reference(100., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {-9.706483739, 2.18119658, 5.494719752}, {-0.00186331001, 1.00186331}, {}),
		Reference(100., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {-9.706483739, 2.18119658, 5.494719752}, {-0.0001123062686, 1.000112306}, {}),
		Reference(100., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {-9.706483739, 2.18119658, 5.494719752}, {-0.0001072917425, 1.000107292}, {}),
		Reference(100., 273.15, {0.5, 0.25, 0.25}, {-9.706483739, 2.18119658, 5.494719752}, {0.5132407229, 0.4867592771}, {}),
		Reference(100., 273.15, {0.8, 0.1, 0.1}, {-9.706483739, 2.18119658, 5.494719752}, {0.8204201149, 0.1795798851}, {}),
		Reference(100., 273.15, {0.9, 0.05, 0.05}, {-9.706483739, 2.18119658, 5.494719752}, {0.9209840974, 0.07901590259}, {}),
		// Reference(100., 273.15, zero[0], {-9.706483739, 2.18119658, 5.494719752}, {1., 0.}, {}),
		// Reference(100., 273.15, zero[1], {-9.706483739, 2.18119658, 5.494719752}, {0., 1.}, {}),

		// Boiling point
		// Reference(1., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, {0.5, 0.25, 0.25}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, {0.8, 0.1, 0.1}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, {0.9, 0.05, 0.05}, {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, zero[0], {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		// Reference(1., 373.15, zero[1], {-5.101313553, 6.786366766, 10.09988994}, {0., 1.}, {}),
		Reference(10., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {-2.284581466, 6.838432915, 9.661403745}, {-0.1225571278, 1.122557128}, {}),
		Reference(10., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {-2.284581466, 6.838432915, 9.661403745}, {-0.1225459115, 1.122545911}, {}),
		Reference(10., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {-2.284581466, 6.838432915, 9.661403745}, {1.000157002, -0.0001570015211}, {}),
		Reference(10., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {-2.284581466, 6.838432915, 9.661403745}, {1.000151383, -0.0001513833213}, {}),
		Reference(10., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {-2.284581466, 6.838432915, 9.661403745}, {-0.1241988197, 1.12419882}, {}),
		Reference(10., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {-2.284581466, 6.838432915, 9.661403745}, {-0.1241931722, 1.124193172}, {}),
		Reference(10., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {-2.284581466, 6.838432915, 9.661403745}, {-0.1205586382, 1.120558638}, {}),
		Reference(10., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {-2.284581466, 6.838432915, 9.661403745}, {-0.1205530592, 1.1205530598}, {}),
		Reference(10., 373.15, {0.5, 0.25, 0.25}, {-2.284581466, 6.838432915, 9.661403745}, {0.4392704216, 0.5607295784}, {}),
		Reference(10., 373.15, {0.8, 0.1, 0.1}, {-2.284581466, 6.838432915, 9.661403745}, {0.7763746338, 0.2236253662}, {}),
		Reference(10., 373.15, {0.9, 0.05, 0.05}, {-2.284581466, 6.838432915, 9.661403745}, {0.8887319936, 0.1112680064}, {}),
		// Reference(10., 373.15, zero[0], {-2.284581466, 6.838432915, 9.661403745}, {0., 1.}, {}),
		// Reference(10., 373.15, zero[1], {-2.284581466, 6.838432915, 9.661403745}, {0., 1.}, {}),
		Reference(100., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {-4.587166559, 4.535847822, 7.358818652}, {-0.01641650788, 1.016416508}, {}),
		Reference(100., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {-4.587166559, 4.535847822, 7.358818652}, {-0.01640629408, 1.016406294}, {}),
		Reference(100., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {-4.587166559, 4.535847822, 7.358818652}, {1.001309195, -0.001309194935}, {}),
		Reference(100., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {-4.587166559, 4.535847822, 7.358818652}, {1.001304122, -0.00130412173}, {}),
		Reference(100., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {-4.587166559, 4.535847822, 7.358818652}, {-0.02020875895, 1.020208759}, {}),
		Reference(100., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {-4.587166559, 4.535847822, 7.358818652}, {-0.02020353629, 1.020203536}, {}),
		Reference(100., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {-4.587166559, 4.535847822, 7.358818652}, {-0.01347981957, 1.01347982}, {}),
		Reference(100., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {-4.587166559, 4.535847822, 7.358818652}, {-0.01347477277, 1.013474773}, {}),
		Reference(100., 373.15, {0.5, 0.25, 0.25}, {-4.587166559, 4.535847822, 7.358818652}, {0.4958719623, 0.5041280377}, {}),
		Reference(100., 373.15, {0.8, 0.1, 0.1}, {-4.587166559, 4.535847822, 7.358818652}, {0.8031386572, 0.1968613428}, {}),
		Reference(100., 373.15, {0.9, 0.05, 0.05}, {-4.587166559, 4.535847822, 7.358818652}, {0.9053836364, 0.09461636362}, {}),
		// Reference(100., 373.15, zero[0], {-4.587166559, 4.535847822, 7.358818652}, {0., 1.}, {}),
		// Reference(100., 373.15, zero[1], {-4.587166559, 4.535847822, 7.358818652}, {0., 1.}, {}),

		// Difficult conditions
		// Reference(109.29, 300.1, {0.999, 0.001, 0.}, {-8.030917791, 2.881408754, 6.030547157}, {0., 1.}, {}), // difficult conditions
		Reference(30., 330., {0.6, 0.39, 0.01}, {-5.161886783, 4.898823221, 7.897001844}, {0.6019105675, 0.3980894325}, {}) // difficult conditions
	};

	std::vector<bool> modcholesky = {false, true};
	std::vector<FlashParams::SplitVars> vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
	
		for (FlashParams::SplitVars var: vars)
		{
			flash_params.split_variables = var;
			TwoPhaseSplit split(flash_params);
			for (Reference condition: references)
			{
				error_output += condition.test(&split, flash_params, eos, verbose);
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_split2_aq_cubics()", error_output);
	}
	else
	{
		print("No errors occurred in test_split2_aq_cubics()", error_output);
	}
    return error_output;
}
