#include "dartsflash/global/global.hpp"
#include "dartsflash/maths/geometry.hpp"
#include "dartsflash/maths/maths.hpp"
#include "dartsflash/maths/modifiedcholeskys99.hpp"

int test_combinations();
int test_simplex();

int main() 
{
    int error_output = 0;

    error_output += test_combinations();
    error_output += test_simplex();

    return error_output;
}

int test_combinations()
{
    int error_output = 0;

    std::vector<std::pair<Combinations, std::vector<std::vector<int>>>> combinations = {
        {Combinations(2, 2), {{0, 1}}},
        {Combinations(3, 2), {{0, 1}, {0, 2}, {1, 2}}},
        {Combinations(4, 2), {{0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 3}}},
        {Combinations(3, 3), {{0, 1, 2}}},
        {Combinations(4, 3), {{0, 1, 2}, {0, 1, 3}, {0, 2, 3}, {1, 2, 3}}},
        {Combinations(5, 3), {{0, 1, 2}, {0, 1, 3}, {0, 1, 4}, {0, 2, 3}, {0, 2, 4}, {0, 3, 4}, {1, 2, 3}, {1, 2, 4}, {1, 3, 4}, {2, 3, 4}}},
        {Combinations(4, 4), {{0, 1, 2, 3}}},
        {Combinations(5, 4), {{0, 1, 2, 3}, {0, 1, 2, 4}, {0, 1, 3, 4}, {0, 2, 3, 4}, {1, 2, 3, 4}}},
    };

    for (auto it: combinations)
    {
        for (size_t i = 0; i < it.first.combinations.size(); i++)
        {
            std::vector<int> combination = it.first.combinations[i];
            for (size_t ii = 0; ii < combination.size(); ii++)
            {
                if (combination[ii] != it.second[i][ii])
                {
                    error_output++;
                    std::cout << "Different combinations\n";
        			print("n_elements, combination_length", std::vector<int>{it.first.n_elements, it.first.combination_length});
        			print("combinations", it.first.combinations);
		        	print("ref", it.second);
                    break;
                }
            }
        }
    }

    if (error_output > 0)
	{
		print("Errors occurred in test_combinations()", error_output);
	}
	else
	{
		print("No errors occurred in test_combinations()", error_output);
	}
	return error_output;
}

int test_simplex()
{
    int error_output = 0;

    // Is between two points? (2D simplex/line)
    std::vector<std::vector<double>> coords = {{0.}, {1.}};
    std::vector<std::vector<double>> points = {{0.2}, {1.}, {1.2}};
    std::vector<bool> in_simplex = {true, true, false};
    for (size_t i = 0; i < points.size(); i++)
    {
        if (is_in_simplex(points[i], coords) != in_simplex[i])
        {
            error_output++;
            std::cout << (!in_simplex[i] ? "Point is in 2D simplex (line), but shouldn't be" : "Point isn't in 2D simplex (line), but should be") << "\n";
            print("Point", points[i]);
        }
    }

    coords = {{0.5}, {2.}};
    points = {{0.2}, {0.5}, {1.}, {2.}, {3.}};
    in_simplex = {false, true, true, true, false};
    for (size_t i = 0; i < points.size(); i++)
    {
        if (is_in_simplex(points[i], coords) != in_simplex[i])
        {
            error_output++;
            std::cout << (!in_simplex[i] ? "Point is in 2D simplex (line), but shouldn't be" : "Point isn't in 2D simplex (line), but should be") << "\n";
            print("Point", points[i]);
        }
    }

    // Is in triangle? (3D simplex/triangle)
    coords = {{0., 0.}, {0., 1.}, {1., 0.}};
    points = {{0.2, 0.2}, {0., 1.}, {0.5, 0.5}, {1., 1.}};
    in_simplex = {true, true, true, false};
    for (size_t i = 0; i < points.size(); i++)
    {
        if (is_in_simplex(points[i], coords) != in_simplex[i])
        {
            error_output++;
            std::cout << (!in_simplex[i] ? "Point is in 3D simplex (triangle), but shouldn't be" : "Point isn't in 3D simplex (triangle), but should be") << "\n";
            print("Point", points[i]);
        }
    }

    coords = {{1., 4.}, {6., 3.}, {7.1, 6.5}};
    points = {{1., 4.}, {2., 4.2}, {3.5, 3.5}, {0.2, 0.2}, {7.1-1e-12, 6.5}};
    in_simplex = {true, true, true, false, false};
    for (size_t i = 0; i < points.size(); i++)
    {
        if (is_in_simplex(points[i], coords) != in_simplex[i])
        {
            error_output++;
            std::cout << (!in_simplex[i] ? "Point is in 3D simplex (triangle), but shouldn't be" : "Point isn't in 3D simplex (triangle), but should be") << "\n";
            print("Point", points[i]);
        }
    }

    // Is in tetrahedron? (4D simplex/tetrahedron)
    coords = {{0., 0., 0.}, {0., 0., 1.}, {0., 1., 0.}, {1., 0., 0.}};
    points = {{0.2, 0.2, 0.2}, {0.25, 0.25, 0.25}, {0., 1., 0.}, {1., 1., 0.}, {0., 1., 1.}, {0.5, 0.5, 0.5}, {1., 1., 1.}};
    in_simplex = {true, true, true, false, false, false, false};
    for (size_t i = 0; i < points.size(); i++)
    {
        if (is_in_simplex(points[i], coords) != in_simplex[i])
        {
            error_output++;
            std::cout << (!in_simplex[i] ? "Point is in 4D simplex (tetrahedron), but shouldn't be" : "Point isn't in 4D simplex (tetrahedron), but should be") << "\n";
            print("Point", points[i]);
        }
    }

    if (error_output > 0)
	{
		print("Errors occurred in test_simplex()", error_output);
	}
	else
	{
		print("No errors occurred in test_simplex()", error_output);
	}
    return error_output;
}
