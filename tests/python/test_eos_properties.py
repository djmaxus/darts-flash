import numpy as np
import xarray as xr
import pytest

from dartsflash.libflash import CubicEoS, AQEoS, InitialGuess
from dartsflash.pyflash import PyFlash
from .conftest import mixture_brine_vapour


# Test of full compositional space for H2O-CO2-C1 mixture
@pytest.fixture()
def flash_obj(mixture_brine_vapour):
    mix = mixture_brine_vapour
    f = PyFlash(mixture=mix)

    f.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])
    f.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012}))
    f.init_flash(stabilityflash=False, eos=["AQ", "PR"], initial_guess=[InitialGuess.Henry_AV])
    return f


@pytest.fixture()
def ref_eos_properties():
    return "./tests/python/data/ref_eos_properties.h5"


def test_eos_properties(flash_obj, ref_eos_properties):
    state_spec = ["pressure", "temperature"]
    zrange = np.concatenate([np.array([1e-12, 1e-10, 1e-8]),
                             np.linspace(1e-6, 1. - 1e-6, 10),
                             np.array([1. - 1e-8, 1. - 1e-10, 1. - 1e-12])])
    dimensions = {"pressure": np.array([1., 2., 5., 10., 50., 100., 200., 400.]),
                  "temperature": np.arange(273.15, 373.15, 10),
                  "H2O": zrange,
                  "CO2": zrange,
                  }
    constants = {"C1": 1.}
    dims_order = ["H2O",
                  "CO2",
                  "pressure",
                  "temperature"]
    properties = {"V": flash_obj.eos["PR"].V,
                  "G": flash_obj.eos["PR"].G_TP,
                  "H": flash_obj.eos["PR"].H_TP,
                  "Cvr": flash_obj.eos["PR"].Cvr,
                  "Cv": flash_obj.eos["PR"].Cv,
                  "Cpr": flash_obj.eos["PR"].Cpr,
                  "Cp": flash_obj.eos["PR"].Cp,
                  "JT": flash_obj.eos["PR"].JT,
                  "vs": flash_obj.eos["PR"].vs,
                  }
    props = list(properties.keys())

    results = flash_obj.evaluate_properties_1p(state_spec=state_spec, dimensions=dimensions, constants=constants,
                                                  properties_to_evaluate=properties, mole_fractions=True, dims_order=dims_order)
    results = results[props]

    if 1:
        # Use assertions from xarray
        ref = xr.open_dataset(ref_eos_properties, engine='h5netcdf')
        ref = ref[props]

        try:
            xr.testing.assert_allclose(results, ref, rtol=1e-5)
        except AssertionError:
            for i, p in enumerate(results.pressure.values):
                for j, t in enumerate(results.temperature.values):
                    for k, h2o in enumerate(results.H2O.values):
                        for m, co2 in enumerate(results.CO2.values):
                            v = results.isel(pressure=i, temperature=j, H2O=k, CO2=m).V.values
                            v_ref = ref.isel(pressure=i, temperature=j, H2O=k, CO2=m).V.values
                            g = results.isel(pressure=i, temperature=j, H2O=k, CO2=m).G.values
                            g_ref = ref.isel(pressure=i, temperature=j, H2O=k, CO2=m).G.values
                            h = results.isel(pressure=i, temperature=j, H2O=k, CO2=m).H.values
                            h_ref = ref.isel(pressure=i, temperature=j, H2O=k, CO2=m).H.values
                            jt = results.isel(pressure=i, temperature=j, H2O=k, CO2=m).JT.values
                            jt_ref = ref.isel(pressure=i, temperature=j, H2O=k, CO2=m).JT.values
                            if not np.allclose(v, v_ref, rtol=1e-5, equal_nan=True):
                                raise AssertionError("v != v_ref", p, t, h2o, co2, v, v_ref)
                            elif not np.allclose(g, g_ref, rtol=1e-5, equal_nan=True):
                                raise AssertionError("g != g_ref", p, t, h2o, co2, g, g_ref)
                            elif not np.allclose(h, h_ref, rtol=1e-5, equal_nan=True):
                                raise AssertionError("h != h_ref", p, t, h2o, co2, h, h_ref)
                            elif not np.allclose(jt, jt_ref, rtol=1e-5, equal_nan=True):
                                raise AssertionError("jt != jt_ref", p, t, h2o, co2, jt, jt_ref)

    else:
        # Update reference file
        results.to_netcdf(ref_eos_properties, engine='h5netcdf')
