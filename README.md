# DARTS-flash

[![Documentation Status](https://readthedocs.org/projects/darts-flash/badge/?version=latest)](https://darts-flash.readthedocs.io/en/latest/?badge=latest)

DARTS-flash is a standalone multiphase flash library that performs flash and thermodynamic properties calculations. DARTS-flash has interfaces in C++ and Python and it depends on `Eigen` and `Pybind11` libraries.

## Features
- (TODO Michiel)

## Installation

### Via pip (Coming soon)

DARTS-flash is available for Python 3.8 to 3.11.

```shell
pip install dartsflash
```

### Building dartsflash

> By default (i.e. unless indicated otherwise), when [building darts-flash](https://gitlab.com/open-darts/darts-flash/-/wikis/Build-instructions) the python package will be installed.

The package can be installed after compiling `libflash.so` or `libflash.pyd` and copying it to the folder `dartsflash`. Then, while located at the root folder `darts-flash` run the following:

```shell
pip install .
```

## For developers

Check our [wiki](https://gitlab.com/open-darts/darts-flash/-/wikis/home) and the section on [how to contribute](https://gitlab.com/open-darts/darts-flash/-/wikis/Contributing).

## Citing DARTS-flash
