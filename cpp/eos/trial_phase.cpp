#include <iostream>
#include <numeric>
#include <algorithm>
#include "dartsflash/global/global.hpp"
#include "dartsflash/eos/trial_phase.hpp"
#include "dartsflash/eos/eos.hpp"

TrialPhase::TrialPhase(std::string eos_name_, std::vector<double>& Y_)
{
	this->Y = Y_;
	this->y = Y_;
	this->eos_name = eos_name_;
	this->tpd = 0.;
	this->root = EoS::RootFlag::STABLE;
	this->is_stable_root = true;
	this->is_preferred_root = EoS::RootSelect::ACCEPT;
	this->is_in_range = true;
}

void TrialPhase::set_stationary_point(std::vector<double>& Y_, double tpd_)
{
	// Set stationary point value and tpd
	this->Y = Y_;
	this->tpd = tpd_;

	// Calculate mole fractions
	double Y_tot_inv = 1./std::accumulate(Y.begin(), Y.end(), 0.);
	std::transform(Y.begin(), Y.end(), y.begin(), [&Y_tot_inv](double element) { return element *= Y_tot_inv; });
}

void TrialPhase::print_point(std::string text)
{
	std::cout << text << ":\n";
	print("Y", this->Y);
	print("tpd", this->tpd);
	print("eos", this->eos_name);
	print("within EoS-range?", this->is_in_range);
	print("root", this->root);
	print("is stable root?", this->is_stable_root);
	print("is preferred root?", this->is_preferred_root);
}
