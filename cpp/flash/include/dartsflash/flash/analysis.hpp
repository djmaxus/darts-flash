//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_FLASH_ANALYSIS_H
#define OPENDARTS_FLASH_FLASH_ANALYSIS_H
//--------------------------------------------------------------------------

#include <vector>
#include <algorithm>
#include <Eigen/Dense>
#include "dartsflash/global/global.hpp"
#include "dartsflash/flash/flash_params.hpp"
#include "dartsflash/stability/stability.hpp"

class Analysis
{
protected:
    int nc, ni, ns;
    std::vector<double> h;
    std::vector<std::vector<double>> solution_path;
    TrialPhase ref_comp, trial_comp;
    std::vector<TrialPhase> stationary_points;
    FlashParams flash_params;

public:
    Analysis(FlashParams& flashparams);
    virtual ~Analysis() = default;

    // Stability analysis
    void init_stability(double p, double T, std::vector<double>& x, std::string ref_eos);
    std::vector<std::vector<double>> calc_stability_path(std::vector<double>& Y, std::string eosname);

    // Phase split analysis
    // std::vector<std::vector<double>> calc_split_path(std::vector<double>& );

    // Stationary point analysis
    std::vector<TrialPhase> find_stationary_points(double p, double T, std::vector<double>& X);
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_FLASH_ANALYSIS_H
//--------------------------------------------------------------------------
