import numpy as np
import matplotlib.pyplot as plt

from dartsflash.pyflash import PyFlash
from dartsflash.diagram import Plot
from dartsflash.libflash import InitialGuess, EoS, EoSParams


from dartsflash.mixtures import Mixture
from dartsflash.libflash import CubicEoS, AQEoS, PureSolid


if 1:
    mix = Mixture(components=["H2O", "CO2"], name="H2O-CO2", np_max=2)
    f = PyFlash(mixture=mix)

    f.add_eos("CEOSl", CubicEoS(mix.comp_data, CubicEoS.SRK),
              initial_guesses=[InitialGuess.Yi.Wilson],
              root_flag=EoSParams.MIN,
              preferred_roots=[(0, 0.6, EoS.MAX)]
              )
    f.add_eos("CEOSv", CubicEoS(mix.comp_data, CubicEoS.SRK),
              initial_guesses=[InitialGuess.Yi.Wilson],
              root_flag=EoSParams.MAX,
              preferred_roots=[(0, 0.6, EoS.MAX)]
              )
    f.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012}),
              eos_range={0: [0.6, 1.]}, initial_guesses=[0])
    f.init_flash(stabilityflash=False, eos=["AQ", "CEOS"], initial_guess=[InitialGuess.Ki.Henry_AV])

    aq = f.eos["AQ"]
    ceosl = f.eos["CEOSl"]
    ceosv = f.eos["CEOSv"]

    state_spec = ["pressure", "temperature"]
    components = mix.comp_data.components[:-1]
    dimensions = {"pressure": np.array([35.]),
                  "temperature": np.arange(273.15, 523.16, 25.),
                  "H2O": np.arange(0.005, 1., 0.005)}
    constants = {"CO2": 1.}
    dims_order = ["H2O",
                  "pressure",
                  "temperature"]
    props_a = {"G_TP": aq.G_TP}
    props_l = {"G_TP": ceosl.G_TP}
    props_v = {"G_TP": ceosv.G_TP}

    ice = PureSolid(mix.comp_data, "Ice")

    Ga = f.evaluate_properties_1p(state_spec, dimensions, constants, properties_to_evaluate=props_a,
                                  mole_fractions=True, dims_order=dims_order)
    Ga_mix = f.evaluate_properties_of_mixing(state_spec, dimensions, constants, properties_to_evaluate=props_a,
                                             mole_fractions=True, dims_order=dims_order)

    Gl = f.evaluate_properties_1p(state_spec, dimensions, constants, properties_to_evaluate=props_l,
                                  mole_fractions=True, dims_order=dims_order)
    Gl_mix = f.evaluate_properties_of_mixing(state_spec, dimensions, constants, properties_to_evaluate=props_l,
                                             mole_fractions=True, dims_order=dims_order)

    Gv = f.evaluate_properties_1p(state_spec, dimensions, constants, properties_to_evaluate=props_v,
                                  mole_fractions=True, dims_order=dims_order)
    Gv_mix = f.evaluate_properties_of_mixing(state_spec, dimensions, constants, properties_to_evaluate=props_v,
                                             mole_fractions=True, dims_order=dims_order)

    p_idx, T_idx = 0, 0
    pres, temp = dimensions["pressure"][p_idx], dimensions["temperature"][T_idx]
    xH2O = dimensions['H2O']
    G = [Ga.isel(temperature=T_idx, pressure=p_idx).G_TP.values,
         Gl.isel(temperature=T_idx, pressure=p_idx).G_TP.values,
         Gv.isel(temperature=T_idx, pressure=p_idx).G_TP.values]
    Gmix = [Ga_mix.isel(temperature=T_idx, pressure=p_idx).G_TP.values,
            Gl_mix.isel(temperature=T_idx, pressure=p_idx).G_TP.values,
            Gv_mix.isel(temperature=T_idx, pressure=p_idx).G_TP.values]
    Gi = ice.G_TP(pres, temp, [1.])

    gibbs_plot = Plot(nrows=2, figsize=(10, 8), sharex=True)
    gibbs_plot.add_attributes(suptitle=r"H$_2$O-CO$_2$ mixture at P = {} bar and T = {} K".format(pres, temp))

    gibbs_plot.subplot_idx = 0
    gibbs_plot.draw_plot([xH2O, xH2O, xH2O], ydata=G, number_of_curves=3,
                         datalabels=["Aq", "L", "V"])
    gibbs_plot.draw_plot([1.], [Gi], number_of_curves=1, plot_type="scatter")
    gibbs_plot.add_attributes(title="G", ax_labels=["xH2O [-]", "G/RT"], grid=True, legend=True)

    gibbs_plot.subplot_idx = 1
    gibbs_plot.draw_plot([xH2O, xH2O, xH2O], ydata=Gmix, number_of_curves=3,
                         datalabels=["Aq", "L", "V"])
    gibbs_plot.add_attributes(title=r"G$_{mix}$", ax_labels=["xH2O [-]", r"G$_{mix}$/RT"], grid=True, legend=True)

if 0:
    mix = Mixture(components=["H2S", "C1"], np_max=2)
    f = PyFlash(mixture=mix)

    f.add_eos("CEOSl", CubicEoS(mix.comp_data, CubicEoS.SRK),
              initial_guesses=[InitialGuess.Yi.Wilson],
              root_flag=EoSParams.MIN,
              )
    f.add_eos("CEOSv", CubicEoS(mix.comp_data, CubicEoS.SRK),
              initial_guesses=[InitialGuess.Yi.Wilson],
              root_flag=EoSParams.MAX,
              )
    f.init_flash(stabilityflash=True)

    ceosl = f.eos["CEOSl"]
    ceosv = f.eos["CEOSv"]

    state_spec = ["pressure", "temperature"]
    components = mix.comp_data.components[:-1]
    dimensions = {"pressure": np.array([40. * 1.01325]),
                  "temperature": np.array([190.]),
                  "H2S": np.arange(0.005, 1., 0.005)}
    constants = {"C1": 1.}
    dims_order = ["H2S",
                  "pressure",
                  "temperature"]
    props_l = {"G_TP": ceosl.G_TP}
    props_v = {"G_TP": ceosv.G_TP}

    Gl = f.evaluate_properties_1p(state_spec, dimensions, constants, properties_to_evaluate=props_l,
                                  mole_fractions=True, dims_order=dims_order)
    Gl_mix = f.evaluate_properties_of_mixing(state_spec, dimensions, constants, properties_to_evaluate=props_l,
                                             mole_fractions=True, dims_order=dims_order)

    Gv = f.evaluate_properties_1p(state_spec, dimensions, constants, properties_to_evaluate=props_v,
                                  mole_fractions=True, dims_order=dims_order)
    Gv_mix = f.evaluate_properties_of_mixing(state_spec, dimensions, constants, properties_to_evaluate=props_v,
                                             mole_fractions=True, dims_order=dims_order)

    p_idx, T_idx = 0, 0
    pres, temp = dimensions["pressure"][p_idx], dimensions["temperature"][T_idx]
    xH2S = dimensions['H2S']
    G = [Gl.isel(temperature=T_idx, pressure=p_idx).G_TP.values,
         Gv.isel(temperature=T_idx, pressure=p_idx).G_TP.values,
         ]
    Gmix = [Gl_mix.isel(temperature=T_idx, pressure=p_idx).G_TP.values,
            Gv_mix.isel(temperature=T_idx, pressure=p_idx).G_TP.values,
            ]

    gibbs_plot = Plot(nrows=2, figsize=(10, 8), sharex=True)
    gibbs_plot.add_attributes(suptitle=r"H$_2$S-C$_1$ mixture at P = {} bar and T = {} K".format(pres, temp))

    gibbs_plot.subplot_idx = 0
    gibbs_plot.draw_plot([xH2S, xH2S], ydata=G, number_of_curves=2,
                         datalabels=["L", "V"])
    gibbs_plot.add_attributes(title="G", ax_labels=["xH2S [-]", "G/RT"], grid=True, legend=True)

    gibbs_plot.subplot_idx = 1
    gibbs_plot.draw_plot([xH2S, xH2S], ydata=Gmix, number_of_curves=2,
                         datalabels=["L", "V"])
    gibbs_plot.add_attributes(title=r"G$_{mix}$", ax_labels=["xH2S [-]", r"G$_{mix}$/RT"], grid=True, legend=True)

plt.show()
