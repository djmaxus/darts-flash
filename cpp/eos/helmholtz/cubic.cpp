#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <complex>
#include <numeric>

#include "dartsflash/eos/helmholtz/cubic.hpp"
#include "dartsflash/global/global.hpp"
#include "dartsflash/global/components.hpp"
#include "dartsflash/maths/maths.hpp"

CubicEoS::CubicEoS(CompData& comp_data, CubicEoS::CubicType type) : HelmholtzEoS(comp_data)
{
    // Define Cubic EoS version specific parameters
    double omegaA = 0.0;
    double omegaB = 0.0;
    std::vector<double> kappa(comp_data.nc);

    // Peng-Robinson EoS
    if (type == CubicEoS::PR)
    {
        this->d1 = 1. + std::sqrt(2.);
        this->d2 = 1. - std::sqrt(2.);
        double eta_c = 1. / (1. + std::cbrt(4.-std::sqrt(8.)) + std::cbrt(4.+std::sqrt(8.)));
        omegaA = (8. + 40.*eta_c) / (49. - 37.*eta_c);  // ~= 0.45724
        omegaB = eta_c / (3. + eta_c);  // ~= 0.0778

        // Calculate PR kappa
        for (int i = 0; i < comp_data.nc; i++)
	    {
            if (comp_data.ac[i] <= 0.49)
	    	{
		    	kappa[i] = 0.37464 + 1.54226 * comp_data.ac[i] - 0.26992 * std::pow(comp_data.ac[i], 2);
    		}
            else
		    {
			    kappa[i] = 0.379642 + 1.48503 * comp_data.ac[i] - 0.164423 * std::pow(comp_data.ac[i], 2) + 0.016667 * std::pow(comp_data.ac[i], 3);
    		}
	    }
    }
    // Soave-Redlich-Kwong EoS
    else if (type == CubicEoS::SRK)
    {
        this->d1 = 1.;
        this->d2 = 0.;
        omegaA = 1. / (9. * (std::cbrt(2.) - 1.));  // ~= 0.42748
        omegaB = (std::cbrt(2.) - 1.) / 3.;  // ~= 0.08664

        // Calculate SRK kappa
        for (int i = 0; i < comp_data.nc; i++)
    	{
            kappa[i] = 0.48 + 1.574 * comp_data.ac[i] - 0.176 * std::pow(comp_data.ac[i], 2);
	    }
    }

    // Initialize Mixing Rule object
	this->mix = new Mix(comp_data, omegaA, omegaB, kappa);
}

CubicEoS::CubicEoS(CompData& comp_data, CubicParams& cubic_params) : HelmholtzEoS(comp_data)
{
    d1 = cubic_params.d1;
    d2 = cubic_params.d2;
    mix = cubic_params.mix->getCopy();
}

CubicParams::CubicParams(double d1_, double d2_, double omegaA, double omegaB, std::vector<double>& kappa, CompData& comp_data)
{
    this->d1 = d1_;
    this->d2 = d2_;

    // Initialize Mixing Rule object
	this->mix = new Mix(comp_data, omegaA, omegaB, kappa);
}

void CubicEoS::init_TV(double T_, double V_)
{
    this->v = V_;

    // Only recalculate if T is different
    if (T_ != T)
    {
        this->T = T_;

        // Calculate ai, aij, bi and bij mixing rule parameters, ignore output
        (void) mix->aij(T);
        (void) mix->bij();
    }
    return;
}
void CubicEoS::init_TP(double p_, double T_) 
{
    this->p = p_;
    // this->p = this->units.input_to_pa(p_);

    // Only recalculate if T is different
    if (T_ != T)
    {
        this->T = T_;

        // Calculate ai, aij, bi and bij mixing rule parameters, ignore output
        (void) mix->aij(T);
        (void) mix->bij();
    }
    return;
}

std::vector<std::complex<double>> CubicEoS::Z()
{
    // Calculate A and B coefficients for cubic
    double a_mix = mix->D()/std::pow(N, 2);
    double b_mix = mix->B()/N;
    this->A = a_mix * p * std::pow(T, -2);
    this->B = b_mix * p * std::pow(T, -1);

    // Find roots of cubic EoS: f(Z) = Z^3 + a2 Z^2 + a1 Z + a0 = 0
    double a2 = (d1 + d2 - 1.) * B - 1.;
    double a1 = A + d1 * d2 * std::pow(B, 2) - (d1 + d2) * B * (B + 1.);
    double a0 = - (A * B + d1 * d2 * std::pow(B, 2) * (B + 1.));
    return cubic_roots_analytical(a2, a1, a0);
}

double CubicEoS::V() {
    // Check if three real roots
    this->is_stable_root = true;
    this->is_preferred_root = RootSelect::ACCEPT;
    if (Z_roots[2].imag() != 0.)
    {
        // only one real root
        z = Z_roots[0].real();

        if (!(root == RootFlag::STABLE))
        {
            if (!((Z_roots[1].real() > 0.) 
                   && ((root == RootFlag::MIN && z <= Z_roots[1].real()) 
                    || (root == RootFlag::MAX && z >= Z_roots[1].real()))))
            {
                this->is_preferred_root = RootSelect::REJECT;
            }
        }
    }
    else
    {
        // Find zmin and zmax
        double z_min = Z_roots[0].real();
        if (Z_roots[1].real() < z_min) { z_min = Z_roots[1].real(); }
        if (Z_roots[2].real() < z_min) { z_min = Z_roots[2].real(); }
        double z_max = Z_roots[0].real();
        if (Z_roots[1].real() > z_max) { z_max = Z_roots[1].real(); }
        if (Z_roots[2].real() > z_max) { z_max = Z_roots[2].real(); }

        if ((mix->B() > 0. && z_min * N * T / p <= mix->B()) || (mix->B() < 0. && z_min * N * T / p >= mix->B()))
        {
            // Volume of smallest root smaller than co-volume parameter B
            z = z_max;
        }
        else 
        {
            // Calculate Gibbs energy of min (L) and max (V) roots
            double gE_l = (z_min - 1.) - std::log(z_min - B) - A / (B * (d1 - d2)) * std::log((z_min + d1 * B) / (z_min + d2 * B));
            double gE_v = (z_max - 1.) - std::log(z_max - B) - A / (B * (d1 - d2)) * std::log((z_max + d1 * B) / (z_max + d2 * B));
            if (root == RootFlag::STABLE)
            {
                z = gE_v < gE_l ? z_max : z_min;
            }
            else
            {
                z = (root == RootFlag::MIN) ? z_min : z_max;  // min (L) or max (V) root
                this->is_stable_root = ((root == RootFlag::MAX && gE_v < gE_l) || (root == RootFlag::MIN && gE_l < gE_v)) ? true : false;
            }
        }
    }
    v = z * N * this->units.R * T / p;

    return v;
}

// Calculate derivatives of Helmholtz function
double CubicEoS::F() 
{
    return -N * g - mix->D()/T * f;
}
double CubicEoS::dF_dV() 
{
    return this->F_V() / this->units.R;
}
double CubicEoS::dF_dT() 
{
    return this->F_T() + this->F_D() * mix->DT();
}
double CubicEoS::dF_dni(int i) 
{
    return this->F_n() + this->F_B() * mix->Bi(i) + this->F_D() * mix->Di(i);
}
double CubicEoS::d2F_dnidnj(int i, int j) 
{
    return this->F_nB() * (mix->Bi(i) + mix->Bi(j)) + this->F_BD()*(mix->Bi(i) * mix->Di(j) + mix->Bi(j) * mix->Di(i))
        + this->F_B() * mix->Bij(i, j) + this->F_BB() * mix->Bi(i) * mix->Bi(j) + this->F_D() * mix->Dij(i, j);
}
double CubicEoS::d2F_dTdni(int i) 
{
    return (this->F_BT() + this->F_BD() * mix->DT()) * mix->Bi(i) + this->F_DT() * mix->Di(i) + this->F_D() * mix->DiT(i);
}
double CubicEoS::d2F_dVdni(int i) 
{
    return (this->F_nV() + this->F_BV() * mix->Bi(i) + this->F_DV() * mix->Di(i)) / this->units.R;
}
double CubicEoS::d2F_dTdV() 
{
    return (this->F_TV() + this->F_DV() * mix->DT()) / this->units.R;
}
double CubicEoS::d2F_dV2() 
{
    return this->F_VV() / std::pow(this->units.R, 2);
}
double CubicEoS::d2F_dT2() 
{
    return this->F_TT() + 2*this->F_DT() * mix->DT() + this->F_D() * mix->DTT();
}

// Elements of Helmholtz function and derivatives
// Zero'th order: g, f, B, D
void CubicEoS::zeroth_order(std::vector<double>::iterator n_it)
{
    // Zero'th order parameters for P, T, n specification
    // Mixing rules
    mix->zeroth_order(n_it);

    // Calculate compressibility roots
    this->Z_roots = this->Z();

    // Calculate V(P, T, n) and zero'th order parameters
    this->zeroth_order(this->V());
}
void CubicEoS::zeroth_order(std::vector<double>::iterator n_it, double V_)
{
    // Zero'th order parameters for (T, V, n) specification
    // Mixing rules
    mix->zeroth_order(n_it);
    this->zeroth_order(V_);
}
void CubicEoS::zeroth_order(double V_)
{
    // Elements of Helmholtz function and derivatives
    // Zero'th order: g, f, B, D
    this->v = V_;
    this->Vr = V_/this->units.R;

    g = std::log(1. - mix->B()/Vr);
    f = 1./(mix->B() * (d1-d2)) * std::log((Vr + d1*mix->B())/(Vr + d2*mix->B()));
}

// First order: g_V, g_B, f_V, f_B, B_i, D_i, D_T
void CubicEoS::first_order(std::vector<double>::iterator n_it) 
{
    mix->first_order(n_it);

    g_V = mix->B() / (Vr * (Vr - mix->B()));
    g_B = -1. / (Vr - mix->B());
    f_V = -1. / ((Vr + d1 * mix->B()) * (Vr + d2 * mix->B()));
    f_B = -(f + Vr * f_V) / mix->B();
    return;
}
double CubicEoS::F_n() 
{
    return -g;
}
double CubicEoS::F_T() 
{
    return mix->D()/std::pow(T, 2) * f;
}
double CubicEoS::F_V() 
{
    return -N * g_V - mix->D()/T * f_V;
}
double CubicEoS::F_B() 
{
    return -N * g_B - mix->D()/T * f_B;
}
double CubicEoS::F_D() 
{
    return -1. * f/T;
}

// Second order:
void CubicEoS::second_order(std::vector<double>::iterator n_it) 
{
    this->first_order(n_it);
    mix->second_order(T, n_it);

    g_VV = -1. / std::pow((Vr - mix->B()), 2) + 1. / std::pow(Vr, 2);
    g_BV = 1. / std::pow((Vr - mix->B()), 2);
    g_BB = -1. / std::pow((Vr - mix->B()), 2);
    f_VV = 1. / (mix->B() * (d1 - d2)) * (-1. / std::pow(Vr + d1 * mix->B(), 2) + 1. / std::pow(Vr + d2 * mix->B(), 2));
    f_BV = -1. * (2. * f_V + Vr * f_VV) / mix->B();
    f_BB = -1. * (2. * f_B + Vr * f_BV) / mix->B();
    return;
}
double CubicEoS::F_nV() 
{
    return -g_V;
}
double CubicEoS::F_nB() 
{
    return -g_B;
}
double CubicEoS::F_TT() 
{
    return -2. * this->F_T()/T;
}
double CubicEoS::F_BT() 
{
    return mix->D() * f_B / std::pow(T, 2);
}
double CubicEoS::F_DT() 
{
    return f / std::pow(T, 2);
}
double CubicEoS::F_BV() 
{
    return -N * g_BV - mix->D()/T * f_BV;
}
double CubicEoS::F_BB() 
{
    return -N * g_BB - mix->D()/T * f_BB;
}
double CubicEoS::F_DV() 
{
    return -1. * f_V / T;
}
double CubicEoS::F_BD() 
{
    return -1. * f_B / T;
}
double CubicEoS::F_TV() 
{
    return mix->D() / std::pow(T, 2) * f_V;
}
double CubicEoS::F_VV() 
{
    return -N * g_VV - mix->D()/T * f_VV;
}

int CubicEoS::mix_dT_test(double T_, std::vector<double>& n, double tol)
{
    // Analytical derivatives of mixing rule w.r.t. temperature
    int error_output = 0;
    this->T = T_;
    double dT = 2e-3;

    // Calculate mixing rule parameters at p, T-dT, n
    std::vector<double> ai_ = mix->ai(T-dT);
    std::vector<double> aij_ = mix->aij(T-dT);

    // Calculate mixing rule parameters at p, T+dT, n
    std::vector<double> ai1 = mix->ai(T+dT);
    std::vector<double> aij1 = mix->aij(T+dT);

    // Calculate mixing rule parameters at p, T, n
    std::vector<double> ai0 = mix->ai(T);
    std::vector<double> aij0 = mix->aij(T);
    mix->second_order(T, n.begin());

    // Calculate derivatives of ai w.r.t. T
    for (int i = 0; i < nc; i++)
    {
        double dai_num = (ai1[i]-ai_[i])/(2*dT);
        double d = (dai_num - mix->dai_dT(T, i))/dai_num;
        if (std::fabs(d) > tol)
        {
            print("dai/dT", std::vector<double>{dai_num, mix->dai_dT(T, i), d});
            error_output++;
        }

        double d2ai_num = (ai1[i] - 2*ai0[i] + ai_[i])/std::pow(dT, 2);
        d = (d2ai_num - mix->d2ai_dT2(T, i))/d2ai_num;
        if (std::fabs(d) > tol)
        {
            print("d2ai/dT2", std::vector<double>{d2ai_num, mix->d2ai_dT2(T, i), d});
            error_output++;
        }
    }

    // Calculate derivatives of aij w.r.t. T
    for (int i = 0; i < nc; i++)
    {
        for (int j = 0; j < nc; j++)
        {
            int idx = i*nc + j;
            double daij_num = (aij1[idx]-aij_[idx])/(2*dT);
            double d = (daij_num - mix->daij_dT(T, i, j))/daij_num;
            if (std::fabs(d) > tol)
            {
                print("daij/dT", std::vector<double>{daij_num, mix->daij_dT(T, i, j), d});
                error_output++;
            }

            double d2aij_num = (aij1[idx] - 2*aij0[idx] + aij_[idx])/std::pow(dT, 2);
            d = (d2aij_num - mix->d2aij_dT2(T, i, j))/d2aij_num;
            if (std::fabs(d) > tol)
            {
                print("d2aij/dT2", std::vector<double>{d2aij_num, mix->d2aij_dT2(T, i, j), d});
                error_output++;
            }
        }
    }

    return error_output;
}
