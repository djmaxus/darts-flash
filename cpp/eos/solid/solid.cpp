#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

#include "dartsflash/global/global.hpp"
#include "dartsflash/global/components.hpp"
#include "dartsflash/global/ideal.hpp"
#include "dartsflash/maths/maths.hpp"
#include "dartsflash/eos/solid/solid.hpp"

namespace solid_par {
    double T_0{ 298.15 }; // reference temperature
    double P_0{ 1. }; // reference pressure [bar]
    double R{ 8.3145 };

    std::unordered_map<std::string, std::string> pure_comp = {{"Ice", "H2O"}, {"NaCl", "NaCl"}, {"CaCl2", "CaCl2"}, {"KCl", "KCl"}};

    std::unordered_map<std::string, double> gi0 = {{"Ice", -236539.24}, {"NaCl", -384138.00}, {"CaCl2", -748100.00}, {"KCl", -409140.00}};
    std::unordered_map<std::string, double> hi0 = {{"Ice", -292714.43}, {"NaCl", -411153.00}, {"CaCl2", -795800.00}, {"KCl", -436747.00}};
    std::unordered_map<std::string, double> v0 = {{"Ice", 19.7254}, {"NaCl", 26.9880}, {"CaCl2", 51.5270}, {"KCl", 37.5760}};
    std::unordered_map<std::string, double> kappa = {{"Ice", 1.3357E-5}, {"NaCl", 2.0000E-6}, {"CaCl2", 2.0000E-6}, {"KCl", 2.0000E-6}};

    std::unordered_map<std::string, std::vector<double>> cp = {
        {"Ice", {0.735409713, 1.4180551e-2, -1.72746e-5, 63.5104e-9}},
        {"NaCl", {5.526, 0.1963e-2, 0., 0.}},
        {"CaCl2", {8.646, 0.153e-2, 0., 0.}},
        {"KCl", {6.17, 0., 0., 0.}}
    };
    std::unordered_map<std::string, std::vector<double>> alpha = {
        {"Ice", {1.522300E-4, 1.660000E-8, 0.}},
        {"NaCl", {2.000000E-5, 0., 0.}},
        {"CaCl2", {2.000000E-5, 0., 0.}},
        {"KCl", {2.000000E-5, 0., 0.}}
    };

    double H::f(double T) {
        // molar enthalpy of pure phase [eq. 3.38], J/mol
        return (hi0[phase] / M_R
                + cp[phase][0] * (T-T_0) 
                + 1. / 2 * cp[phase][1] * (std::pow(T, 2)-std::pow(T_0, 2)) 
                + 1. / 3 * cp[phase][2] * (std::pow(T, 3)-std::pow(T_0, 3)) 
                + 1. / 4 * cp[phase][3] * (std::pow(T, 4)-std::pow(T_0, 4))) / std::pow(T, 2);
    }

    double H::F(double T) {
        // Integral of h(T)/RT^2 dT from T_0 to T
        return -(hi0[phase] / M_R
                - cp[phase][0] * T_0 
                - 1. / 2 * cp[phase][1] * std::pow(T_0, 2) 
                - 1. / 3 * cp[phase][2] * std::pow(T_0, 3)
                - 1. / 4 * cp[phase][3] * std::pow(T_0, 4)) * (1./T - 1./T_0)
                + cp[phase][0] * (std::log(T) - std::log(T_0))
                + 1. / 2 * cp[phase][1] * (T - T_0)
                + 1. / 6 * cp[phase][2] * (std::pow(T, 2) - std::pow(T_0, 2))
                + 1. / 12 * cp[phase][3] * (std::pow(T, 3) - std::pow(T_0, 3));
    }

    double H::dFdT(double T) {        
        return this->f(T);
    }

    double V::f(double p, double T) {
        // molar volume of pure phase [eq. 3.39], m3/mol
        double a = alpha[phase][0] * (T-T_0) 
                + alpha[phase][1] * std::pow((T-T_0), 2) 
                + alpha[phase][2] * std::pow((T-T_0), 3);
        double b = - kappa[phase];
        return v0[phase]*1e-6 * std::exp(a+b*(p-P_0)) / T;
    }

    double V::F(double p, double T) {
        // int e^cx dx = 1/c e^cx
        // int v0 exp(a + b*p)/RT dp = v0 exp(a)/RT * int exp(b*p) dp = v0 exp(a)/bRT * exp(b*p)
        double a = alpha[phase][0] * (T-T_0) 
                + alpha[phase][1] * std::pow((T-T_0), 2) 
                + alpha[phase][2] * std::pow((T-T_0), 3);
        double b = -kappa[phase];
        return v0[phase]*1e-6 * std::exp(a-b*P_0)/T * (std::exp(b*p) / b - std::exp(b*P_0)/b);
    }

    double V::dFdP(double p, double T) {
        return this->f(p, T);
    }

    double V::dFdT(double p, double T) {
        double a = alpha[phase][0] * (T-T_0) 
                + alpha[phase][1] * std::pow(T-T_0, 2) 
                + alpha[phase][2] * std::pow(T-T_0, 3);
        double b = -kappa[phase];

        double da_dT = alpha[phase][0] 
                    + 2*alpha[phase][1] * (T-T_0) 
                    + 3*alpha[phase][2] * std::pow(T-T_0, 2);
        
        double d_dT = (T * da_dT * std::exp(a-b*P_0) - std::exp(a-b*P_0)) / std::pow(T, 2);
        
        return v0[phase]*1e-6 * d_dT * (std::exp(b*p) / b - std::exp(b*P_0) / b);
    }
}

PureSolid::PureSolid(CompData& comp_data, std::string phase_) : EoS(comp_data)
{
    this->phase = phase_;
    std::string pure_comp = solid_par::pure_comp[phase];
    this->pure_comp_idx = std::distance(compdata.components.begin(), std::find(compdata.components.begin(), compdata.components.end(), pure_comp));
}

void PureSolid::init_TP(double p_, double T_) {
    if (p_ != p || T_ != T)
    {
        this->p = p_; this->T = T_;

        // ideal gas Gibbs energy
        double gio = this->compdata.ideal.gi(pure_comp_idx);
        double hio = this->compdata.ideal.Hi(T, pure_comp_idx);  // eq. 3.3
        double gi0 = gio - hio;  // eq. 3.2

        // Gibbs energy of ions in aqueous phase
        solid_par::H h = solid_par::H(phase);
        solid_par::V v = solid_par::V(phase);
        double gs0 = solid_par::gi0[phase] / (M_R * solid_par::T_0);
        double hs = h.F(T);
        double vs = v.F(p, T);
        double gs = gs0 - hs + vs;
        
        // Calculate fugacity coefficient
        lnfS = gs - gi0;
    }
}

void PureSolid::solve_TP(std::vector<double>::iterator n_it, bool second_order) {
    (void) n_it;
    (void) second_order;
    return;
}

double PureSolid::P(double T_, double V_, std::vector<double>& n)
{
    // Find pressure at given (T, V, n)
    this->p = 1.;
    double v = V_;

    // Newton loop to find root
    while (true)
    {
        double res = this->V(p, T_, n) - v;
        double dres_dp = this->dV_dP(p, T_, n);
        p -= res/dres_dp;

        if (std::fabs(res) < 1e-14)
        {
            break;
        }
    }
    return p;
}

double PureSolid::V(double p_, double T_, std::vector<double>& n)
{
    // Calculate volume at (P, T, n)
    (void) n;
    solid_par::V v = solid_par::V(phase);
    return v.F(p_, T_);
}

double PureSolid::dV_dP(double p_, double T_, std::vector<double>& n)
{
    // Calculate pressure derivative of volume at (P, T, n)
    (void) n;
    solid_par::V v = solid_par::V(phase);
    return v.dFdP(p_, T_);
}

double PureSolid::dV_dT(double p_, double T_, std::vector<double>& n)
{
    // Calculate temperature derivative of volume at (P, T, n)
    (void) n;
    solid_par::V v = solid_par::V(phase);
    return v.dFdT(p_, T_);
}

double PureSolid::dV_dni(double p_, double T_, std::vector<double>& n, int i)
{
    // Calculate temperature derivative of volume at (P, T, n)
    (void) p_;
    (void) T_;
    (void) n;
    (void) i;
    return 0.;
}

double PureSolid::lnphii(int i) 
{
    if (i == pure_comp_idx)
    {
        return lnfS - std::log(p);
    }
    else
    {
        return 1e20;
    }
}

std::vector<double> PureSolid::dlnphi_dP()
{
    solid_par::V v = solid_par::V(phase);
    double dVs_dP = v.dFdP(p, T);
    
    dlnphidP = std::vector<double>(nc, 0.);
    dlnphidP[pure_comp_idx] = dVs_dP - 1./p;
    return dlnphidP;
}

std::vector<double> PureSolid::dlnphi_dT() {
    solid_par::H h = solid_par::H(phase);
    solid_par::V v = solid_par::V(phase);
    double dHs_dT = h.dFdT(T);
    double dVs_dT = v.dFdT(p, T);

    double dHi_dT = this->compdata.ideal.dHidT(T, pure_comp_idx);

    dlnphidT = std::vector<double>(nc, 0.);
    dlnphidT[pure_comp_idx] = -dHs_dT + dVs_dT + dHi_dT;
    return dlnphidT;
}

std::vector<double> PureSolid::dlnphi_dn() {
    return std::vector<double>(nc, 0.);
}

int PureSolid::pvt_test(double p_, double T_, std::vector<double>& n, double tol)
{
    // Consistency of PVT: Calculate volume at (P, T, n) and find P at (T, V, n)
    int error_output = 0;

    // Calculate volume at P, T, n
    double v = this->V(p_, T_, n);

    // Evaluate P(T,V,n)
    double pp = this->P(T_, v, n);
    if (std::fabs(pp - p_) > tol)
    {
        print("P(T, V, n) != p", std::vector<double>{pp, p_, std::fabs(pp-p_)});
        error_output++;
    }
    
    return error_output;
}