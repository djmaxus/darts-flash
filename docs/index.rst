.. 
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to openDARTS-flash documentation!
#########################################

.. toctree::
   :maxdepth: 2
   :caption: About DARTS-flash

   about_darts-flash/about.md
   
.. toctree::
   :maxdepth: 2
   :caption: GETTING STARTED

   getting_started/installation.md
   getting_started/tutorial.md

.. toctree::
   :maxdepth: 2
   :caption: FOR DEVELOPERS
   
   for_developers/build_instructions.md
   for_developers/gitlab_setup.md

.. toctree::
   :maxdepth: 2
   :caption: API

   api.rst


   
