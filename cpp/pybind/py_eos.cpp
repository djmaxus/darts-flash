#include <pybind11/pybind11.h>
#include <pybind11/stl_bind.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <complex>

#include "dartsflash/eos/eos.hpp"
#include "dartsflash/eos/initial_guess.hpp"
#include "dartsflash/eos/trial_phase.hpp"
#include "dartsflash/eos/helmholtz/helmholtz.hpp"
#include "dartsflash/eos/helmholtz/cubic.hpp"
#include "dartsflash/eos/aq/aq.hpp"
#include "dartsflash/eos/aq/jager.hpp"
#include "dartsflash/eos/aq/ziabakhsh.hpp"
#include "dartsflash/eos/vdwp/vdwp.hpp"
#include "dartsflash/eos/vdwp/ballard.hpp"
#include "dartsflash/eos/vdwp/munck.hpp"
#include "dartsflash/eos/solid/solid.hpp"

namespace py = pybind11;

template <class EoSBase = EoS> class PyEoS : public EoSBase {
public:
    using EoSBase::EoSBase;

    EoS* getCopy() override {
        PYBIND11_OVERRIDE_PURE(
            EoS*, EoS, getCopy,
        );
    }

    EoS::RootSelect select_root(std::vector<double>::iterator n_it) override {
        PYBIND11_OVERRIDE(
            EoS::RootSelect, EoS, select_root, n_it
        );
    }

    void init_TP(double p_, double T_) override {
        PYBIND11_OVERRIDE_PURE(
            void, EoS, init_TP, p_, T_
        );
    }
    void solve_TP(std::vector<double>::iterator n_it, bool second_order) override {
        PYBIND11_OVERRIDE_PURE(
            void, EoS, solve_TP, n_it, second_order
        );
    }

    double lnphii(int i) override {
        PYBIND11_OVERRIDE_PURE(
            double, EoS, lnphii, i
        );
    }
    double dlnphii_dP(int i) override {
        PYBIND11_OVERRIDE(
            double, EoS, dlnphii_dP, i
        );
    }
	double dlnphii_dT(int i) override {
        PYBIND11_OVERRIDE(
            double, EoS, dlnphii_dT, i
        );
    }
    double dlnphii_dnj(int i, int j) override {
        PYBIND11_OVERRIDE(
            double, EoS, dlnphii_dnj, i, j
        );
    }

    std::vector<double> dlnphi_dP() override {
        PYBIND11_OVERRIDE(
            std::vector<double>, EoS, dlnphi_dP
        );
    }
    std::vector<double> dlnphi_dT() override {
        PYBIND11_OVERRIDE(
            std::vector<double>, EoS, dlnphi_dT
        );
    }
    std::vector<double> dlnphi_dn() override {
        PYBIND11_OVERRIDE(
            std::vector<double>, EoS, dlnphi_dn
        );
    }

    double Gr_TP(double p_, double T_, std::vector<double>& n, int start_idx=0) override {
        PYBIND11_OVERRIDE(
            double, EoS, Gr_TP, p_, T_, n, start_idx
        );
    }
    double Hr_TP(double p_, double T_, std::vector<double>& n, int start_idx=0) override {
        PYBIND11_OVERRIDE(
            double, EoS, Hr_TP, p_, T_, n, start_idx
        );
    }
    double Sr_TP(double p_, double T_, std::vector<double>& n, int start_idx=0) override {
        PYBIND11_OVERRIDE(
            double, EoS, Sr_TP, p_, T_, n, start_idx
        );
    }
    // double Ar_TP(double p_, double T_, std::vector<double>& n, int start_idx) override {
    //     PYBIND11_OVERRIDE(
    //         double, EoS, Ar_TP, p_, T_, n, start_idx
    //     );
    // }
    // double Ur_TP(double p_, double T_, std::vector<double>& n, int start_idx) override {
    //     PYBIND11_OVERRIDE(
    //         double, EoS, Ur_TP, p_, T_, n, start_idx
    //     );
    // }
};

template <class HelmholtzEoSBase = HelmholtzEoS> class PyHelmholtz : public PyEoS<HelmholtzEoSBase> {
public:
    using PyEoS<HelmholtzEoSBase>::PyEoS; // inherit constructors

    EoS::RootSelect select_root(std::vector<double>::iterator n_it) override {
        PYBIND11_OVERRIDE(
            EoS::RootSelect, HelmholtzEoS, select_root, n_it
        );
    }

    void init_TV(double T_, double V_) override {
        PYBIND11_OVERRIDE_PURE(
            void, HelmholtzEoS, init_TV, T_, V_
        );
    }
    void zeroth_order(std::vector<double>::iterator n_it) override {
        PYBIND11_OVERRIDE_PURE(
            void, HelmholtzEoS, zeroth_order, n_it
        );
    }
    void zeroth_order(std::vector<double>::iterator n_it, double V_) override {
        PYBIND11_OVERRIDE_PURE(
            void, HelmholtzEoS, zeroth_order, n_it, V_
        );
    }
    void zeroth_order(double V_) override {
        PYBIND11_OVERRIDE_PURE(
            void, HelmholtzEoS, zeroth_order, V_
        );
    }
    void first_order(std::vector<double>::iterator n_it) override {
        PYBIND11_OVERRIDE_PURE(
            void, HelmholtzEoS, first_order, n_it
        );
    }
    void second_order(std::vector<double>::iterator n_it) override {
        PYBIND11_OVERRIDE_PURE(
            void, HelmholtzEoS, second_order, n_it
        );
    }
    
    double V() override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, V, 
        );
    }
    std::vector<std::complex<double>> Z() override {
        PYBIND11_OVERRIDE_PURE(
            std::vector<std::complex<double>>, HelmholtzEoS, Z, 
        );
    }

    double lnphii(int i) override {
        PYBIND11_OVERRIDE(
            double, HelmholtzEoS, lnphii, i
        );
    }
    double dlnphii_dP(int i) override {
        PYBIND11_OVERRIDE(
            double, HelmholtzEoS, dlnphii_dP, i
        );
    }
    double dlnphii_dT(int i) override {
        PYBIND11_OVERRIDE(
            double, HelmholtzEoS, dlnphii_dT, i
        );
    }
    double dlnphii_dnj(int i, int j) override {
        PYBIND11_OVERRIDE(
            double, HelmholtzEoS, dlnphii_dnj, i, j
        );
    }

    double F() override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, F, 
        );
    }
    double dF_dV() override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, dF_dV, 
        );
    }
    double dF_dT() override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, dF_dT, 
        );
    }
    double dF_dni(int i) override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, dF_dni, i
        );
    }
    double d2F_dnidnj(int i, int j) override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, d2F_dnidnj, i, j 
        );
    }
    double d2F_dTdni(int i) override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, d2F_dTdni, i 
        );
    }
    double d2F_dVdni(int i) override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, d2F_dVdni, i 
        );
    }
    double d2F_dTdV() override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, d2F_dTdV, 
        );
    }
    double d2F_dV2() override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, d2F_dV2, 
        );
    }
    double d2F_dT2() override {
        PYBIND11_OVERRIDE_PURE(
            double, HelmholtzEoS, d2F_dT2, 
        );
    }

    double Gr_TP(double p_, double T_, std::vector<double>& n, int start_idx) override {
        PYBIND11_OVERRIDE(
            double, HelmholtzEoS, Gr_TP, p_, T_, n, start_idx
        );
    }
    double Hr_TP(double p_, double T_, std::vector<double>& n, int start_idx) override {
        PYBIND11_OVERRIDE(
            double, HelmholtzEoS, Hr_TP, p_, T_, n, start_idx
        );
    }
    double Sr_TP(double p_, double T_, std::vector<double>& n, int start_idx) override {
        PYBIND11_OVERRIDE(
            double, HelmholtzEoS, Sr_TP, p_, T_, n, start_idx
        );
    }
    // double Ar_TP(double p_, double T_, std::vector<double>& n, int start_idx) override {
    //     PYBIND11_OVERRIDE(
    //         double, HelmholtzEoS, Ar_TP, p_, T_, n, start_idx
    //     );
    // }
    // double Ur_TP(double p_, double T_, std::vector<double>& n, int start_idx) override {
    //     PYBIND11_OVERRIDE(
    //         double, HelmholtzEoS, Ur_TP, p_, T_, n, start_idx
    //     );
    // }
};

template <class CubicEoSBase = CubicEoS> class PyCubic : public PyHelmholtz<CubicEoSBase> {
public:
    using PyHelmholtz<CubicEoSBase>::PyHelmholtz;

    void zeroth_order(std::vector<double>::iterator n_it) override {
        PYBIND11_OVERRIDE(
            void, CubicEoS, zeroth_order, n_it
        );
    }
    void zeroth_order(std::vector<double>::iterator n_it, double V_) override {
        PYBIND11_OVERRIDE(
            void, CubicEoS, zeroth_order, n_it, V_
        );
    }
    void zeroth_order(double V_) override {
        PYBIND11_OVERRIDE(
            void, CubicEoS, zeroth_order, V_
        );
    }
    void first_order(std::vector<double>::iterator n_it) override {
        PYBIND11_OVERRIDE(
            void, CubicEoS, first_order, n_it
        );
    }
    void second_order(std::vector<double>::iterator n_it) override {
        PYBIND11_OVERRIDE(
            void, CubicEoS, second_order, n_it
        );
    }

    double V() override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, V, 
        );
    }
    std::vector<std::complex<double>> Z() override {
        PYBIND11_OVERRIDE(
            std::vector<std::complex<double>>, CubicEoS, Z, 
        );
    }

    double F() override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, F, 
        );
    }
    double dF_dV() override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, dF_dV, 
        );
    }
    double dF_dT() override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, dF_dT, 
        );
    }
    double dF_dni(int i) override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, dF_dni, i
        );
    }
    double d2F_dnidnj(int i, int j) override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, d2F_dnidnj, i, j 
        );
    }
    double d2F_dTdni(int i) override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, d2F_dTdni, i 
        );
    }
    double d2F_dVdni(int i) override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, d2F_dVdni, i 
        );
    }
    double d2F_dTdV() override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, d2F_dTdV, 
        );
    }
    double d2F_dV2() override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, d2F_dV2, 
        );
    }
    double d2F_dT2() override {
        PYBIND11_OVERRIDE(
            double, CubicEoS, d2F_dT2, 
        );
    }
};

template <class VdWPBase = VdWP> class PyVdWP : public PyEoS<VdWPBase> {
public:
    using PyEoS<VdWPBase>::PyEoS;

    double lnphii(int i) override {
        PYBIND11_OVERRIDE_PURE(
            double, VdWP, lnphii, i
        );
    }

    double V(double p_, double T_, std::vector<double>& n) override {
        PYBIND11_OVERRIDE_PURE(
            double, VdWP, V, p_, T_, n
        );
    }

    double fw(std::vector<double>& fi) override {
        PYBIND11_OVERRIDE_PURE(
            double, VdWP, fw, fi
        );
    }
    double dfw_dP(std::vector<double>& dfidP) override {
        PYBIND11_OVERRIDE_PURE(
            double, VdWP, dfw_dP, dfidP
        );
    }
    double dfw_dT(std::vector<double>& dfidT) override {
        PYBIND11_OVERRIDE_PURE(
            double, VdWP, dfw_dT, dfidT
        );
    }
    std::vector<double> dfw_dxj(std::vector<double>& dfidxj) override {
        PYBIND11_OVERRIDE_PURE(
            std::vector<double>, VdWP, dfw_dxj, dfidxj
        );
    }

    std::vector<double> calc_Ckm() override {
        PYBIND11_OVERRIDE_PURE(
            std::vector<double>, VdWP, calc_Ckm
        );
    }
    std::vector<double> dCkm_dP() override {
        PYBIND11_OVERRIDE_PURE(
            std::vector<double>, VdWP, dCkm_dP
        );
    }
    std::vector<double> dCkm_dT() override {
        PYBIND11_OVERRIDE_PURE(
            std::vector<double>, VdWP, dCkm_dT
        );
    }

    int dP_test(double p_, double T_, std::vector<double>& n_, double tol) override {
        PYBIND11_OVERRIDE(
            int, VdWP, dP_test, p_, T_, n_, tol
        );
    }
    int dT_test(double p_, double T_, std::vector<double>& n_, double tol) override {
        PYBIND11_OVERRIDE(
            int, VdWP, dT_test, p_, T_, n_, tol
        );
    }
};

template <class AQBaseBase = AQBase> class PyAQ : AQBaseBase {
public:
    using AQBaseBase::AQBaseBase;

    AQBase* getCopy() override {
        PYBIND11_OVERRIDE_PURE(
            AQBase*, AQBase, getCopy,
        );
    }

    void init_TP(double p_, double T_, AQEoS::CompType comp_type) override {
        PYBIND11_OVERRIDE_PURE(
            void, AQBase, init_TP, p_, T_, comp_type
        );
    }
    void solve_TP(std::vector<double>& x_, bool second_order, AQEoS::CompType comp_type) override {
        PYBIND11_OVERRIDE_PURE(
            void, AQBase, solve_TP, x_, second_order, comp_type
        );
    }

    double lnphii(int i) override {
        PYBIND11_OVERRIDE_PURE(
            double, AQBase, lnphii, i
        );
    }
    double dlnphii_dP(int i) override {
        PYBIND11_OVERRIDE_PURE(
            double, AQBase, dlnphii_dP, i
        );
    }
    double dlnphii_dT(int i) override {
        PYBIND11_OVERRIDE_PURE(
            double, AQBase, dlnphii_dT, i
        );
    }
    double dlnphii_dxj(int i, int j) override {
        PYBIND11_OVERRIDE_PURE(
            double, AQBase, dlnphii_dxj, i, j
        );
    }
};

void pybind_eos(py::module& m)
{
    using namespace pybind11::literals;  // bring in '_a' literal
    
    // Expose EoS derived classes from EoS base class
    py::class_<EoS, PyEoS<>> eos(m, "EoS", R"pbdoc(
            This is a base class for Equations of State (EoS).

            Each EoS child class overrides the methods for:
            - (P,T)-dependent parameters in `init_TP(p, T)`
            - (n)-dependent parameters in `solve_TP(n)`
            - Expressions for `lnphi(i)` and derivatives w.r.t. P, T, nj
            - Expressions for Gibbs free energy `Gr` and enthalpy `Hr`
            )pbdoc");

    py::enum_<EoS::RootFlag>(eos, "RootFlag", "Flag for roots to be selected in EoS")
        .value("STABLE", EoS::RootFlag::STABLE)
        .value("MIN", EoS::RootFlag::MIN)
        .value("MAX", EoS::RootFlag::MAX)
        .export_values()
        ;

    eos.def(py::init<CompData&>(), R"pbdoc(
            This is the constructor of the EoS base class for multicomponent phases.

            :param comp_data: Component data
            :type comp_data: CompData
            )pbdoc", "comp_data"_a)

        .def("set_eos_range", &EoS::set_eos_range, R"pbdoc(
            Specify composition range for EoS
            
            :param i: Component index
            :type i: int
            :param range: Composition lower and upper bound
            :type range: list
            )pbdoc", "i"_a, "range"_a)

        .def("solve_TP", py::overload_cast<double, double, std::vector<double>&, int, bool>(&EoS::solve_TP), R"pbdoc(
            Solve EoS at given PTn
            
            :param p: Pressure
            :type p: double
            :param T: Temperature
            :type T: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0]
            :type start_idx: int
            :param second_order: Switch for second order parameters
            :type second_order: bool
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0, py::arg("second_order")=true)
        
        .def("lnphi", &EoS::lnphi, R"pbdoc(
            :returns: List of lnphi for each component at (P,T,n)
            :rtype: list
            )pbdoc")
        .def("dlnphi_dP", &EoS::dlnphi_dP, R"pbdoc(
            :returns: List of dlnphi/dP for each component at (P,T,n)
            :rtype: list
            )pbdoc")
        .def("dlnphi_dT", &EoS::dlnphi_dT, R"pbdoc(
            :returns: List of dlnphi/dT for each component at (P,T,n)
            :rtype: list
            )pbdoc")
        .def("dlnphi_dn", &EoS::dlnphi_dn, R"pbdoc(
            :returns: List of dlnphi/dn for each component at (P,T,n)
            :rtype: list
            )pbdoc")
        .def("fugacity", &EoS::fugacity, R"pbdoc(
            Calculate mixture fugacity at (P,T,x)

            :param p: Pressure
            :type p: double
            :param T: Temperature
            :type T: double
            :param x: Composition
            :type x: list
            :returns: List of component fugacities
            :rtype: list
            )pbdoc", "p"_a, "T"_a, "x"_a)

        .def("G_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::G_TP), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            :returns: Total Gibbs free energy Gr of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        .def("H_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::H_TP), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            :returns: Total enthalpy Hr of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        // .def("S_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::S_TP), R"pbdoc(
        //     :param p: Pressure
        //     :type p: double
        //     :param p: Temperature
        //     :type p: double
        //     :param n: Composition
        //     :type n: list
        //     :param start_idx: Index of n[0], default is 0
        //     :type start_idx: int
        //     :returns: Total entropy Sr of mixture at (P,T,n)
        //     :rtype: double
        //     )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        // .def("A_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::A_TP), R"pbdoc(
        //     :param p: Pressure
        //     :type p: double
        //     :param p: Temperature
        //     :type p: double
        //     :param n: Composition
        //     :type n: list
        //     :param start_idx: Index of n[0], default is 0
        //     :type start_idx: int
        //     :returns: Total Helmholtz free energy Ar of mixture at (P,T,n)
        //     :rtype: double
        //     )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        // .def("U_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::U_TP), R"pbdoc(
        //     :param p: Pressure
        //     :type p: double
        //     :param p: Temperature
        //     :type p: double
        //     :param n: Composition
        //     :type n: list
        //     :param start_idx: Index of n[0], default is 0
        //     :type start_idx: int
        //     :returns: Total internal energy Ur of mixture at (P,T,n)
        //     :rtype: double
        //     )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)

        .def("Gr_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::Gr_TP), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            :returns: Residual Gibbs free energy Gr of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        .def("Hr_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::Hr_TP), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            :returns: Residual enthalpy Hr of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        .def("Sr_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::Sr_TP), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            :returns: Residual entropy Sr of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        // .def("Ar_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::Ar_TP), R"pbdoc(
        //     :param p: Pressure
        //     :type p: double
        //     :param p: Temperature
        //     :type p: double
        //     :param n: Composition
        //     :type n: list
        //     :param start_idx: Index of n[0], default is 0
        //     :type start_idx: int
        //     :returns: Residual Helmholtz free energy Ar of mixture at (P,T,n)
        //     :rtype: double
        //     )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        // .def("Ur_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::Ur_TP), R"pbdoc(
        //     :param p: Pressure
        //     :type p: double
        //     :param p: Temperature
        //     :type p: double
        //     :param n: Composition
        //     :type n: list
        //     :param start_idx: Index of n[0], default is 0
        //     :type start_idx: int
        //     :returns: Residual internal energy Ur of mixture at (P,T,n)
        //     :rtype: double
        //     )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)

        .def("Gri_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::Gri_TP), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            :returns: Partial molar Gibbs free energy Gr of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        .def("Hri_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::Hri_TP), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            :returns: Partial molar enthalpy Hr of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        .def("Sri_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::Sri_TP), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            :returns: Partial molar entropy Sr of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        // .def("Ari_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::Ari_TP), R"pbdoc(
        //     :param p: Pressure
        //     :type p: double
        //     :param p: Temperature
        //     :type p: double
        //     :param n: Composition
        //     :type n: list
        //     :param start_idx: Index of n[0], default is 0
        //     :type start_idx: int
        //     :returns: Partial molar Helmholtz free energy Ar of mixture at (P,T,n)
        //     :rtype: double
        //     )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        // .def("Uri_TP", py::overload_cast<double, double, std::vector<double>&, int>(&EoS::Uri_TP), R"pbdoc(
        //     :param p: Pressure
        //     :type p: double
        //     :param p: Temperature
        //     :type p: double
        //     :param n: Composition
        //     :type n: list
        //     :param start_idx: Index of n[0], default is 0
        //     :type start_idx: int
        //     :returns: Partial molar internal energy Ur of mixture at (P,T,n)
        //     :rtype: double
        //     )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        ;

    py::class_<HelmholtzEoS, PyHelmholtz<>, EoS>(m, "HelmholtzEoS", R"pbdoc(
            This is a base class for Helmholtz-based EoS. 
            
            For reference, see Michelsen and Mollerup (2007) - Thermodynamic Models: Fundamentals & Computational Aspects.
            )pbdoc")
        .def(py::init<CompData&>(), R"pbdoc(
            This is the constructor of the HelmholtzEoS base class.

            :param comp_data: Component data
            :type comp_data: CompData
            )pbdoc", "comp_data"_a)

        .def("solve_TV", py::overload_cast<double, double, std::vector<double>&, int, bool>(&HelmholtzEoS::solve_TV), R"pbdoc(
            Solve EoS at given (T,V,n)
            
            :param T: Temperature
            :type T: double
            :param V: Volume
            :type V: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0]
            :type start_idx: int
            :param second_order: Switch for second order parameters
            :type second_order: bool
            )pbdoc", "T"_a, "V"_a, "n"_a, py::arg("start_idx")=0, py::arg("second_order")=true)

        .def("Z", py::overload_cast<>(&HelmholtzEoS::Z), R"pbdoc(
            :returns: Volume roots of mixture at (P,T,n)
            :rtype: list
            )pbdoc")
        .def("set_root_flag", &HelmholtzEoS::set_root_flag, R"pbdoc(
            :param flag: Root choice, 0/STABLE) lowest Gibbs energy, 1/MIN) minimum (L), 2/MAX) maximum (V); default is 0
            :type flag: EoS.RootFlag
            )pbdoc", "root_flag"_a)
        .def("set_preferred_roots", &HelmholtzEoS::set_preferred_roots, R"pbdoc(
            :param i: Component index
            :type i: int
            :param x: Mole fraction of specified component
            :type x: float
            :param root_flag: Root choice, 0/STABLE) lowest Gibbs energy, 1/MIN) minimum (L), 2/MAX) maximum (V); default is 0
            :type root_flag: EoS.RootFlag
            )pbdoc", "i"_a, "x"_a, "root_flag"_a)

        .def("P", py::overload_cast<double, double, std::vector<double>&>(&HelmholtzEoS::P), R"pbdoc(
            :param T: Temperature
            :type T: double
            :param V: Volume
            :type V: double
            :param n: Composition
            :type n: list
            :returns: Pressure of mixture at (T,V,n)
            :rtype: double
            )pbdoc", "T"_a, "V"_a, "n"_a)
        .def("V", py::overload_cast<double, double, std::vector<double>&>(&HelmholtzEoS::V), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param T: Temperature
            :type T: double
            :param n: Composition
            :type n: list
            :returns: Molar volume of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a)
        .def("Z", py::overload_cast<double, double, std::vector<double>&>(&HelmholtzEoS::Z), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param T: Temperature
            :type T: double
            :param n: Composition
            :type n: list
            :returns: Compressibility factor Z of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a)
        .def("rho", &HelmholtzEoS::rho, R"pbdoc(
            :param p: Pressure
            :type p: double
            :param T: Temperature
            :type T: double
            :param n: Composition
            :type n: list
            :returns: Mass density of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a)
        
        .def("Ar_TV", &HelmholtzEoS::Ar_TV, R"pbdoc(
            :param T: Temperature
            :type T: double
            :param V: Volume
            :type V: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0]
            :type start_idx: int

            :returns: Residual Helmholtz free energy Ar of mixture at (T,V,n)
            :rtype: double
            )pbdoc", "T"_a, "V"_a, "n"_a, py::arg("start_idx")=0)
        .def("Sr_TV", &HelmholtzEoS::Sr_TV, R"pbdoc(
            :param T: Temperature
            :type T: double
            :param V: Volume
            :type V: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0]
            :type start_idx: int
            
            :returns: Residual entropy Sr of mixture at (T,V,n)
            :rtype: double
            )pbdoc", "T"_a, "V"_a, "n"_a, py::arg("start_idx")=0)
        .def("Ur_TV", &HelmholtzEoS::Ur_TV, R"pbdoc(
            :param T: Temperature
            :type T: double
            :param V: Volume
            :type V: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0]
            :type start_idx: int

            :returns: Residual internal energy Ur of mixture at (T,V,n)
            :rtype: double
            )pbdoc", "T"_a, "V"_a, "n"_a, py::arg("start_idx")=0)
        .def("Hr_TV", &HelmholtzEoS::Hr_TV, R"pbdoc(
            :param T: Temperature
            :type T: double
            :param V: Volume
            :type V: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0]
            :type start_idx: int

            :returns: Residual enthalpy Hr of mixture at (T,V,n)
            :rtype: double
            )pbdoc", "T"_a, "V"_a, "n"_a, py::arg("start_idx")=0)
        .def("Gr_TV", &HelmholtzEoS::Gr_TV, R"pbdoc(
            :param T: Temperature
            :type T: double
            :param V: Volume
            :type V: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0]
            :type start_idx: int

            :returns: Residual Gibbs free energy Gr of mixture at (T,V,n)
            :rtype: double
            )pbdoc", "T"_a, "V"_a, "n"_a, py::arg("start_idx")=0)

        .def("Cvr", &HelmholtzEoS::Cvr, R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            
            :returns: Residual heat capacity at constant volume Cvr of mixture at (T,V,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        .def("Cv", &HelmholtzEoS::Cv, R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            
            :returns: Heat capacity at constant volume Cv of mixture at (T,V,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        .def("Cpr", &HelmholtzEoS::Cpr, R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int

            :returns: Residual heat capacity at constant pressure Cpr of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        .def("Cp", &HelmholtzEoS::Cp, R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int

            :returns: Heat capacity at constant pressure Cp of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        .def("vs", &HelmholtzEoS::vs, R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int
            
            :returns: Sound speed vs in mixture at (T,V,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        .def("JT", &HelmholtzEoS::JT, R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list
            :param start_idx: Index of n[0], default is 0
            :type start_idx: int

            :returns: Joule-Thomson coefficient of mixture at (P,T,n)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a, py::arg("start_idx")=0)
        ;

    py::class_<CubicEoS, PyCubic<>, HelmholtzEoS> cubic(m, "CubicEoS", R"pbdoc(
            This is a base class for cubic EoS. 
            
            For reference, see Michelsen and Mollerup (2007) - Thermodynamic Models: Fundamentals & Computational Aspects.
            )pbdoc");
    cubic.def(py::init<CompData&, CubicEoS::CubicType>(), R"pbdoc(
            This is the constructor of the HelmholtzEoS base class for predefined cubic parameters. 
            The user can provide `CubicEoS.PR` or `CubicEoS.SRK` in the argument list.

            :param comp_data: Component data
            :type comp_data: CompData
            :param cubic_type: Predefined cubic parameters, `CubicEoS.PR` (default) or `CubicEoS.SRK`
            )pbdoc", "comp_data"_a, "cubic_type"_a)
        .def(py::init<CompData&, CubicParams&>(), R"pbdoc(
            This is the constructor of the HelmholtzEoS base class for user-defined cubic parameters. 
            The user can provide a :class:`CubicParams` in the argument list.

            :param comp_data: Component data
            :type comp_data: CompData
            :param cubic_params: User-defined cubic parameters
            )pbdoc", "comp_data"_a, "cubic_params"_a)
        ;

    py::enum_<CubicEoS::CubicType>(cubic, "CubicType", "Predefined Cubic EoS")
        .value("PR", CubicEoS::CubicType::PR)
        .value("SRK", CubicEoS::CubicType::SRK)
        .export_values()
        ;

    py::class_<CubicParams>(m, "CubicParams",  R"pbdoc(
            This class allows the user to pass user-defined parameters for CubicEoS.
            )pbdoc")
        .def(py::init<double, double, double, double, std::vector<double>&, CompData&>(), R"pbdoc(
            :param d1: Cubic volumetric dependence of the attractive contribution d1
            :type d1: double
            :param d2: Cubic volumetric dependence of the attractive contribution d2
            :type d2: double
            :param omegaA: Cubic Omega parameter for attractive term a
            :type omegaA: double
            :param omegaB: Cubic Omega parameter for repulsive term b
            :type omegaB: double
            :param kappa: List of cubic linear part of alpha correlations
            :type kappa: list
            :param comp_data: Component data
            :type comp_data: CompData
            )pbdoc", "d1"_a, "d2"_a, "omegaA"_a, "omegaB"_a, "kappa"_a, "comp_data"_a)
        ;

    py::class_<AQEoS, PyEoS<AQEoS>, EoS> aqeos(m, "AQEoS", R"pbdoc(
            This class is a composition of multiple AQBase child classes.

            It allows the user to describe solvent (water), solutes and ions with different fugacity-activity models.
            )pbdoc");
    aqeos.def(py::init<CompData&>(), R"pbdoc()pbdoc", "comp_data"_a)
        .def(py::init<CompData&, AQEoS::Model>(), R"pbdoc(
            This constructor creates instance of specified AQBase model

            :param comp_data: Component data
            :type comp_data: CompData
            :param model: AQBase model type
            )pbdoc", "comp_data"_a, "model"_a)
        .def(py::init<CompData&, std::map<AQEoS::CompType, AQEoS::Model>&>(), R"pbdoc(
            This constructor creates instance of AQBase models specified in evaluator_map

            :param comp_data: Component data
            :type comp_data: CompData
            :param evaluator_map: Map of [CompType, Model]
            )pbdoc", "comp_data"_a, "evaluator_map"_a)
        .def(py::init<CompData&, std::map<AQEoS::CompType, AQEoS::Model>&, std::map<AQEoS::Model, AQBase*>&>(), R"pbdoc(
            This constructor creates copies of AQBase models passed in evaluators.
            
            :param comp_data: Component data
            :type comp_data: CompData
            :param evaluator_map: Map of [CompType, Model]
            :param evaluators: Map of [Model, AQBase*] for evaluation of fugacities
            )pbdoc", "comp_data"_a, "evaluator_map"_a, "evaluators"_a)
        ;

    py::enum_<AQEoS::Model>(aqeos, "Model", "Type of AQBase model")
        .value("Ziabakhsh2012", AQEoS::Model::Ziabakhsh2012)
        .value("Jager2003", AQEoS::Model::Jager2003)
        .export_values()
        ;

    py::enum_<AQEoS::CompType>(aqeos, "CompType", "Type of component")
        .value("water", AQEoS::CompType::water)
        .value("solute", AQEoS::CompType::solute)
        .value("ion", AQEoS::CompType::ion)
        .export_values()
        ;

    py::class_<AQBase, PyAQ<>>(m, "AQBase", R"pbdoc(
            This is a base class for aqueous phase fugacity-activity models.
            )pbdoc")
        .def(py::init<CompData&>(), R"pbdoc(
            :param comp_data: Component data
            :type comp_data: CompData
            )pbdoc", "comp_data"_a)
        ;

    py::class_<Ziabakhsh2012, PyAQ<Ziabakhsh2012>, AQBase>(m, "Ziabakhsh2012", R"pbdoc(
            This class evaluates Ziabakhsh and Kooi (2012) aqueous phase fugacity-activity model.
            )pbdoc")
        .def(py::init<CompData&>(), R"pbdoc(
            :param comp_data: Component data
            :type comp_data: CompData
            )pbdoc", "comp_data"_a)
        ;

    py::class_<Jager2003, PyAQ<Jager2003>, AQBase>(m, "Jager2003", R"pbdoc(
            This class evaluates Jager (2003) aqueous phase fugacity model.
            )pbdoc")
        .def(py::init<CompData&>(), R"pbdoc(
            :param comp_data: Component data
            :type comp_data: CompData
            )pbdoc", "comp_data"_a)
        ;

    py::class_<VdWP, PyVdWP<>, EoS>(m, "VdWP", R"pbdoc(
            This is a base class for hydrate Van der Waals-Platteeuw (1959) type EoS.
            )pbdoc")
        .def(py::init<CompData&, std::string>(), R"pbdoc(
            :param comp_data: Component data
            :type comp_data: CompData
            :param hydrate_type: Hydrate type (sI, sII, sH)
            :type hydrate_type: str
            )pbdoc", "comp_data"_a, "hydrate_type"_a)

        .def("V", &VdWP::V, R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param n: Composition
            :type n: list

            :returns: Hydrate molar volume at (P,T,x)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "n"_a)
        .def("fw", py::overload_cast<double, double, std::vector<double>&>(&VdWP::fw), R"pbdoc(
            :param p: Pressure
            :type p: double
            :param p: Temperature
            :type p: double
            :param f0: List of fugacities of guest molecules
            :type f0: list

            :returns: Fugacity of water in hydrate phase at (P,T,f0)
            :rtype: double
            )pbdoc", "p"_a, "T"_a, "f0"_a)
        .def("xH", &VdWP::xH, R"pbdoc(
            :returns: Hydrate composition at equilibrium at (P,T,f0)
            :rtype: list
            )pbdoc")
        ;

    py::class_<Ballard, PyVdWP<Ballard>, VdWP>(m, "Ballard", R"pbdoc(
            This class evaluates the Ballard (2002) implementation of VdWP EoS.
            )pbdoc")
        .def(py::init<CompData&, std::string>(), R"pbdoc(
            :param comp_data: Component data
            :type comp_data: CompData
            :param hydrate_type: Hydrate type (sI, sII, sH)
            :type hydrate_type: str
            )pbdoc", "comp_data"_a, "hydrate_type"_a)
        ;

    py::class_<Munck, PyVdWP<Munck>, VdWP>(m, "Munck", R"pbdoc(
            This class evaluates the Munck (1988) implementation of VdWP EoS.
            )pbdoc")
        .def(py::init<CompData&, std::string>(), R"pbdoc(
            :param comp_data: Component data
            :type comp_data: CompData
            :param hydrate_type: Hydrate type (sI, sII, sH)
            :type hydrate_type: str
            )pbdoc", "comp_data"_a, "hydrate_type"_a)
        ;

    py::class_<PureSolid, PyEoS<PureSolid>, EoS>(m, "PureSolid", R"pbdoc(
            This class evaluates the Ballard (2002) implementation of a pure solid EoS.
            )pbdoc")
        .def(py::init<CompData&, std::string>(), R"pbdoc(
            :param comp_data: Component data
            :type comp_data: CompData
            :param phase: Pure phase
            :type phase: str
            )pbdoc", "comp_data"_a, "phase"_a)
        ;

    py::class_<TrialPhase>(m, "TrialPhase", R"pbdoc(
            This is a struct containing all information for a trial phase
            )pbdoc")
        .def(py::init<std::string, std::vector<double>&>(), R"pbdoc(
            :param tpd: TPD value of composition Y
            :param Y: Trial phase composition Y
            :param eos_name: Name of EoS of trial phase
            )pbdoc", "Y"_a, "eos_name"_a)

        .def_readonly("Y", &TrialPhase::Y)
        .def_readonly("tpd", &TrialPhase::tpd)
        .def_readonly("eos_name", &TrialPhase::eos_name)

        .def("print_point", &TrialPhase::print_point, R"pbdoc(
            Function to print out trial phase information
            )pbdoc")
        ;

    py::class_<InitialGuess> ig(m, "InitialGuess", R"pbdoc(
            This is a base class for providing initial guesses to Flash, Stability or PhaseSplit algorithms.

            It contains methods for evaluating Wilson's (1968) correlation for vapour-liquid, Henry's law coefficients (Sander, 2015)
            vapour-hydrate and `pure phase` ideal K-values.
            )pbdoc");
    
    ig.def(py::init<CompData&>(), R"pbdoc(
            This is the constructor for generating initial guesses for Stability algorithms

            :param comp_data: Component data
            :type comp_data: CompData
            )pbdoc", "comp_data"_a)
        
        .def("init", &InitialGuess::init, R"pbdoc(
            Initialize P and T inside InitialGuess object.

            :param p: Pressure
            :type p: double
            :param T: Temperature
            :type T: double
            )pbdoc", "p"_a, "T"_a)
        
        .def("wilson", &InitialGuess::k_wilson, R"pbdoc(        
            :returns: List of Wilson (1968) K-values at P-T
            :rtype: list
            )pbdoc")
        .def("henry", &InitialGuess::k_henry, R"pbdoc(        
            :returns: List of Henry's law K-values at P-T, Sander (2015)
            :rtype: list
            )pbdoc")
        .def("y_henry", &InitialGuess::y_henry, R"pbdoc(        
            :returns: List of Henry's law K-values at P-T, Sander (2015)
            :rtype: list
            )pbdoc")
        .def("vapour_sI", &InitialGuess::k_vapour_sI, R"pbdoc(        
            :returns: List of vapour-sI K-values at P-T
            :rtype: list
            )pbdoc")
        .def("vapour_sII", &InitialGuess::k_vapour_sII, R"pbdoc(        
            :returns: List of vapour-sII K-values at P-T
            :rtype: list
            )pbdoc")
        .def("y_pure", &InitialGuess::y_pure, R"pbdoc(        
            :param j: j'th component
            :type j: int
            :param pure: Mole fraction of pure component, default is 0.9
            :type pure: double
            :returns: List of pure component K-values at P-T-x
            :rtype: list
            )pbdoc", "j"_a)
        .def("set_ypure", &InitialGuess::set_ypure, "Set mole fraction for pure phase guess of component j", "j"_a, "pure"_a)
        ;

    py::enum_<InitialGuess::Ki>(ig, "Ki", "Correlation type for phase split initial guess (K)")
        .value("Wilson_VL", InitialGuess::Ki::Wilson_VL)
        .value("Wilson_LV", InitialGuess::Ki::Wilson_LV)
        .value("Henry_VA", InitialGuess::Ki::Henry_VA)
        .value("Henry_AV", InitialGuess::Ki::Henry_AV)
        .export_values()
        ;
    
    py::enum_<InitialGuess::Yi>(ig, "Yi", "Correlation type for stability test initial guess (Y)")
        .value("Wilson", InitialGuess::Yi::Wilson)
        .value("Wilson13", InitialGuess::Yi::Wilson13)
        .value("Henry", InitialGuess::Yi::Henry)
        .value("sI", InitialGuess::Yi::sI)
        .value("sII", InitialGuess::Yi::sII)
        .value("sH", InitialGuess::Yi::sH)
        .value("Pure", InitialGuess::Yi::Pure)
        .export_values()
        ;
}
