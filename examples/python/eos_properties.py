import numpy as np

from dartsflash.libflash import CubicEoS, InitialGuess
from dartsflash.pyflash import PyFlash
from dartsflash.mixtures import Mixture
from dartsflash.diagram import plt


NA = 6.02214076e23  # Avogadro's number [mol-1]
kB = 1.380649e-23  # Boltzmann constant [J/K]
R = NA * kB  # Gas constant [J/mol.K]


if 1:
    mix = Mixture(components=["CO2"])
    a = PyFlash(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])

    state_spec = ["pressure", "temperature"]
    dimensions = {"pressure": np.arange(10, 301, 1),
                  "temperature": np.arange(250, 400, 1),
                  }
    constants = {"CO2": 1.}
    dims_order = ["pressure",
                  "temperature"]
    properties = {"V": a.eos["PR"].V,
                  "rho": a.eos["PR"].rho,
                  "JT": a.eos["PR"].JT,
                  }
    props = list(properties.keys())

    results = a.evaluate_properties_1p(state_spec=state_spec, dimensions=dimensions, constants=constants,
                                       properties_to_evaluate=properties, mole_fractions=True, dims_order=dims_order)
    results.V.plot()
    plt.show()
    results.rho.plot()
    plt.show()
    results.JT.plot()


if 0:
    mix = Mixture(components=["CO2", "C1"])
    a = PyFlash(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])

    state_spec = ["pressure", "temperature"]
    dimensions = {"pressure": np.arange(10, 301, 1),
                  "temperature": np.array([273.15 + 25, 273.15 + 50, 273.15 + 75]),
                  "CO2": np.array([1.-1e-10, 0.5, 1e-10]),
                  }
    constants = {"C1": 1.}
    dims_order = ["CO2",
                  "pressure",
                  "temperature"]
    properties = {"V": a.eos["PR"].V,
                  "Cv": a.eos["PR"].Cv,
                  "Cp": a.eos["PR"].Cp,
                  "JT": a.eos["PR"].JT,
                  "vs": a.eos["PR"].vs,
                  }
    props = list(properties.keys())

    results = a.evaluate_properties_1p(state_spec=state_spec, dimensions=dimensions, constants=constants,
                                       properties_to_evaluate=properties, mole_fractions=True, dims_order=dims_order)
    results = results[props]

    num_curves = 3
    ref_p = [np.arange(25, 301, 25) for i in range(num_curves)]
    ref_Cv_CO2 = np.array([
        [32.047, 37.992, 43.558, 41.573, 40.916, 40.617, 40.467, 40.391, 40.357, 40.348, 40.355, 40.373],
        [32.061, 34.836, 39.008, 45.270, 43.077, 41.540, 40.831, 40.441, 40.220, 40.095, 40.028, 39.996],
        [32.493, 34.223, 36.241, 38.525, 40.606, 41.326, 41.005, 40.575, 40.259, 40.053, 39.923, 39.845]]) / R
    ref_Cp_CO2 = np.array([
        [48.005, 82.938, 172.52, 126.22, 110.64, 102.23, 96.804, 92.940, 90.012, 87.696, 85.807, 84.228],
        [45.528, 59.236, 95.319, 255.60, 185.54, 134.21, 114.71, 104.37, 97.856, 93.325, 89.960, 87.344],
        [44.580, 52.560, 65.648, 88.452, 122.37, 139.32, 128.65, 115.35, 105.71, 98.921, 93.957, 90.204]]) / R
    ref_JT_CO2 = [[1.1035, 1.0884, 0.14586, 0.087512, 0.061627, 0.045863, 0.034932, 0.026768, 0.020367, 0.015176, 0.010858, 0.0071951],
                  [0.89274, 0.87627, 0.80974, 0.56138, 0.24332, 0.14483, 0.10067, 0.074904, 0.057712, 0.045288, 0.035820, 0.028325],
                  [0.73793, 0.72154, 0.68380, 0.60928, 0.48363, 0.33536, 0.22619, 0.16054, 0.11992, 0.092761, 0.073363, 0.058842]]
    ref_vs_CO2 = [[248.53, 220.79, 347.29, 432.31, 487.35, 530.04, 565.68, 596.70, 624.44, 649.71, 673.04, 694.81],
                  [263.76, 246.78, 228.38, 218.24, 291.23, 362.47, 416.21, 459.90, 497.05, 529.56, 558.63, 585.07],
                  [276.98, 265.24, 254.37, 246.80, 249.30, 273.13, 313.83, 356.73, 396.22, 431.80, 464.00, 493.38]]
    ref_Cv_C1 = np.array([
        [27.800, 28.210, 28.605, 28.961, 29.259, 29.489, 29.656, 29.775, 29.863, 29.934, 29.997, 30.058],
        [28.716, 29.023, 29.316, 29.583, 29.818, 30.016, 30.180, 30.313, 30.423, 30.517, 30.599, 30.673],
        [29.807, 30.047, 30.274, 30.483, 30.672, 30.839, 30.985, 31.112, 31.222, 31.321, 31.409, 31.490]]) / R
    ref_Cp_C1 = np.array([
        [38.249, 41.282, 44.753, 48.424, 51.873, 54.634, 56.426, 57.721, 57.406, 57.107, 56.583, 55.965],
        [38.730, 40.992, 43.445, 45.963, 48.364, 50.459, 52.109, 53.262, 53.954, 54.273, 54.321, 54.190],
        [39.510, 41.276, 43.126, 44.988, 46.769, 48.377, 49.742, 50.826, 51.629, 52.175, 52.505, 52.668]]) / R
    ref_JT_C1 = [[0.42234, 0.39961, 0.36866, 0.32979, 0.28554, 0.23989, 0.19667, 0.15842, 0.12610, 0.099500, 0.077808, 0.060106],
                 [0.35408, 0.33455, 0.31041, 0.28201, 0.25059, 0.21802, 0.18617, 0.15650, 0.12987, 0.10660, 0.086592, 0.069549],
                 [0.29872, 0.28197, 0.26520, 0.24060, 0.21698, 0.19259, 0.16486, 0.14543, 0.12408, 0.10474, 0.087488, 0.072275]]
    ref_vs_C1 = [[441.73, 437.57, 437.49, 442.75, 454.35, 472.51, 496.53, 525.00, 556.27, 588.95, 622.02, 654.84],
                  [460.90, 459.03, 460.35, 465.50, 474.94, 488.78, 506.73, 528.19, 552.36, 578.39, 605.57, 633.29],
                  [478.43, 478.22, 480.59, 485.86, 494.27, 505.85, 520.45, 537.73, 557.27, 578.56, 601.14, 624.58]]
    labels = ['25$\degree$C', '50$\degree$C', '75$\degree$C']

    p = [dimensions["pressure"] for i in range(num_curves)]
    cv = [results.isel(temperature=0, CO2=0).Cv.values,
          results.isel(temperature=1, CO2=0).Cv.values,
          results.isel(temperature=2, CO2=0).Cv.values,
          ]
    cp = [results.isel(temperature=0, CO2=0).Cp.values,
          results.isel(temperature=1, CO2=0).Cp.values,
          results.isel(temperature=2, CO2=0).Cp.values,
          ]
    jt = [results.isel(temperature=0, CO2=0).JT.values,
          results.isel(temperature=1, CO2=0).JT.values,
          results.isel(temperature=2, CO2=0).JT.values,
          ]
    vs = [results.isel(temperature=0, CO2=0).vs.values,
          results.isel(temperature=1, CO2=0).vs.values,
          results.isel(temperature=2, CO2=0).vs.values,
          ]

    plot = Plot()
    plot.draw_plot(xdata=p, ydata=jt, number_of_curves=num_curves, logy=False,
                   title="Joule-Thomson coefficient CO2", xlabel="pressure", ylabel="JT [K/bar]", datalabels=labels)
    plot.draw_refdata(number_of_curves=num_curves, xref=ref_p, yref=ref_JT_CO2)

if 0:
    mix = Mixture(components=["H2O", "CO2"])
    a = PyFlash(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])

    state_spec = ["temperature", "volume"]
    dimensions = {"volume": np.linspace(2.25e-5, 5e-3, 1000),
                  "temperature": np.array([323.15, 373.15, 423.15, 473.15, 523.15]),
                  "H2O": np.array([1. - 1e-10, 0.5, 1e-10]),
                  }
    constants = {"CO2": 1.}
    dims_order = ["H2O",
                  "volume",
                  "temperature"]
    properties = {"P": a.eos["PR"].P,
                  }
    props = list(properties.keys())

    results = a.evaluate_properties_1p(state_spec=state_spec, dimensions=dimensions, constants=constants,
                                       properties_to_evaluate=properties, mole_fractions=True, dims_order=dims_order)
    results = results[props]

    num_curves = 5
    v = [dimensions["volume"] for i in range(num_curves)]
    p = [results.isel(H2O=0, temperature=i).P.values for i, temp in enumerate(dimensions["temperature"])]
    labels = ['{} K'.format(temp) for temp in dimensions["temperature"][:num_curves]]

    plot = Plot()
    plot.draw_plot(xdata=v, ydata=p, number_of_curves=num_curves, logy=False, xlim=[-1e-4, 0.51e-2], ylim=[0, 100],
                   title="P-V diagram H2O-CO2", xlabel="volume [m3/mol]", ylabel="pressure [bar]", datalabels=labels)

if 0:
    mix = Mixture(components=["H2O", "CO2"])
    a = PyFlash(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson], preferred_roots=(0, 0.9, EoS.MAX))
    a.add_eos("PR_l", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])
    a.add_eos("PR_v", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])
    a.add_eos("AQ", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])
    # a.eos["PR_l"].set_root_flag(0)
    # a.eos["PR_v"].set_root_flag(1)

    state_spec = ["temperature", "volume"]
    dimensions = {"volume": np.linspace(2.25e-5, 5e-3, 1000),
                  "temperature": np.array([373.15, 423.15, 473.15, 523.15]),
                  "H2O": np.array([1. - 1e-10, 0.95, 0.6]),
                  }
    constants = {"CO2": 1.}
    dims_order = ["H2O",
                  "volume",
                  "temperature"]
    properties = {"P": a.eos["PR"].P,
                  "G_V": a.eos["PR"].G_TP,
                  "G_A": a.eos["AQ"].G_TP,
                  }
    props = list(properties.keys())

    results = a.evaluate_properties_1p(state_spec=state_spec, dimensions=dimensions, constants=constants,
                                       properties_to_evaluate=properties, mole_fractions=True, dims_order=dims_order)
    results = results[props]

    num_curves = 4
    t = dimensions["temperature"][:num_curves]
    v = [dimensions["volume"] for i in range(num_curves)]
    p = [results.isel(H2O=0, temperature=i).P.values for i in range(num_curves)]
    n = [[h2o, 1.-h2o] for h2o in dimensions["H2O"]]
    labels = [f'{t[i]:.2f} K' for i in range(num_curves)]

    V_minG = np.empty(np.shape(p))
    for i, T in enumerate(t):
        for j, P in enumerate(p[i]):
            if P > 0.:
                V_minG[i, j] = a.eos["PR"].V(P, T, n[0])
            else:
                V_minG[i, j] = np.nan

    plot = Plot()
    plot.draw_plot(xdata=v, ydata=p, number_of_curves=num_curves, logy=False, xlim=[-1e-4, 0.51e-2], ylim=[0, 100],
                   datalabels=labels)
    plot.add_attributes(title=r"P-V diagram H$_2$O", ax_labels=[r"volume [m$^3$/mol]", "pressure [bar]"], grid=True,
                        legend=True, legend_loc='upper right')
    # plot.draw_plot(xdata=V_minG, ydata=p, number_of_curves=num_curves, plot_type="scatter")

    # num_curves = len(dimensions["temperature"])
    # t_idx = 1
    # p = [results.isel(H2O=i, temperature=t_idx).P.values for i in range(num_curves)]
    # labels = [f'zH2O = {n[i][0]:.2f}' for i in range(num_curves)]
    #
    # V_minG = np.empty(np.shape(p))
    # for i, ni in enumerate(n):
    #     for j, P in enumerate(p[i]):
    #         if P > 0.:
    #             V_minG[i, j] = a.eos["PR"].V(P, t[t_idx], ni)
    #         else:
    #             V_minG[i, j] = np.nan
    #
    # plot2 = Plot()
    # plot2.draw_plot(xdata=v, ydata=p, number_of_curves=num_curves, logy=False, xlim=[-1e-4, 0.51e-2], ylim=[0, 100],
    #                title="P-V diagram H2O-CO2", xlabel="volume [m3/mol]", ylabel="pressure [bar]", datalabels=labels)
    # plot2.draw_plot(xdata=V_minG, ydata=p, number_of_curves=num_curves, plot_type="scatter")

plt.show()
