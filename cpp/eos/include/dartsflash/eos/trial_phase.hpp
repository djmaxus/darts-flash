//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_TRIALPHASE_H
#define OPENDARTS_FLASH_EOS_TRIALPHASE_H
//--------------------------------------------------------------------------

#include <vector>
#include <string>
#include "dartsflash/eos/eos.hpp"

struct TrialPhase
{
	double tpd;
	std::string eos_name;
	std::vector<double> Y, y;
	EoS::RootFlag root = EoS::RootFlag::STABLE;
	bool is_stable_root = true;
	EoS::RootSelect is_preferred_root = EoS::RootSelect::ACCEPT;
	bool is_in_range = true;

	TrialPhase() {}
	TrialPhase(std::string eos_name_, std::vector<double>& Y_);

	void set_stationary_point(std::vector<double>& Y_, double tpd_);
	void print_point(std::string text="TrialPhase");
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_TRIALPHASE_H
//--------------------------------------------------------------------------
