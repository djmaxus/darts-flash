//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_SOLID_SOLID_H
#define OPENDARTS_FLASH_EOS_SOLID_SOLID_H
//--------------------------------------------------------------------------

#include <unordered_map>
#include <string>
#include <vector>

#include "dartsflash/eos/eos.hpp"
#include "dartsflash/global/global.hpp"

namespace solid_par {
	extern double T_0, P_0, R;

	extern std::unordered_map<std::string, std::string> pure_comp;

    extern std::unordered_map<std::string, double> gi0, hi0, v0;

    extern std::unordered_map<std::string, std::vector<double>> cp, alpha;

	class Integral
	{
	protected:
    	double pp, TT;
		std::string phase;
    
	public:
		Integral(std::string phase_) { phase = phase_; }
	};

	class H : public Integral
	{
	public:
		H(std::string phase_) : Integral(phase_) {}

		double f(double T);
		double F(double T);

		double dFdT(double T);
	};

	class V : public Integral
	{
	public:
		V(std::string phase_) : Integral(phase_) {}
		
		double f(double p, double T);
		double F(double p, double T);

		double dFdP(double p, double T);
		double dFdT(double p, double T);
	};
}

class PureSolid : public EoS
{
protected:
	std::string phase;
	int pure_comp_idx;
	double lnfS;

public:
	PureSolid(CompData& comp_data, std::string phase);

	EoS* getCopy() override { return new PureSolid(*this); }

	// Overloaded function for calculation of P(T, V, n) and V(p, T, n)
	double P(double T_, double V_, std::vector<double>& n);
	double V(double p_, double T_, std::vector<double>& n);
	double dV_dP(double p_, double T_, std::vector<double>& n);
	double dV_dT(double p_, double T_, std::vector<double>& n);
	double dV_dni(double p_, double T_, std::vector<double>& n, int i);

	void init_TP(double p_, double T_) override;
	void solve_TP(std::vector<double>::iterator n_it, bool second_order=true) override;

	double lnphii(int i) override;
	std::vector<double> dlnphi_dP() override;
	std::vector<double> dlnphi_dT() override;
	std::vector<double> dlnphi_dn() override;

	int pvt_test(double p_, double T_, std::vector<double>& n, double tol);

};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_SOLID_SOLID_H
//--------------------------------------------------------------------------
