#include "dartsflash/global/timer.hpp"
#include "dartsflash/global/global.hpp"

void Timer::start()
{
    t0 = std::chrono::steady_clock::now();
    running = true;
    return;
}

void Timer::stop()
{
    t1 = std::chrono::steady_clock::now();
    running = false;
    double dt = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();
    time += dt;
    return;
}

double Timer::elapsedMicroseconds()
{
    if (running)
    {
        return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - t0).count();
    }
    else
    {
        return time;
    }
}
