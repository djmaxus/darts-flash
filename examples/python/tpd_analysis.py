import numpy as np
import matplotlib.pyplot as plt

from dartsflash.libflash import InitialGuess, EoS
from dartsflash.libflash import CubicEoS, AQEoS
from dartsflash.pyflash import PyFlash
from dartsflash.diagram import PhaseDiagram

from dartsflash.mixtures import Mixture


mix = Mixture(components=["CO2", "H2O"], np_max=3, name=r"CO$_2$-H$_2$O")
a = PyFlash(mixture=mix)

pr = CubicEoS(mix.comp_data, CubicEoS.PR)
a.add_eos("CEOS", CubicEoS(mix.comp_data, CubicEoS.PR),
          initial_guesses=[InitialGuess.Wilson, 0, 1], preferred_roots=[(1, 0.6, EoS.MAX)],
          # switch_tol=1e-5
          )
a.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                      AQEoS.solute: AQEoS.Ziabakhsh2012}),
          initial_guesses=[1])
a.init_flash()

state_spec = ["pressure", "temperature"]
components = mix.comp_data.components[:-1]
dimensions = {"pressure": np.array([50.]),
              "temperature": np.arange(273.15, 373.16, 1.),
              "CO2": np.arange(0.005, 1., 0.005)}
constants = {"H2O": 1.}
dims_order = ["CO2",
              "pressure",
              "temperature"]

results = a.evaluate_stationary_points(state_spec, dimensions, constants, mole_fractions=True, dims_order=dims_order)

pres = dimensions["pressure"][0]
data = results.isel(pressure=0).neg_sp
pd = PhaseDiagram()
levels = np.arange(np.min(data) - 0.5, np.max(data) + 0.51)
pd.Tx(temperature=dimensions["temperature"], Xi=dimensions["CO2"], data=data, colours=pd.colours[1:len(levels)], levels=levels)

pd.add_attributes(title="Number of negative TPD for " + mix.name + " at P = {:.0f} bar".format(pres),
                  ax_labels=[r"x$_{CO_2}$ [-]", "Temperature [K]"],
                  colorbar=True, ticks=range(0, len(levels)),
                  )

# if diagram_type == 'PT':
#     p_min, p_max = mixture.P_range
#     pressure = np.arange(p_min, p_max, 1)
#
#     T_min, T_max = mixture.T_range
#     temperature = np.arange(T_min, T_max, 1)
#
#     z = mixture.composition
#
#     pd = PhaseDiagram()
#
#     data = np.zeros((len(temperature), len(pressure)))
#
#     P = [189, 206, 225, 226]
#     T = [269, 384, 323, 338]
#
#     for j, p in enumerate(pressure):
#         for i, t in enumerate(temperature):
#             # error = flash.evaluate(p, t, z)
#             # nu = flash.getnu()
#             # data[i, j] = np.count_nonzero(nu)
#             p = P[1]
#             t = T[1]
#             sp = flash.get_stationary_points(p, t, z)
#             exit()
#             data[i, j] = np.sum([(p.tpd < 0.) for p in sp])
#
#     levels = np.arange(np.min(data) - 0.5, np.max(data) + 0.51)
#     pd.PT(pressure=pressure, temperature=temperature, data=data, colours=pd.colours[1:len(levels)],
#           levels=levels)
#
#     pd.add_attributes(title="Number of negative TPD for " + mixture.name, colorbar=True,
#                       # ticks=range(0, len(levels)))
#                       )
#
#     elif diagram_type == 'Px':
#         p_min, p_max = mixture.P_range
#         p_min, p_max = 67., 77.
#         pressure = np.arange(p_min, p_max+1e-10, 0.1)
#         # pressure = np.arange(p_min, p_max+1e-10, 0.01)
#
#         x_min, x_max = mixture.X_range
#         x_min, x_max = 1e-3, 1e-1
#         Xi = np.arange(x_min, x_max+1e-10, 0.001)
#         # Xi = np.arange(x_min, x_max+1e-10, 0.1)
#
#         t = mixture.T_range
#         z = mixture.composition
#
#         pd = PhaseDiagram()
#
#         data = np.zeros((len(Xi), len(pressure)))
#
#         # data[1, 1:3] = 1
#
#         for j, p in enumerate(pressure):
#             for i, xi in enumerate(Xi):
#                 Z = [xi] + [(1.-xi)*zi/np.sum(mixture.composition[1:]) for zi in mixture.composition[1:]]
#                 # Z = np.array([xi] + [zi * (1.-xi) for zi in mixture.composition[1:]])
#                 # Z /= np.sum(Z)
#                 ref_comps = [TrialPhase("PR", Z)]
#                 sp = flash.find_stationary_points(p, t, ref_comps)
#                 data[i, j] = np.sum([(p.tpd < 0.) for p in sp])
#                 # error = flash.evaluate(p, t, Z)
#                 # nu = flash.getnu()
#                 # data[i, j] = np.count_nonzero(nu)
#
#         levels = np.arange(np.min(data) - 0.5, np.max(data) + 0.51)
#         pd.Px(pressure=pressure, Xi=Xi, data=data, colours=pd.colours[1:len(levels)], levels=levels)
#         pd.draw_contours(x=Xi, y=pressure, data=data, colours='k', linewidth=1)
#
#         pd.add_attributes(title="Number of negative TPD for " + mixture.name,
#                           colorbar=True, ticks=range(0, len(levels)-1),
#                           ax_labels=['x_C1 [-]', 'Pressure [bar]'])
#
#     elif diagram_type == 'Tx':
#         T_min, T_max = mixture.T_range
#         temperature = np.arange(T_min, T_max, 1)
#
#         x_min, x_max = mixture.X_range
#         x = np.arange(x_min, x_max, 0.01)
#
#         z = mixture.composition
#
#     elif diagram_type == 'Binary':
#
#         print()
#
#     elif diagram_type == 'Ternary':
#         pressure = 70
#         temperature = 300
#
#         labels = [r"\mathregular{C_1}", r"\mathregular{nC_{10}}", r"\mathregular{CO_2}"]
#         td = TernaryDiagram(dz=1e-2, figsize=(6, 6))
#
#         data = np.zeros(td.n_data_points)
#         for i, z in enumerate(td.comp_physical):
#             ref_comps = [TrialPhase("PR", z)]
#             sp = flash.find_stationary_points(pressure, temperature, ref_comps)
#             data[i] = np.sum([(p.tpd < 0.) for p in sp])
#
#         # gmix = a.calc_gibbs_mixing(pressure, temperature, td.comp_physical, "PR")
#         td.draw_surf(data=data, levels=[0, 1, 2, 3])
#         # compositions = [[0.3, 0.4, 0.3], [0.6, 0.2, 0.2], [0., 0., 1.], [0.3, 0.4, 0.3]]
#         # fig, ax = td.draw_line(compositions, fig=fig, ax=ax)
#         # fig, ax = td.draw_point(compositions, fig=fig, ax=ax)
#         td.add_attributes(title="Number of negative TPD at p={} bar and T={} K".format(pressure, temperature),
#                           corner_labels=labels, colorbar=True)
#
#     elif diagram_type == 'Quaternary':
#
#         print()
#
#     plt.tight_layout()
plt.show()
