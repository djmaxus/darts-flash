//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_HELMHOLTZ_HELMHOLTZ_H
#define OPENDARTS_FLASH_EOS_HELMHOLTZ_HELMHOLTZ_H
//--------------------------------------------------------------------------

#include <complex>
#include "dartsflash/eos/eos.hpp"
#include "dartsflash/global/global.hpp"

class HelmholtzEoS : public EoS
{
protected:
	double z, v, N;
	std::unordered_map<int, std::pair<double, EoS::RootFlag>> preferred_roots = {};
	std::vector<std::complex<double>> Z_roots;

public:
	HelmholtzEoS(CompData& comp_data);
	
protected:
	// Volume, pressure function and derivatives
	virtual double V() = 0;
	double P();
	double Z(double V_);
	double dP_dV();
	double dP_dT();
	double dP_dni(int i);
	double dV_dni(int i);
	double dV_dT();
	double dT_dni(int i);

	// Reduced Helmholtz function and derivatives
	virtual double F() = 0;
	virtual double dF_dV() = 0;
	virtual double dF_dT() = 0;
	virtual double dF_dni(int i) = 0;
    virtual double d2F_dnidnj(int i, int j) = 0;
    virtual double d2F_dTdni(int i) = 0;
    virtual double d2F_dVdni(int i) = 0;
    virtual double d2F_dTdV() = 0;
    virtual double d2F_dV2() = 0;
    virtual double d2F_dT2() = 0;

public:
	// Calculate compressibility factor Z
	virtual std::vector<std::complex<double>> Z() = 0;
	void set_preferred_roots(int i, double x, EoS::RootFlag root_flag) { this->preferred_roots[i] = std::pair<double, EoS::RootFlag>{x, root_flag}; }
	virtual EoS::RootSelect select_root(std::vector<double>::iterator n_it) override;

	// Overloaded function for calculation of P(T, V, n) and V(p, T, n)
	double P(double T_, double V_, std::vector<double>& n);
	double V(double p_, double T_, std::vector<double>& n);
	double Z(double p_, double T_, std::vector<double>& n);
	double rho(double p_, double T_, std::vector<double>& n);

	// Evaluation of EoS at (P, T, n) or (T, V, n)
	void init_TP(double p_, double T_) = 0;
	void solve_TP(std::vector<double>::iterator n_it, bool second_order=true) override;
	void solve_TP(double p_, double T_, std::vector<double>& n, int start_idx=0, bool second_order=true) { return EoS::solve_TP(p_, T_, n, start_idx, second_order); }
	virtual void init_TV(double T_, double V_) = 0;
	void solve_TV(std::vector<double>::iterator n_it, bool second_order=true);
	void solve_TV(double T_, double V_, std::vector<double>& n, int start_idx=0, bool second_order=true);

	// Zero'th, first and second order parameters for Helmholtz formulation
	virtual void zeroth_order(std::vector<double>::iterator n_it) = 0;
	virtual void zeroth_order(std::vector<double>::iterator n_it, double V_) = 0;
	virtual void zeroth_order(double V_) = 0;
	virtual void first_order(std::vector<double>::iterator n_it) = 0;
	virtual void second_order(std::vector<double>::iterator n_it) = 0;
	
	// Fugacity coefficient and derivatives
	double lnphii(int i) override;
	double dlnphii_dP(int i) override;
	double dlnphii_dT(int i) override;
	double dlnphii_dnj(int i, int j) override;

	// Residual bulk properties
	double Ar_TV(double T_, double V_, std::vector<double>& n, int start_idx=0);
	double Sr_TV(double T_, double V_, std::vector<double>& n, int start_idx=0);
	double Ur_TV(double T_, double V_, std::vector<double>& n, int start_idx=0);
	double Hr_TV(double T_, double V_, std::vector<double>& n, int start_idx=0);
	double Gr_TV(double T_, double V_, std::vector<double>& n, int start_idx=0);
	virtual double Gr_TP(double p_, double T_, std::vector<double>& n, int start_idx=0) override;
	virtual double Hr_TP(double p_, double T_, std::vector<double>& n, int start_idx=0) override;
	virtual double Sr_TP(double p_, double T_, std::vector<double>& n, int start_idx=0) override;
	double Ar_TP(double p_, double T_, std::vector<double>& n, int start_idx=0);  // override;
	double Ur_TP(double p_, double T_, std::vector<double>& n, int start_idx=0);  // override;

	double Cvr(double p_, double T_, std::vector<double>& n, int start_idx=0);
	double Cv(double p_, double T_, std::vector<double>& n, int start_idx=0);
	double Cpr(double p_, double T_, std::vector<double>& n, int start_idx=0);
	double Cp(double p_, double T_, std::vector<double>& n, int start_idx=0);
	double vs(double p_, double T_, std::vector<double>& n, int start_idx=0);
	double JT(double p_, double T_, std::vector<double>& n, int start_idx=0);

	// Consistency tests
	int derivatives_test(double p_, double T_, std::vector<double>& n, double tol);
	int lnphi_test(double p_, double T_, std::vector<double>& n, double tol);
	int pressure_test(double p_, double T_, std::vector<double>& n, double tol);
	int temperature_test(double p_, double T_, std::vector<double>& n, double tol);
	int composition_test(double p_, double T_, std::vector<double>& n, double tol);
	int pvt_test(double p_, double T_, std::vector<double>& n, double tol);
	int properties_test(double p_, double T_, std::vector<double>& n, double tol);
	
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_HELMHOLTZ_HELMHOLTZ_H
//--------------------------------------------------------------------------
