#include <iostream>
#include <vector>
#include <cassert>

#include "dartsflash/flash/flash.hpp"
#include <Eigen/Dense>

NegativeFlash::NegativeFlash(FlashParams& flashparams, const std::vector<std::string>& eos_used, const std::vector<int>& initial_guesses_) 
	: Flash(flashparams, static_cast<int>(eos_used.size())) 
{
	assert((eos_used.size() == (initial_guesses_.size() + 1)) && 
			"Length of specified EoS incompatible with number of initial guesses\n");
	this->eos = eos_used;
	this->initial_guesses = initial_guesses_;
	
	this->sp_idxs.resize(np_max);
	std::iota(this->sp_idxs.begin(), this->sp_idxs.end(), 0);
}

int NegativeFlash::evaluate(double p, double T, std::vector<double>& z_)
{
	// Solve N-phase split with negative flash
	this->np = np_max;

	// Initialize flash
	// Initialize EoS at p, T, correct composition vector and generate initial guess for two-phase split
	this->init(p, T, z_);

	// Initialize vector with TrialPhase for each phase
	this->stationary_points.clear();
    for (std::string eos_name: eos)
    {
        stationary_points.push_back(TrialPhase(eos_name, z));
    }

	// Perform negative flash with eos specified
	int split_output = this->run_split(sp_idxs);

	// Return output
	if (split_output == 0)
	{
		// If split output is 0, output is correct
		return 0;
	}
	else if (split_output == -1)
	{
		// If split output is -1, one or more negative phases present
		(void) this->check_negative();
		if (flash_params.verbose)
        {
            print("NegativeFlash", "===============");
            print("p, T", std::vector<double>{p, T});
			print("z", z_);
            print("nu", nu);
            print("X", X, np);
        }
		return 0;
	}
	else
	{
		// Else, error occurred in split. Check if negative phases are present
		if (this->check_negative())
		{
			return 0;
		}
		// If not, phase split output is incorrect
		if (flash_params.verbose)
		{
			print("ERROR in NegativeFlash", split_output);
        	print("p, T", std::vector<double>{p, T}, 10);
			print("z", z_, 10);
		}
		this->nu = std::vector<double>(np, NAN);
		this->X = std::vector<double>(np*ns, NAN);
		return 1;
	}
}

std::vector<double> NegativeFlash::generate_lnK(std::vector<int>& sp_idxs_)
{
	(void) sp_idxs_;
	return flash_params.initial_guess.evaluate(this->initial_guesses);
}

bool NegativeFlash::check_negative() {
	if (nu[0] > 1.)
	{
		nu = {1., 0.};
		X = std::vector<double>(ns*np, 0.);
		std::copy(z.begin(), z.end(), X.begin());
		return true;
	}
	else if (nu[1] > 1.)
	{
		nu = {0., 1.};
		X = std::vector<double>(ns*np, 0.);
		std::copy(z.begin(), z.end(), X.begin() + ns);
		return true;
	}
	return false;
}	
