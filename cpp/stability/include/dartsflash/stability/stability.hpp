//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_STABILITY_STABILITY_H
#define OPENDARTS_FLASH_STABILITY_STABILITY_H
//--------------------------------------------------------------------------

#include "dartsflash/global/global.hpp"
#include "dartsflash/maths/modifiedcholeskys99.hpp"
#include "dartsflash/eos/trial_phase.hpp"
#include "dartsflash/flash/flash_params.hpp"
#include "dartsflash/flash/analysis.hpp"
#include <Eigen/Dense>

class Stability
{
protected:
    int nc, ni, ns;
    double tpd;
    std::vector<double> h, lnphi, dlnphidn;
    std::vector<double> lnY, alpha;
    std::vector<std::vector<double>> solution_path;
    TrialPhase ref_composition, trial_composition;
    FlashParams flash_params;
    EoSParams params;
    Eigen::VectorXd g, ga;
    int error{ 0 }, ssi_iter, newton_iter, switch_back_iter;

public:
    Stability(FlashParams& flashparams);
    virtual ~Stability() = default;

    // Give Analysis class access to Stability methods as a friend class
    friend class Analysis;

    // Initialize stability test at mixture composition x, set initial guess Y and run test from Y
    void init(TrialPhase& ref_comp);
    int run(TrialPhase& trial_comp);

    // Method to find if stationary point is a minimum of TPD surface
    bool is_minimum(TrialPhase& trial_comp);

    // Method to find gradient vector with respect to pure component
    double calc_gradient_at_pure(int i);

    // Getters for solution path and iteration numbers
    std::vector<std::vector<double>> get_solution_path() { return this->solution_path; }
    int get_ssi_iter() { return this->ssi_iter + this->switch_back_iter; }
    int get_newton_iter() { return this->newton_iter; }
    double calc_condition_number();

protected:
    // SSI and Newton steps
    void perform_ssi();
    void perform_Y();
    void perform_lnY();
    void perform_alpha();

    // Update fugacities, derivatives, tpd and gradient vector
    void update_fugacities(TrialPhase& comp, bool second_order);
    double calc_modtpd();
    void update_g();

    // Calculate norm of gradient vector
    double l2norm() { return g.squaredNorm(); }

    // Construct matrices
    Eigen::VectorXd construct_ga(std::vector<double>& alpha);
    Eigen::MatrixXd construct_U();
    Eigen::MatrixXd construct_Uinv();
    Eigen::MatrixXd construct_PHI();
    Eigen::MatrixXd construct_H(Eigen::MatrixXd& U, Eigen::MatrixXd& PHI);
    Eigen::MatrixXd construct_H(std::vector<double>& alpha);
    Eigen::MatrixXd construct_J(Eigen::MatrixXd& PHI, Eigen::MatrixXd& Uinv);

public:
    int test_matrices();

};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_STABILITY_STABILITY_H
//--------------------------------------------------------------------------
