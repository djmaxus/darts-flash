#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <numeric>

#include "dartsflash/flash/flash.hpp"
#include "dartsflash/stability/stability.hpp"

StabilityFlash::StabilityFlash(FlashParams& flashparams, int np_max_) : Flash(flashparams, np_max_) { }

int StabilityFlash::evaluate(double p, double T, std::vector<double>& z_)
{
    // Evaluate sequential stability + flash algorithm
    
    // Initialize flash
    // Initialize EoS at p, T and check if feed composition needs to be corrected
    this->init(p, T, z_);

    // Perform stability and phase split loop starting from np = 1
    ref_compositions = {flash_params.find_ref_comp(p, T, z)};
    eos = {ref_compositions[0].eos_name};
    this->np = 1;
    this->nu = {1.};
    this->X = z;

    // Run stability test + flash loop over phases until np_max has been reached
    int it = 1;
    while (true)
    {
        int output = this->run_loop(ref_compositions);
        if (output == -1)
        {
            // Output -1, all phases stable
            if (flash_params.verbose)
            {
                print("StabilityFlash", "===============");
                print("p, T", std::vector<double>{p, T});
			    print("z", z_);
                print("nu", nu);
                print("X", X, np);
            }
            return 0;
        }
        else if (output > 0 || it > 10)
        {
            // Else, error occurred in split
            if (flash_params.verbose)
            {
                print("ERROR in StabilityFlash", output);
    		    print("p, T", std::vector<double>{p, T}, 10);
			    print("z", z_, 10);
            }
            this->nu = std::vector<double>(np, NAN);
		    this->X = std::vector<double>(np*ns, NAN);
            return output;
        }
        it++;
    }

    return 0;
}
