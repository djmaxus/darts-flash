//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_GLOBAL_COMPONENTS_H
#define OPENDARTS_FLASH_GLOBAL_COMPONENTS_H
//--------------------------------------------------------------------------

#include <unordered_map>
#include <vector>
#include <string>
#include "dartsflash/global/ideal.hpp"
#include "dartsflash/global/units.hpp"

namespace comp_data {
	extern std::unordered_map<std::string, int> charge;
	extern std::unordered_map<std::string, double> Pc, Tc, ac, Mw, H0, dlnH0, cp1, cp2, cp3, cp4;
	extern std::unordered_map<std::string, std::unordered_map<std::string, double>> kij;
}

class CompData
{
public:
    int nc, ni, ns, water_index;
    Units units;
    IdealGas ideal;
    std::vector<std::string> components, ions;
    std::vector<double> Pc, Tc, ac, Mw, kij;
    std::vector<double> cp1, cp2, cp3, cp4;
    std::vector<double> H0, dlnH0;
    std::vector<int> charge;

    CompData() { }
    CompData(const std::vector<std::string>& components_, const std::vector<std::string>& ions_={});
	~CompData() = default;

    void set_units(Units& units_) { this->units = units_; }
    void set_binary_coefficients(int i, std::vector<double> kij_);
    double get_molar_weight(std::vector<double>& n);
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_GLOBAL_COMPONENTS_H
//--------------------------------------------------------------------------
