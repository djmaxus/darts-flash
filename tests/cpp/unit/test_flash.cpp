#include <iterator>
#include "dartsflash/flash/flash.hpp"
#include "dartsflash/stability/stability.hpp"
#include "dartsflash/eos/helmholtz/cubic.hpp"
#include "dartsflash/eos/aq/jager.hpp"
#include "dartsflash/eos/aq/ziabakhsh.hpp"
#include "dartsflash/eos/vdwp/ballard.hpp"
#include "dartsflash/eos/solid/solid.hpp"
#include "dartsflash/global/global.hpp"

int test_negativeflash2_vapour_liquid();
int test_stabilityflash2_vapour_liquid_2comp();
int test_stabilityflash2_vapour_liquid_3comp();
int test_stabilityflash2_vapour_liquid();
int test_stabilityflashN_vapour_liquid();
int test_stabilityflashN_vapour_liquid_water();

int test_negativeflash2_vapour_brine();
int test_stabilityflash2_vapour_brine();
int test_stabilityflashN_vapour_brine();
int test_negativeflash2_vapour_brine_ions();
int test_stabilityflashN_vapour_brine_si();
int test_stabilityflashN_vapour_brine_ice();

struct Reference
{
	double pressure, temperature, nu_tol{ 1e-5 };
	std::vector<double> composition, nu_ref, nu;

	Reference(const double p, const double T, const std::vector<double>& z, const std::vector<double>& nu_ref_) 
	: pressure(p), temperature(T), composition(z), nu_ref(nu_ref_) {}

	void print_conditions(bool verbose)
	{
		if (verbose)
		{
			std::cout << "==================================\n";
			print("p, T", std::vector<double>{pressure, temperature});
			print("z", composition);
		}
		return;
	}

	int test(Flash *flash, bool verbose)
	{
		int error = flash->evaluate(pressure, temperature, composition);
		if (error > 0)
		{
			print("Error in Flash", error);
			print("p, T", std::vector<double>{pressure, temperature});
			print("z", composition);
			return error;
		}
		
		// Output and compare results
		Flash::Results flash_results = flash->get_flash_results();
		nu = flash_results.nu;
		if (verbose)
		{
			std::cout << "\nResults:\n";
			print("nu", nu);
			print("x", flash_results.X, static_cast<int>(flash_results.nu.size()));
			std::cout << " Number of flash iterations ssi = " << flash->get_flash_total_ssi_iter() << std::endl;
			std::cout << " Number of flash iterations newton = " << flash->get_flash_total_newton_iter() << std::endl;
			std::cout << " Number of flash total iterations = " << flash->get_flash_total_ssi_iter() + flash->get_flash_total_newton_iter() << std::endl;
		}

		if (nu.size() != nu_ref.size())
		{
			std::cout << "nu and nu_ref are not the same size\n";
			print("p, T", std::vector<double>{pressure, temperature});
			print("z", composition);
			print("nu", nu);
			print("nu_ref", nu_ref);
			return 1;
		}
		if (this->compare_nu())
		{
			std::cout << "Different values for nu\n";
			print("p, T", std::vector<double>{pressure, temperature});
			print("z", composition);
			print("nu", nu);
			print("nu_ref", nu_ref);
			return 1;
		}
		if (verbose)
		{
			std::cout << "Output is correct!\n";
		}
		return 0;
	}

	int compare_nu()
	{
		for (size_t j = 0; j < nu.size(); j++)
		{
			bool found = false;
			for (size_t k = 0; k < nu_ref.size(); k++)
			{
				if (std::fabs(nu[j]-nu_ref[k]) < nu_tol)
				{
					found = true;
					break;
				}
			}
			if (!found)
			{
				return 1;
			}
		}
		return 0;
	}

	void write_ref(std::string& ref_string)
	{
		std::string p_str = std::to_string(pressure);
		std::string t_str = std::to_string(temperature);

		std::ostringstream z_str_, nu_str_;

    	// Convert all but the last element to avoid a trailing ", "
	    std::copy(composition.begin(), composition.end()-1, std::ostream_iterator<double>(z_str_, ", "));
		std::copy(nu.begin(), nu.end()-1, std::ostream_iterator<double>(nu_str_, ", "));

    	// Now add the last element with no delimiter
    	z_str_ << composition.back();
		nu_str_ << nu.back();

		// Add curly brackets front and back
		std::string z_str = "{" + z_str_.str() + "}";
		std::string nu_str = "{" + nu_str_.str() + "}";

		ref_string += "\t\t";
		ref_string += ("Reference(" + p_str + ", " + t_str + ", " + z_str + ", " + nu_str + "),");
		ref_string += "\n";
		return;
	}
};

int main()
{
	int error_output = 0;
	
	error_output += test_negativeflash2_vapour_liquid();
	error_output += test_stabilityflash2_vapour_liquid_2comp();
	error_output += test_stabilityflash2_vapour_liquid_3comp();
	error_output += test_stabilityflash2_vapour_liquid();
	error_output += test_stabilityflashN_vapour_liquid();
	// error_output += test_stabilityflashN_vapour_liquid_water();

	error_output += test_negativeflash2_vapour_brine();
	error_output += test_stabilityflash2_vapour_brine();
	// error_output += test_stabilityflashN_vapour_brine();
	error_output += test_negativeflash2_vapour_brine_ions();
	error_output += test_stabilityflashN_vapour_brine_si();
	error_output += test_stabilityflashN_vapour_brine_ice();

    return error_output;
}

int test_negativeflash2_vapour_liquid()
{
	// Test negative flash on Y8 mixture
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P NEGATIVEFLASH VAPOUR-LIQUID\n" : "");
    int error_output = 0;

	std::vector<std::string> comp{"C1", "C2", "C3", "nC5", "nC7", "nC10"};
	CompData comp_data(comp);
	comp_data.Pc = {45.99, 48.72, 42.48, 33.70, 27.40, 21.10};
	comp_data.Tc = {190.56, 305.32, 369.83, 469.70, 540.20, 617.70};
	comp_data.ac = {0.011, 0.099, 0.152, 0.252, 0.350, 0.490};
	comp_data.kij = std::vector<double>(6*6, 0.);

	std::vector<double> z = {0.8097, 0.0566, 0.0306, 0.0457, 0.0330, 0.0244};

	CubicEoS pr(comp_data, CubicEoS::PR);

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.split_max_iter = 50;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);

	std::vector<Reference> references = {
		Reference(1.013250, 298.150000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {0.961031, 0.0389694}),
		Reference(100.000000, 335.000000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {0.854433, 0.145567}),
		Reference(220.000000, 335.000000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {0.888439, 0.111561}),
		Reference(10.000000, 210.000000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {0.82805, 0.17195}),
	};

	std::string ref_string = "\tstd::vector<Reference> references = {\n";
	bool write = true;

	std::vector<FlashParams::SplitVars> vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	for (FlashParams::SplitVars var: vars)
	{
		flash_params.split_variables = var;
		NegativeFlash flash(flash_params, {"PR", "PR"}, {InitialGuess::Ki::Wilson_VL});
		for (Reference condition: references)
		{
			error_output += condition.test(&flash, verbose);
			if (write) { condition.write_ref(ref_string); }
		}
		write = false;
	}
	ref_string += "\t};\n";

	if (error_output > 0)
	{
		std::cout << ref_string;
		print("Errors occurred in test_negativeflash2_vapour_liquid()", error_output);
	}
	else
	{
		print("No errors occurred in test_negativeflash2_vapour_liquid()", error_output);
	}
    return error_output;
}

int test_stabilityflash2_vapour_liquid_2comp()
{
	// Test stability flash for a binary mixture
	bool verbose = false;
	std::cout << (verbose ? "TESTING BINARY STABILITYFLASH VAPOUR-LIQUID\n" : "");
    int error_output = 0;

	// H2S-C1 mixture (Michelsen, 1982)
	std::vector<std::string> comp{"H2S", "C1"};
	CompData comp_data(comp);
	comp_data.Pc = {89.63, 46.04};
	comp_data.Tc = {373.53, 190.58};
	comp_data.ac = {0.0942, 0.012};
	comp_data.kij = std::vector<double>(2*2, 0.);
	comp_data.set_binary_coefficients(0, {0., 0.1});

	CubicEoS ceos(comp_data, CubicEoS::SRK);

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.split_max_iter = 50;
	flash_params.verbose = verbose;

	flash_params.add_eos("CEOS", &ceos);
	flash_params.eos_params["CEOS"].stability_switch_tol = 1e-3;
	flash_params.eos_params["CEOS"].initial_guesses = {InitialGuess::Yi::Wilson, 0, 1};

	std::vector<Reference> references = {
		Reference(40.530000, 190.000000, {0.01, 0.99}, {1}),
		Reference(40.530000, 190.000000, {0.05, 0.95}, {0.38538, 0.61462}),
		Reference(40.530000, 190.000000, {0.1, 0.9}, {0.0413661, 0.958634}),
		Reference(40.530000, 190.000000, {0.2, 0.8}, {0.15904, 0.84096}),
		Reference(40.530000, 190.000000, {0.5, 0.5}, {0.463016, 0.536984}),
		Reference(40.530000, 190.000000, {0.8, 0.2}, {0.128227, 0.871773}),
		Reference(40.530000, 190.000000, {0.9, 0.1}, {0.0172459, 0.982754}),
		Reference(40.530000, 190.000000, {0.95, 0.05}, {1}),
		Reference(40.530000, 190.000000, {0.99, 0.01}, {1}),
	};

	std::string ref_string = "\tstd::vector<Reference> references = {\n";
	bool write = true;

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	for (FlashParams::StabilityVars stab_var: stab_vars)
	{
		flash_params.stability_variables = stab_var;
		for (FlashParams::SplitVars split_var: split_vars)
		{
			flash_params.split_variables = split_var;
			StabilityFlash flash(flash_params, 2);
			for (Reference condition: references)
			{
				error_output += condition.test(&flash, verbose);
				if (write) { condition.write_ref(ref_string); }
			}
			write = false;
		}
	}
	ref_string += "\t};\n";

	if (error_output > 0)
	{
		std::cout << ref_string;
		print("Errors occurred in test_stabilityflash2_vapour_liquid_2comp()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflash2_vapour_liquid_2comp()", error_output);
	}
    return error_output;
}

int test_stabilityflash2_vapour_liquid_3comp()
{
	// Test stability flash for a 3-component mixture
	// It can be used to quickly verify new implementations like Newton modified mole number
	bool verbose = false;
	std::cout << (verbose ? "TESTING TERNARY STABILITYFLASH VAPOUR-LIQUID\n" : "");
    int error_output = 0;

	//Test 1 ->H2O-C4-C20 mixture
	std::vector<std::string> comp{"H2O", "C4", "C20"};
	CompData comp_data(comp);
	comp_data.Pc = {220.5, 38., 14.6};
	comp_data.Tc = {647., 425.2, 782.};
	comp_data.ac = {0.344, 0.1928, 0.8160};
	comp_data.kij = std::vector<double>(3*3, 0.);
	comp_data.set_binary_coefficients(0, {0., 0.5, 0.5, 0.5, 0.5});

	std::vector<double> z = {0.8, 0.16, 0.04};

	CubicEoS pr(comp_data, CubicEoS::PR);

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.split_max_iter = 50;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].stability_switch_tol = 1e-3;
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson};

	std::vector<Reference> references = {
		Reference(50, 500, z, {0.947413562, 0.0525865263}),
	};

	std::string ref_string = "\tstd::vector<Reference> references = {\n";
	bool write = true;

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	for (FlashParams::StabilityVars stab_var: stab_vars)
	{
		flash_params.stability_variables = stab_var;
		for (FlashParams::SplitVars split_var: split_vars)
		{
			flash_params.split_variables = split_var;
			StabilityFlash flash(flash_params, 2);
			for (Reference condition: references)
			{
				error_output += condition.test(&flash, verbose);
				if (write) { condition.write_ref(ref_string); }
			}
			write = false;
		}
	}
	ref_string += "\t};\n";

	if (error_output > 0)
	{
		std::cout << ref_string;
		print("Errors occurred in test_stabilityflash2_vapour_liquid_3comp()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflash2_vapour_liquid_3comp()", error_output);
	}
    return error_output;
}

int test_stabilityflash2_vapour_liquid()
{
	// Test stability flash
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P STABILITYFLASH VAPOUR-LIQUID\n" : "");
    int error_output = 0;

	//Test 1 ->Y8 mixture
	std::vector<std::string> comp{"C1", "C2", "C3", "nC5", "nC7", "nC10"};
	CompData comp_data(comp);
	comp_data.Pc = {45.99, 48.72, 42.48, 33.70, 27.40, 21.10};
	comp_data.Tc = {190.56, 305.32, 369.83, 469.70, 540.20, 617.70};
	comp_data.ac = {0.011, 0.099, 0.152, 0.252, 0.350, 0.490};
	comp_data.kij = std::vector<double>(6*6, 0.);

	std::vector<double> z = {0.8097, 0.0566, 0.0306, 0.0457, 0.0330, 0.0244};

	CubicEoS pr(comp_data, CubicEoS::PR);

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-1;
	flash_params.split_max_iter = 50;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].stability_switch_tol = 1e-1;
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson};

	std::vector<Reference> references = {
		Reference(1.013250, 298.150000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {0.0389694, 0.961031}),
		Reference(100.000000, 335.000000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {0.145567, 0.854433}),
		Reference(220.000000, 335.000000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {0.111561, 0.888439}),
		Reference(250.000000, 200.000000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {1}),
		Reference(16.000000, 235.000000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {0.15911, 0.84089}),
		Reference(220.000000, 350.000000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {0.0362088, 0.963791}),
		// Reference(1., 150., z, {0.1878472774, 0.8121527226}),
		Reference(191.000000, 275.000000, {0.8097, 0.0566, 0.0306, 0.0457, 0.033, 0.0244}, {0.377731, 0.622269}),
	};

	std::string ref_string = "\tstd::vector<Reference> references = {\n";
	bool write = true;

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	for (FlashParams::StabilityVars stab_var: stab_vars)
	{
		flash_params.stability_variables = stab_var;
		for (FlashParams::SplitVars split_var: split_vars)
		{
			flash_params.split_variables = split_var;
			StabilityFlash flash(flash_params, 2);
			for (Reference condition: references)
			{
				error_output += condition.test(&flash, verbose);
				if (write) { condition.write_ref(ref_string); }
			}
			write = false;
		}
	}
	ref_string += "\t};\n";

	if (error_output > 0)
	{
		std::cout << ref_string;
		print("Errors occurred in test_stabilityflash2_vapour_liquid()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflash2_vapour_liquid()", error_output);
	}
    return error_output;
}

int test_stabilityflashN_vapour_liquid()
{
	// Test Maljamar separator mixture (Orr, 1981), data from (Li, 2012)
	bool verbose = false;
	std::cout << (verbose ? "TESTING NP STABILITYFLASH\n" : "");
	int error_output = 0;

    int np_max = 3;
    std::vector<std::string> comp = {"CO2", "C5-7", "C8-10", "C11-14", "C15-20", "C21-28", "C29+"};
	int nc = 7;

    CompData comp_data(comp);
    comp_data.Pc = {73.9, 28.8, 23.7, 18.6, 14.8, 12.0, 8.5};
    comp_data.Tc = {304.2, 516.7, 590.0, 668.6, 745.8, 812.7, 914.9};
    comp_data.ac = {0.225, 0.265, 0.364, 0.499, 0.661, 0.877, 1.279};
    comp_data.kij = std::vector<double>(7*7, 0.);
    comp_data.set_binary_coefficients(0, {0.0, 0.115, 0.115, 0.115, 0.115, 0.115, 0.115});

	std::vector<double> z_init = {0.0, 0.2354, 0.3295, 0.1713, 0.1099, 0.0574, 0.0965};

	FlashParams flash_params(comp_data);
    flash_params.split_switch_tol = 1e-3;
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
    flash_params.eos_params["PR"].stability_switch_tol = 1e-1;
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson, 
													 InitialGuess::Yi::Wilson13,
													 0};  // pure CO2 initial guess

	std::vector<std::pair<Reference, double>> references = {
		// {Reference(64., 305.35, z_init, {0.98716891, 0.01283109005}), 0.65},
		// {Reference(64., 305.35, z_init, {0.98716891, 0.01283109005}), 0.68},
		{Reference(68.5, 305.35, z_init, {0.1405898757, 0.2738025267, 0.5856075976}), 0.90},
		{Reference(68., 305.35, z_init, {0.04762089785, 0.02586867064, 0.9265104315}), 0.70},
	};

	for (size_t j = 0; j < references.size(); j++)
	{
		references[j].first.composition[0] = references[j].second;
    	for (int i = 1; i < nc; i++)
    	{
        	references[j].first.composition[i] = z_init[i]*(1.0-references[j].second);
    	}
	}

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};

	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 

		for (FlashParams::StabilityVars stab_var: stab_vars)
		{
			flash_params.stability_variables = stab_var;
			for (FlashParams::SplitVars split_var: split_vars)
			{
				flash_params.split_variables = split_var;
				StabilityFlash flash(flash_params, np_max);
				for (std::pair<Reference, double> condition: references)
				{
					error_output += condition.first.test(&flash, verbose);
				}
			}
		}
	}

	
	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflashN_vapour_liquid()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflashN_vapour_liquid()", error_output);
	}
    return error_output;
}

int test_stabilityflashN_vapour_liquid_water()
{
	// Test Water/NWE mixture (Khan et al, 1992), data from (Li, 2018)
	bool verbose = false;
	std::cout << (verbose ? "TESTING NP STABILITYFLASH\n" : "");
	int error_output = 0;

    int np_max = 3;

	std::vector<std::string> comp = {"H2O","CO2", "C1", "C2-3", "C4-6", "C7-14", "C15-24", "C25+"};
	CompData comp_data = CompData(comp);
	comp_data.Pc = {220.48, 73.76, 46.0, 45.05, 33.50,24.24, 18.03, 17.26};
	comp_data.Tc = {647.3, 304.20, 190.6, 343.64, 466.41, 603.07, 733.79, 923.2};
	comp_data.ac = {0.344, 0.225, 0.008, 0.13, 0.244, 0.6, 0.903, 1.229};
	comp_data.kij = std::vector<double>(8*8, 0.);
	comp_data.set_binary_coefficients(0, {0., 0.1896, 0.4850, 0.5, 0.5, 0.5, 0.5, 0.5});
	comp_data.set_binary_coefficients(1, {0.1896, 0., 0.12, 0.12, 0.12, 0.09, 0.09, 0.09});

	std::vector<double> z = {0.5, 0.251925, 0.050625, 0.02950, 0.0371, 0.071575, 0.03725, 0.022025};

	FlashParams flash_params(comp_data);
    flash_params.split_switch_tol = 1e-3;
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
    flash_params.eos_params["PR"].stability_switch_tol = 1e-3;
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson,
													 InitialGuess::Yi::Wilson13};  // pure H2O initial guess

	std::vector<Reference> references = {
		Reference(200., 500., z, {0.4046631329, 0.36088833279, 0.23444853426}),
	};

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (FlashParams::StabilityVars stab_var: stab_vars)
		{
			flash_params.stability_variables = stab_var;
			for (FlashParams::SplitVars split_var: split_vars)
			{
				flash_params.split_variables = split_var;
				StabilityFlash flash(flash_params, np_max);
				for (Reference condition: references)
				{
					error_output += condition.test(&flash, verbose);
				}
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflashN_vapour_liquid_water()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflashN_vapour_liquid_water()", error_output);
	}
    return error_output;
}

int test_negativeflash2_vapour_brine()
{
	//
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P NEGATIVEFLASH VAPOUR-BRINE\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};
	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};

	CubicEoS pr(comp_data, CubicEoS::PR);

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);
	aq.set_eos_range(0, std::vector<double>{0.9, 1.});

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.split_tol = 1e-10;
	flash_params.rr2_tol = 1e-15;
	flash_params.min_z = 1e-13;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);
	flash_params.add_eos("AQ", &aq);

	std::vector<std::vector<double>> zero = {{1.-1e-11, 1e-11, 0.}, {1e-11, 1.-1e-11, 0.}};
	std::vector<double> min_z = {1e-10, 1e-5};
	std::vector<Reference> references = {
		// Freezing point
		Reference(1., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {0., 1.}),
		Reference(1., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {0., 1.}),
		Reference(1., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(1., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		Reference(1., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		Reference(1., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		Reference(1., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		Reference(1., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		Reference(1., 273.15, {0.5, 0.25, 0.25}, {0.4970505533, 0.5029494467}),
		Reference(1., 273.15, {0.8, 0.1, 0.1}, {0.7992302375, 0.2007697625}),
		Reference(1., 273.15, {0.9, 0.05, 0.05}, {0.8999551701, 0.1000448299}),
		Reference(1., 273.15, zero[0], {1., 0.}),
		Reference(1., 273.15, zero[1], {0., 1.}),
		Reference(10., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {0., 1.}),
		Reference(10., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {0., 1.}),
		Reference(10., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(10., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		Reference(10., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		Reference(10., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		Reference(10., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		Reference(10., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		Reference(10., 273.15, {0.5, 0.25, 0.25}, {0.5028281496, 0.4971718504}),
		Reference(10., 273.15, {0.8, 0.1, 0.1}, {0.8048675531, 0.1951324469}),
		Reference(10., 273.15, {0.9, 0.05, 0.05}, {0.9054062135, 0.0945937865}),
		Reference(10., 273.15, zero[0], {1., 0.}),
		Reference(10., 273.15, zero[1], {0., 1.}),
		Reference(100., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {0., 1.}),
		Reference(100., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {0., 1.}),
		Reference(100., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(100., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		Reference(100., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		Reference(100., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		Reference(100., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		Reference(100., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		Reference(100., 273.15, {0.5, 0.25, 0.25}, {0.5132407229, 0.4867592771}),
		Reference(100., 273.15, {0.8, 0.1, 0.1}, {0.8204201149, 0.1795798851}),
		Reference(100., 273.15, {0.9, 0.05, 0.05}, {0.9209840974, 0.07901590259}),
		Reference(100., 273.15, zero[0], {1., 0.}),
		Reference(100., 273.15, zero[1], {0., 1.}),

		// Boiling point
		// Reference(1., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.838212638, 0., 0.}),
		// Reference(1., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.103157324, 0., 0.}),
		Reference(1., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(1., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		// Reference(1., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		// Reference(1., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		// Reference(1., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		// Reference(1., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		// Reference(1., 373.15, {0.5, 0.25, 0.25}, {0.5210220846, 0., 0.}),
		// Reference(1., 373.15, {0.8, 0.1, 0.1}, {0.1795798851, 0.8204201149}),
		// Reference(1., 373.15, {0.9, 0.05, 0.05}, {-74.85585513, 0., 0.}),
		// Reference(1., 373.15, zero[0], {0.1000448299, 0.8999551701}),
		// Reference(1., 373.15, zero[1], {0.1000448299, 0.8999551701}),
		Reference(10., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {0., 1.}),
		Reference(10., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {0., 1.}),
		Reference(10., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(10., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		// Reference(10., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		// Reference(10., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		Reference(10., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		Reference(10., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		// Reference(10., 373.15, {0.5, 0.25, 0.25}, {0.439293082, 0.560706918}),
		// Reference(10., 373.15, {0.8, 0.1, 0.1}, {0.7763746233, 0.2236253767}),
		// Reference(10., 373.15, {0.9, 0.05, 0.05}, {0.8887319884, 0.1112680116}),
		// Reference(10., 373.15, zero[0], {1., 0.}),
		// Reference(10., 373.15, zero[1], {0., 1.}),
		// Reference(100., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {0., 1.}),
		// Reference(100., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {0., 1.}),
		Reference(100., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(100., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		Reference(100., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		Reference(100., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		Reference(100., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		Reference(100., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		// Reference(100., 373.15, {0.5, 0.25, 0.25}, {0.4958719596, 0.5041280404}),
		// Reference(100., 373.15, {0.8, 0.1, 0.1}, {0.8031386567, 0.1968613433}),
		// Reference(100., 373.15, {0.9, 0.05, 0.05}, {0.9053836364, 0.09461636357}),
		// Reference(100., 373.15, zero[0], {1., 0.}),
		// Reference(100., 373.15, zero[1], {0., 1.}),

		// Difficult conditions
		Reference(50., 293.15, {0.1111118889, 0.8888881111, 0.}, {0.1129686354, 0.8870313646}),
		Reference(50., 293.15, {0.222222778, 0.777777222, 0.}, {0.2268868604, 0.7731131396}),
		// Reference(109.29, 300.1, {0.999, 0.001, 0.}, {1., 0.}), // difficult conditions
		Reference(30., 330., {0.6, 0.39, 0.01}, {0.6019105675, 0.3980894325}) // difficult conditions
	};

	// Test conventional (y/x) phase ordering
	std::cout << (verbose ? "CALCULATING IN ORDER: AQ-V\n" : "");
	std::vector<FlashParams::SplitVars> vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (FlashParams::SplitVars var: vars)
		{
			flash_params.split_variables = var;
			NegativeFlash flash(flash_params, {"AQ", "PR"}, {InitialGuess::Ki::Henry_AV});
			for (Reference condition: references)
			{
				error_output += condition.test(&flash, verbose);
			}
		}
	}

	// Test reverse phase ordering
	std::cout << (verbose ? "CALCULATING IN ORDER: V-AQ\n" : "");
	for (FlashParams::SplitVars var: vars)
	{
		flash_params.split_variables = var;
		NegativeFlash flash(flash_params, {"AQ", "PR"}, {InitialGuess::Ki::Henry_AV});
		for (Reference condition: references)
		{
			error_output += condition.test(&flash, verbose);
		}
	}

	for (Reference condition: references)
	{
		condition.nu_ref = std::vector<double>{condition.nu_ref[1], condition.nu_ref[0]};
		for (FlashParams::SplitVars var: vars)
		{
			flash_params.split_variables = var;
			NegativeFlash flash(flash_params, {"PR", "AQ"}, {InitialGuess::Ki::Henry_VA});
			error_output += condition.test(&flash, verbose);
		}
	}
	
	if (error_output > 0)
	{
		print("Errors occurred in test_negativeflash2_vapour_brine()", error_output);
	}
	else
	{
		print("No errors occurred in test_negativeflash2_vapour_brine()", error_output);
	}
	return error_output;
}

int test_stabilityflash2_vapour_brine()
{
	//
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P STABILITYFLASH VAPOUR-BRINE\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2"};
	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75};
	comp_data.Tc = {647.14, 304.10};
	comp_data.ac = {0.328, 0.239};
	comp_data.kij = std::vector<double>(2*2, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014});
	comp_data.Mw = {18.015, 44.01};

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.rr2_tol = 1e-15;
	flash_params.min_z = 1e-13;
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	pr.set_preferred_roots(0, 0.6, EoS::RootFlag::MAX);
	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson,
													 0, 1};
	flash_params.eos_params["PR"].stability_switch_tol = 1e-2;
	flash_params.eos_params["PR"].stability_max_iter = 10;
	flash_params.eos_params["PR"].root_flag = EoSParams::RootFlag::MINMAX;

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);
	aq.set_eos_range(0, std::vector<double>{0.6, 1.});
	flash_params.add_eos("AQ", &aq);
	flash_params.eos_params["AQ"].initial_guesses = {//InitialGuess::Yi::Henry,
									 				 0};
	flash_params.eos_params["AQ"].stability_max_iter = 10;
	
	std::vector<std::vector<double>> zero = {{1., 0.}, {0., 1.}};
	std::vector<double> min_z = {1e-10, 1e-5};
	std::vector<Reference> references = {
		Reference(1.000000, 273.150000, {1e-10, 1}, {1}),
		Reference(1.000000, 273.150000, {1e-05, 0.99999}, {1}),
		Reference(1.000000, 273.150000, {1, 1e-10}, {1}),
		Reference(1.000000, 273.150000, {0.99999, 1e-05}, {1}),
		Reference(1.000000, 273.150000, {0.5, 0.5}, {0.497359, 0.502641}),
		Reference(1.000000, 273.150000, {0.777777, 0.222223}, {0.22266, 0.77734}),
		Reference(1.000000, 273.150000, {0.8, 0.2}, {0.200261, 0.799739}),
		Reference(1.000000, 273.150000, {0.9, 0.1}, {0.0994674, 0.900533}),
		Reference(1.000000, 273.150000, {1, 0}, {1}),
		Reference(1.000000, 273.150000, {0, 1}, {1}),
		Reference(10.000000, 273.150000, {1e-10, 1}, {1}),
		Reference(10.000000, 273.150000, {1e-05, 0.99999}, {1}),
		Reference(10.000000, 273.150000, {1, 1e-10}, {1}),
		Reference(10.000000, 273.150000, {0.99999, 1e-05}, {1}),
		Reference(10.000000, 273.150000, {0.5, 0.5}, {0.494178, 0.505822}),
		Reference(10.000000, 273.150000, {0.777777, 0.222223}, {0.212756, 0.787244}),
		Reference(10.000000, 273.150000, {0.8, 0.2}, {0.190242, 0.809758}),
		Reference(10.000000, 273.150000, {0.9, 0.1}, {0.0889301, 0.91107}),
		Reference(10.000000, 273.150000, {1, 0}, {1}),
		Reference(10.000000, 273.150000, {0, 1}, {1}),
		Reference(100.000000, 273.150000, {1e-10, 1}, {1}),
		Reference(100.000000, 273.150000, {1e-05, 0.99999}, {1}),
		Reference(100.000000, 273.150000, {1, 1e-10}, {1}),
		Reference(100.000000, 273.150000, {0.99999, 1e-05}, {1}),
		Reference(100.000000, 273.150000, {0.5, 0.5}, {0.516911, 0.483089}),
		Reference(100.000000, 273.150000, {0.777777, 0.222223}, {0.194878, 0.805122}),
		Reference(100.000000, 273.150000, {0.8, 0.2}, {0.17182, 0.82818}),
		Reference(100.000000, 273.150000, {0.9, 0.1}, {0.0680642, 0.931936}),
		Reference(100.000000, 273.150000, {1, 0}, {1}),
		Reference(100.000000, 273.150000, {0, 1}, {1}),
		Reference(1.000000, 373.150000, {1e-10, 1}, {1}),
		Reference(1.000000, 373.150000, {1e-05, 0.99999}, {1}),
		Reference(1.000000, 373.150000, {1, 1e-10}, {1}),
		Reference(1.000000, 373.150000, {0.99999, 1e-05}, {1}),
		Reference(1.000000, 373.150000, {0.5, 0.5}, {1}),
		Reference(1.000000, 373.150000, {0.777777, 0.222223}, {1}),
		Reference(1.000000, 373.150000, {0.8, 0.2}, {1}),
		Reference(1.000000, 373.150000, {0.9, 0.1}, {1}),
		Reference(1.000000, 373.150000, {1, 0}, {1}),
		Reference(1.000000, 373.150000, {0, 1}, {1}),
		Reference(10.000000, 373.150000, {1e-10, 1}, {1}),
		Reference(10.000000, 373.150000, {1e-05, 0.99999}, {1}),
		Reference(10.000000, 373.150000, {1, 1e-10}, {1}),
		Reference(10.000000, 373.150000, {0.99999, 1e-05}, {1}),
		Reference(10.000000, 373.150000, {0.5, 0.5}, {0.43892, 0.56108}),
		Reference(10.000000, 373.150000, {0.777777, 0.222223}, {0.248236, 0.751764}),
		Reference(10.000000, 373.150000, {0.8, 0.2}, {0.223208, 0.776792}),
		Reference(10.000000, 373.150000, {0.9, 0.1}, {0.110584, 0.889416}),
		Reference(10.000000, 373.150000, {1, 0}, {1}),
		Reference(10.000000, 373.150000, {0, 1}, {1}),
		Reference(100.000000, 373.150000, {1e-10, 1}, {1}),
		Reference(100.000000, 373.150000, {1e-05, 0.99999}, {1}),
		Reference(100.000000, 373.150000, {1, 1e-10}, {1}),
		Reference(100.000000, 373.150000, {0.99999, 1e-05}, {1}),
		Reference(100.000000, 373.150000, {0.5, 0.5}, {0.497271, 0.502729}),
		Reference(100.000000, 373.150000, {0.777777, 0.222223}, {0.215236, 0.784764}),
		Reference(100.000000, 373.150000, {0.8, 0.2}, {0.192236, 0.807764}),
		Reference(100.000000, 373.150000, {0.9, 0.1}, {0.0887389, 0.911261}),
		Reference(100.000000, 373.150000, {1, 0}, {1}),
		Reference(100.000000, 373.150000, {0, 1}, {1}),

		// Difficult conditions
		// Reference(50., 293.15, {0.1111118889, 0.8888881111}, {0.1129686354, 0.8870313646}),
		// Reference(50., 293.15, {0.222222778, 0.777777222}, {0.2268868604, 0.7731131396}),
	};

	// references = {
	// 	// Reference(50., 293.15, {0.1111118889, 0.8888881111}, {0.1129686354, 0.8870313646}),
	// 	Reference(35.000000, 273.150000, {1.05263947e-01, 1.-1.05263947e-01}, {0.4717630501, 0.5282369499}),
	// };

	std::string ref_string = "\tstd::vector<Reference> references = {\n";
	bool write = true;

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (FlashParams::StabilityVars stab_var: stab_vars)
		{
			flash_params.stability_variables = stab_var;
			for (FlashParams::SplitVars split_var: split_vars)
			{
				flash_params.split_variables = split_var;
				StabilityFlash flash(flash_params, 3);
				for (Reference condition: references)
				{
					// condition.print_conditions(true);
					error_output += condition.test(&flash, verbose);
					if (write) { condition.write_ref(ref_string); }
				}
				write = false;
			}
		}
	}
	ref_string += "\t};\n";

	if (error_output > 0)
	{
		std::cout << ref_string;
		print("Errors occurred in test_stabilityflash2_vapour_brine()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflash2_vapour_brine()", error_output);
	}
	return error_output;
}

int test_stabilityflashN_vapour_brine()
{
	//
	bool verbose = false;
	std::cout << (verbose ? "TESTING NP STABILITYFLASH VAPOUR-BRINE\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};
	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.rr2_tol = 1e-15;
	flash_params.min_z = 1e-13;
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	pr.set_preferred_roots(0, 0.6, EoS::RootFlag::MAX);
	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson,
													 0, 1, 2};
	flash_params.eos_params["PR"].stability_switch_tol = 1e-2;
	flash_params.eos_params["PR"].stability_max_iter = 10;
	flash_params.eos_params["PR"].root_flag = EoSParams::RootFlag::MINMAX;

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);
	aq.set_eos_range(0, std::vector<double>{0.6, 1.});
	flash_params.add_eos("AQ", &aq);
	flash_params.eos_params["AQ"].initial_guesses = {//InitialGuess::Yi::Henry,
									 				 0};
	flash_params.eos_params["AQ"].stability_max_iter = 10;
	
	std::vector<std::vector<double>> zero = {{1.-1e-11, 1e-11, 0.}, {1e-11, 1.-1e-11, 0.}};
	std::vector<double> min_z = {1e-10, 1e-5};
	std::vector<Reference> references = {
		// Freezing point
		Reference(1., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(1., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(1., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(1., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(1., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(1., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(1., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(1., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(1., 273.15, {0.5, 0.25, 0.25}, {0.4970505533, 0.5029494467}),
		Reference(1., 273.15, {0.8, 0.1, 0.1}, {0.7992302375, 0.2007697625}),
		Reference(1., 273.15, {0.9, 0.05, 0.05}, {0.8999551701, 0.1000448299}),
		Reference(1., 273.15, zero[0], {1.}),
		Reference(1., 273.15, zero[1], {1.}),
		Reference(10., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(10., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(10., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(10., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(10., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(10., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(10., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(10., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(10., 273.15, {0.5, 0.25, 0.25}, {0.5028281496, 0.4971718504}),
		Reference(10., 273.15, {0.8, 0.1, 0.1}, {0.8048675531, 0.1951324469}),
		Reference(10., 273.15, {0.9, 0.05, 0.05}, {0.9054062135, 0.0945937865}),
		Reference(10., 273.15, zero[0], {1.}),
		Reference(10., 273.15, zero[1], {1.}),
		Reference(100., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(100., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(100., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(100., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(100., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(100., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(100., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(100., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(100., 273.15, {0.5, 0.25, 0.25}, {0.5132407229, 0.4867592771}),
		Reference(100., 273.15, {0.8, 0.1, 0.1}, {0.8204201149, 0.1795798851}),
		Reference(100., 273.15, {0.9, 0.05, 0.05}, {0.9209840974, 0.07901590259}),
		Reference(100., 273.15, zero[0], {1.}),
		Reference(100., 273.15, zero[1], {1.}),

		// Boiling point
		Reference(1., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(1., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(1., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(1., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(1., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(1., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(1., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(1., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(1., 373.15, {0.5, 0.25, 0.25}, {1.}),
		Reference(1., 373.15, {0.8, 0.1, 0.1}, {1.}),
		Reference(1., 373.15, {0.9, 0.05, 0.05}, {1.}),
		Reference(1., 373.15, zero[0], {1.}),
		Reference(1., 373.15, zero[1], {1.}),
		Reference(10., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(10., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(10., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(10., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(10., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(10., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(10., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(10., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(10., 373.15, {0.5, 0.25, 0.25}, {0.5607295931, 0.4392704069}),
		Reference(10., 373.15, {0.8, 0.1, 0.1}, {0.7763746233, 0.2236253767}),
		Reference(10., 373.15, {0.9, 0.05, 0.05}, {0.8887319884, 0.1112680116}),
		Reference(10., 373.15, zero[0], {1.}),
		Reference(10., 373.15, zero[1], {1.}),
		Reference(100., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(100., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(100., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(100., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(100., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(100., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(100., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(100., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(100., 373.15, {0.5, 0.25, 0.25}, {0.4958719596, 0.5041280404}),
		Reference(100., 373.15, {0.8, 0.1, 0.1}, {0.8031386567, 0.1968613433}),
		Reference(100., 373.15, {0.9, 0.05, 0.05}, {0.9053836364, 0.09461636357}),
		Reference(100., 373.15, zero[0], {1.}),
		Reference(100., 373.15, zero[1], {1.}),

		// Difficult conditions
		// Reference(50., 283.15, zero[1], {1.}),
		// Reference(50., 293.15, {0.1111118889, 0.8888881111, 0.}, {0.1129686354, 0.8870313646}),
		// Reference(50., 293.15, {0.222222778, 0.777777222, 0.}, {0.2268868604, 0.7731131396}),
		Reference(109.29, 300.1, {0.999, 0.001, 0.}, {1.}), // difficult conditions
		Reference(30., 330., {0.6, 0.39, 0.01}, {0.6019105675, 0.3980894325}) // difficult conditions
	};

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (FlashParams::StabilityVars stab_var: stab_vars)
		{
			flash_params.stability_variables = stab_var;
			for (FlashParams::SplitVars split_var: split_vars)
			{
				flash_params.split_variables = split_var;
				StabilityFlash flash(flash_params, 3);
				for (Reference condition: references)
				{
					condition.print_conditions(true);
					error_output += condition.test(&flash, verbose);
				}
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflashN_vapour_brine()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflashN_vapour_brine()", error_output);
	}
	return error_output;
}

int test_negativeflash2_vapour_brine_ions()
{
	//
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P NEGATIVEFLASH VAPOUR-BRINE WITH IONS\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};
	std::vector<std::string> ions = {"Na+", "Cl-"};

	CompData comp_data(comp, ions);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};
	comp_data.charge = {1, -1};

	CubicEoS pr(comp_data, CubicEoS::PR);

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);

	FlashParams flash_params(comp_data);
	flash_params.rr_max_iter = 10;
	flash_params.split_max_iter = 50;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);
	flash_params.add_eos("AQ", &aq);

	std::vector<std::vector<double>> Z = {{0.5, 0.25, 0.2, 0.025, 0.025}, {0.5, 5e-06, 0.449995, 0.025, 0.025}};
	std::vector<Reference> references = {
		Reference(1.01325, 298.15, Z[0], {0.536237103, 0.463762897}),
		Reference(1.01325, 298.15, Z[1], {0.5361915827, 0.4638084173}),
		Reference(100., 335., Z[0], {0.551499262, 0.448500738}),
		Reference(100., 335., Z[1], {0.5491050143, 0.4508949857}),
		Reference(54.5, 277.6, Z[0], {0.5558883338, 0.4441116662 }),
		Reference(54.5, 277.6, Z[1], {0.5502680968, 0.4497319032}),
	};

	std::vector<FlashParams::SplitVars> vars = {FlashParams::nik};//, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (FlashParams::SplitVars var: vars)
		{
			flash_params.split_variables = var;
			NegativeFlash flash(flash_params, {"AQ", "PR"}, {InitialGuess::Ki::Henry_AV});
			for (Reference condition: references)
			{
				error_output += condition.test(&flash, verbose);
			}
		}
	}
	if (error_output > 0)
	{
		print("Errors occurred in test_negativeflash2_vapour_brine_ions()", error_output);
	}
	else
	{
		print("No errors occurred in test_negativeflash2_vapour_brine_ions()", error_output);
	}
	return error_output;
}

int test_stabilityflashN_vapour_brine_si()
{
	bool verbose = false;
	std::cout << (verbose ? "TESTING NP STABILITYFLASH VAPOUR-BRINE-sI\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};

	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};

	CubicEoS pr(comp_data, CubicEoS::PR);
	pr.set_eos_range(0, std::vector<double>{0., 0.9});

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);
	aq.set_eos_range(0, std::vector<double>{0.9, 1.});

	Ballard si(comp_data, "sI");

	FlashParams flash_params(comp_data);
	flash_params.add_eos("PR", &pr);
	flash_params.add_eos("AQ", &aq);
	flash_params.add_eos("sI", &si);

	flash_params.split_variables = FlashParams::nik;
	flash_params.split_switch_tol = 1e-3;
	flash_params.stability_variables = FlashParams::alpha;
	flash_params.verbose = verbose;

	std::vector<int> initial_guesses = {InitialGuess::Yi::Henry, 1, 2};

	StabilityFlash flash(flash_params, 2);
	std::vector<double> pres, temp, nu, x, nu_ref, x_ref;

	pres = {5., 10., 15.};
	std::vector<double> xH2O = {0.5, 0.86, 0.88, 0.9, 0.95};
	std::vector<double> xCO2 = {1e-3, 0.1, 0.5, 0.9, 1.-1e-3};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (double p: pres)
		{
			for (double X0: xH2O)
			{
				for (double X1: xCO2)
				{
					std::vector<double> z = {X0, (1.-X0)*X1, (1.-X0)*(1.-X1)};
					// print("z", z);
					error_output += flash.evaluate(p, 100, z);
					// nu = flash.get_phase_fractions();
					// x = flash.get_phase_compositions();
					// print("nu", nu);
					// print("x", x, static_cast<int>(nu.size()));
					// print("Error", error_output);
				}
			}
		}
	}
	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflashN_vapour_brine_si()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflashN_vapour_brine_si()", error_output);
	}
	return error_output;
}

int test_stabilityflashN_vapour_brine_ice()
{
	bool verbose = false;
	std::cout << (verbose ? "TESTING NP STABILITYFLASH VAPOUR-BRINE WITH ICE\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};

	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};

	CubicEoS pr(comp_data, CubicEoS::PR);

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);

	PureSolid ice(comp_data, "Ice");

	FlashParams flash_params(comp_data);
	flash_params.add_eos("PR", &pr);
	flash_params.add_eos("AQ", &aq);
	flash_params.add_eos("Ih", &ice);

	flash_params.rr_max_iter = 10;
	flash_params.split_max_iter = 50;
	flash_params.split_variables = FlashParams::lnK;
	flash_params.verbose = verbose;

	StabilityFlash flash(flash_params, 3);
	std::vector<double> pres, temp, nu, x, nu_ref, x_ref;

	pres = {5., 10., 15.};
	temp = {253.15, 263.15, 273.15, 283.15, 298.15};
	std::vector<double> z = {0.8, 0.2-1e-3, 1e-3};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (double T: temp)
		{
			for (double p: pres)
			{
				// std::cout << "Calculating T = " << T << "; P = " << p << std::endl;
				error_output += flash.evaluate(p, T, z);
				Flash::Results flash_results = flash.get_flash_results();
				// print("nu", flash_results.nu);
				// print("x", flash_results.X, static_cast<int>(flash_results.nu.size()));
				// print("Error", error_output);
			}
		}
	}
	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflashN_vapour_brine_ice()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflashN_vapour_brine_ice()", error_output);
	}
	return error_output;
}
