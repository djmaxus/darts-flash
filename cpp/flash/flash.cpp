#include <iostream>
#include <cmath>
#include <vector>
#include <numeric>
#include <algorithm>
#include <map>

#include "dartsflash/maths/maths.hpp"
#include "dartsflash/maths/geometry.hpp"
#include "dartsflash/eos/trial_phase.hpp"
#include "dartsflash/flash/flash.hpp"
#include "dartsflash/flash/flash_params.hpp"
#include "dartsflash/stability/stability.hpp"
#include "dartsflash/phase-split/twophasesplit.hpp"
#include "dartsflash/phase-split/multiphasesplit.hpp"

Flash::Flash(FlashParams& flashparams, int np_max_) {
	this->flash_params = flashparams;

    this->nc = flash_params.nc;
	this->ni = flash_params.ni;
	this->ns = flash_params.ns;

    this->np_max = np_max_;

    this->z.resize(ns);
    this->eos.reserve(np_max);
    this->nu.reserve(np_max);
    this->X.reserve(np_max*ns);

    this->stationary_points.reserve(np_max);
    this->ref_compositions.reserve(np_max);
    this->sp_idxs.reserve(np_max + 1);
}

void Flash::init(double p, double T, std::vector<double>& z_)
{
    // Set iterations to zero
    total_ssi_flash_iter = total_ssi_stability_iter = total_newton_flash_iter = total_newton_stability_iter = 0;
    
    // Initialize EoS and InitialGuess at p, T
    this->flash_params.init_eos(p, T);

    // Check if feed composition needs to be corrected for 0 values
    z = std::vector<double>(ns);
    for (int i = 0; i < ns; i++)
    {
        z[i] = (z_[i] > flash_params.min_z) ? z_[i] : flash_params.min_z;
    }
    return;
}

int Flash::run_stability(std::vector<TrialPhase>& ref_comps)
{
    // Run stability test on feed X with initial guesses Y
    Stability stab(flash_params);
    stab.init(ref_comps[0]);

    // Add each of the minima trivial stationary points (for np > 1, this corresponds to all phase compositions)
    this->stationary_points.clear();
    if (ref_comps.size() > 1 || stab.is_minimum(ref_comps[0]))
    {
        this->stationary_points = ref_comps;
    }
    sp_idxs = std::vector<int>(stationary_points.size());
    std::iota(sp_idxs.begin(), sp_idxs.end(), 0);

    // Iterate over initial guesses in Y to run stability tests
    for (auto it: flash_params.eos_params)
    {
        // it.first: eos_name; it.second: EoSParams
        std::vector<TrialPhase> trial_comps = it.second.evaluate_initial_guesses(it.first, ref_comps);
        for (TrialPhase trial_comp: trial_comps)
        {
            int error = stab.run(trial_comp);

            if (error > 0)
            {
                return error;
            }

            // Get TPD value and check if it is already in the list
            if (trial_comp.is_preferred_root >= EoS::RootSelect::ACCEPT && !this->compare_stationary_points(trial_comp))
            {
                // No duplicate found: add stationary point to vector of stationary points
                stationary_points.push_back(trial_comp);
            }

            // Get number of iterations from stability
            this->total_ssi_stability_iter += stab.get_ssi_iter();
            this->total_newton_stability_iter += stab.get_newton_iter();
        }
    }
    return 0;
}

int Flash::run_split(std::vector<int>& sp_idxs_)
{
    // Choose proper set of lnK
    std::vector<double> lnK = this->generate_lnK(sp_idxs_);
    std::vector<TrialPhase> trial_comps{};
    for (int sp_idx: sp_idxs_)
    {
        trial_comps.push_back(stationary_points[sp_idx]);
    }

    // Initialize split object
    int error = 0;
    np = static_cast<int>(sp_idxs_.size());
    if (np == 2)
    {
        // Initialize PhaseSplit object for two phases
        TwoPhaseSplit split(flash_params);

        // Run split and return nu and x
        error = split.run(z, lnK, trial_comps);  // Run multiphase split algorithm at P, T, z with initial guess lnK
        nu = split.getnu();
        X = split.getx();
        gibbs = split.get_gibbs();
        total_ssi_flash_iter += split.get_ssi_iter();
        total_newton_flash_iter += split.get_newton_iter();
    }
    else
    {
        // Initialize MultiPhaseSplit object for 3 or more phases
        MultiPhaseSplit split(flash_params, np);

        // Run split and return nu and x
        error = split.run(z, lnK, trial_comps);  // Run multiphase split algorithm at P, T, z with initial guess lnK
        nu = split.getnu();
        X = split.getx();
        gibbs = split.get_gibbs();
        total_ssi_flash_iter += split.get_ssi_iter();
        total_newton_flash_iter += split.get_newton_iter();
    }

    // Determine output value
    if (error)
    {
        // Error occurred: return 1
        return 1;
    }
    else
    {
        // Check if all phases are positive
        // If so, return 0
        int output = 0;
        ref_compositions.clear();
        for (int j = 0; j < np; j++)
        {
            if (nu[j] < 0.)
            {
                // If at least one phase is negative, return -1
                output = -1;
            }
            
            std::vector<double> xj(ns);
		    auto start = X.begin() + j*ns;
		    std::copy(start, start + ns, xj.begin());

            ref_compositions.push_back(TrialPhase{trial_comps[j].eos_name, xj});
        }
        return output;
    }
}

int Flash::run_loop(std::vector<TrialPhase>& ref_comps)
{
    // Perform stability test starting from each of the initial guesses
    int stability_output = this->run_stability(ref_comps);
    if (stability_output > 0)
    {
        // Error occurred in stability test
        return stability_output;
    }

    // Count number of stationary points with negative TPD and sort stationary points according to tpd
    int negative_tpds = 0;
    for (size_t j = sp_idxs.size(); j < stationary_points.size(); j++)
    {
        if (stationary_points[j].tpd < -flash_params.tpd_tol  // negative TPD
            && stationary_points[j].is_preferred_root >= EoS::RootSelect::ACCEPT)  // accept root
        {
            negative_tpds++;

            // Find location of tpd in sorted idxs
            std::vector<int>::iterator it;
            for (it = sp_idxs.begin(); it != sp_idxs.end(); it++)
            {
                if (stationary_points[*it].tpd > stationary_points[j].tpd)
                {
                    sp_idxs.insert(it, static_cast<int>(j));
                    break;
                }
            }
            if (it == sp_idxs.end())
            {
                sp_idxs.push_back(static_cast<int>(j));
            }
        }
    }

    // If no negative TPDs, feed is stable, return -1
    if (negative_tpds == 0)
    {
        return -1;
    }
    else
    {
        // Else not stable
        if (flash_params.verbose)
        {
            print("Unstable feed", "============");
            print("X", X, np);
            for (TrialPhase stationary_point : stationary_points) { stationary_point.print_point(); }
        }

        // Determine lnK-values from set of stationary points for phase split
        int tot_sp = static_cast<int>(sp_idxs.size());
        int split_output = 0;

        if (tot_sp == 2)  // TWO STATIONARY POINTS <= 0 --> 2 PHASES: Select both stationary points
        {
            // Run split with two stationary points
            split_output = this->run_split(sp_idxs);
            return 0;
        }
        else  // In case of 3 or more stationary points <= 0, find possible combinations and determine which one has lowest Gibbs energy
        {
            // Maximum number of phases is equal to number of stationary points, but not larger than NC
            int NP_max = std::min(tot_sp, nc);
            bool stable = false;
            double gibbs_lowest = NAN;
            std::vector<double> nu_stable, X_stable;
            std::vector<std::string> eos_stable;

            for (int NP = NP_max; NP >= 2; NP--)
            {
                // Find combinations of stationary points of maximum length np
                Combinations c(tot_sp, NP);
                
                // Find for each combination of stationary points if simplex with stationary points as vertices contains the feed composition
                // If it does, combination may be a solution to the multiphase equilibrium
                for (std::vector<int>& combination: c.combinations)
                {
                    std::vector<int> idxs = {};
                    std::vector<std::vector<double>> coords = {};
                    for (int idx: combination)
                    {
                        int sp_idx = sp_idxs[idx];
                        coords.push_back(stationary_points[sp_idx].y);
                        idxs.push_back(sp_idx);
                    }
                    if (NP < NP_max || is_in_simplex(z, coords))
                    {
                        split_output = this->run_split(idxs);
                        
                        if (split_output == 0 && (std::isnan(gibbs_lowest) || gibbs < gibbs_lowest))
                        {
                            gibbs_lowest = gibbs;
                            nu_stable = nu;
                            X_stable = X;
                            eos_stable = eos;
                            stable = true;
                        }
                    }
                }
                if (stable)
                {
                    // NP-Split success, found equilibrium of NP phases
                    ref_compositions.clear();
                    for (size_t j = 0; j < nu_stable.size(); j++)
                    {
                        std::vector<double> Xj(nc);
                        std::copy(X_stable.begin() + j*nc, X_stable.begin() + (j+1)*nc, Xj.begin());
                        ref_compositions.push_back(TrialPhase{eos_stable[j], Xj});
                    }
                    return 0;
                }
            }
            return 1;
        }
    }
}

std::vector<double> Flash::generate_lnK(std::vector<int>& sp_idxs_)
{
    // Determine lnK initialization for phase split
    np = static_cast<int>(sp_idxs_.size());
    std::vector<double> lnY0(nc);
    int sp_idx = sp_idxs_[0];
    for (int i = 0; i < nc; i++)
    {
        lnY0[i] = std::log(stationary_points[sp_idx].Y[i]);
    }
    eos = {stationary_points[sp_idx].eos_name};
    
    std::vector<double> lnK((np-1)*nc);
    for (int j = 1; j < np; j++)
    {
        sp_idx = sp_idxs_[j];
        for (int i = 0; i < nc; i++)
        {
            lnK[(j-1) * nc + i] = std::log(stationary_points[sp_idx].Y[i]) - lnY0[i];
        }
        eos.push_back(stationary_points[sp_idx].eos_name);
    }

    return lnK;
}

bool Flash::compare_stationary_points(TrialPhase& stationary_point)
{
    // Compare stationary point with entries in vector of stationary points to check if it is unique
    // Returns true if point is already in the list
    double tpd0 = stationary_point.tpd;
    double lntpd0 = std::log(std::fabs(tpd0));
    for (size_t j = 0; j < stationary_points.size(); j++)
    {
        double tpdj = stationary_points[j].tpd;
        // For small tpd difference (tpd < 1), compare absolute difference; for large tpd values, logarithmic scale is used to compare
        double tpd_diff = lntpd0 < 0. ? std::fabs(tpdj-tpd0) : lntpd0 - std::log(std::fabs(tpdj) + 1e-15);
        if (stationary_points[j].eos_name == stationary_point.eos_name // eos is the same
            && (tpd_diff < flash_params.tpd_tol)) // tpd is within tolerance
        {
            // Similar TPD found; Check if composition is also the same
            if (compare_compositions(stationary_point.Y, stationary_points[j].Y, flash_params.comp_tol))
            {
                return true;
            }
        }
    }
    return false;
}
