import numpy as np
import xarray as xr
import pytest

from dartsflash.libflash import InitialGuess, CubicEoS, AQEoS
from dartsflash.pyflash import PyFlash
from .conftest import mixture_brine_vapour


# Test of full compositional space for H2O-CO2-C1 mixture
@pytest.fixture()
def flash_obj(mixture_brine_vapour):
    mix = mixture_brine_vapour
    f = PyFlash(mixture=mix)

    f.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])
    f.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012}))
    f.init_flash(stabilityflash=False, eos=["AQ", "PR"], initial_guess=[InitialGuess.Henry_AV])
    return f


@pytest.fixture()
def ref_brine_vapour2():
    return "./tests/python/data/ref_flash_brine_vapour2.h5"


def test_brine_vapour2(flash_obj, ref_brine_vapour2):
    state_spec = ["pressure", "temperature"]
    zrange = np.concatenate([np.array([1e-12, 1e-10, 1e-8]),
                             np.linspace(1e-6, 1. - 1e-6, 10),
                             np.array([1. - 1e-8, 1. - 1e-10, 1. - 1e-12])])
    dimensions = {"pressure": np.array([1., 2., 5., 10., 50., 100., 200., 400.]),
                  "temperature": np.arange(273.15, 373.15, 10),
                  "H2O": zrange,
                  "CO2": zrange,
                  }
    constants = {"C1": 1.}
    dims_order = ["H2O",
                  "CO2",
                  "pressure",
                  "temperature"]

    results = flash_obj.evaluate_flash(state_spec=state_spec, dimensions=dimensions, constants=constants,
                                       mole_fractions=True, dims_order=dims_order)
    results = results[["nu", "X", "eos"]]

    if 1:
        # Use assertions from xarray
        ref = xr.open_dataset(ref_brine_vapour2, engine='h5netcdf')
        ref = ref[["nu", "X", "eos"]]
        try:
            xr.testing.assert_allclose(results, ref, rtol=1e-5)
        except AssertionError:
            for i, p in enumerate(results.pressure.values):
                for j, t in enumerate(results.temperature.values):
                    for k, h2o in enumerate(results.H2O.values):
                        for m, co2 in enumerate(results.CO2.values):
                            nu = results.isel(pressure=i, temperature=j, H2O=k, CO2=m).nu.values
                            nu_ref = ref.isel(pressure=i, temperature=j, H2O=k, CO2=m).nu.values
                            X = results.isel(pressure=i, temperature=j, H2O=k, CO2=m).X.values
                            X_ref = ref.isel(pressure=i, temperature=j, H2O=k, CO2=m).X.values
                            if not np.allclose(nu, nu_ref, rtol=1e-5, equal_nan=True):
                                raise AssertionError("nu != nu_ref", p, t, h2o, co2, nu, nu_ref)
                            elif not np.allclose(X, X_ref, rtol=1e-5, equal_nan=True):
                                raise AssertionError("X != X_ref", p, t, h2o, co2, X, X_ref)
    else:
        # Update reference file
        results.to_netcdf(ref_brine_vapour2, engine='h5netcdf')
