#include <iostream>
#include <vector>
#include <complex>
#include <cmath>
#include <numeric>

#include "dartsflash/global/global.hpp"
#include "dartsflash/global/ideal.hpp"
#include "dartsflash/maths/maths.hpp"

namespace ideal_gas {
    double P_0 = 1.;
    double T_0 = 298.15;
    double T_01 = 273.15;
    
    // gibbs energy of ideal gas at p0, T0
    std::unordered_map<std::string, double> gi_00 = {
        {"H2O", -228700}, {"CO2", -394600}, {"N2", 0}, {"H2S", -33100}, 
        {"C1", -50830}, {"C2", -32900}, {"C3", -23500}, {"iC4", -20900}, {"nC4", -17200}, {"iC5", -15229}, {"nC5", -8370}, {"nC6", -290}, {"nC7", 8120},
        {"NaCl", -384138.00} 
    };
    // molar enthalpy of ideal gas at p0, T0
    std::unordered_map<std::string, double> hi_00 = {
        {"H2O", -242000}, {"CO2", -393800}, {"N2", 0}, {"H2S", -20200}, 
        {"C1", -74900}, {"C2", -84720}, {"C3", -103900}, {"iC4", -134600}, {"nC4", -126200}, {"iC5", -165976}, {"nC5", -146500}, {"nC6", -167300}, {"nC7", -187900} ,
        {"NaCl", -411153.00}
    };
    // ideal gas heat capacity parameters [eq. 3.4]
    std::unordered_map<std::string, std::vector<double>> hi_a =  {
        {"H2O", {3.8747, 0.0231E-2, 0.1269E-5, -0.4321E-9}},
        {"CO2", {2.6751, 0.7188E-2, -0.4208E-5, 0.8977E-9}},
        {"N2", {3.4736, -0.0189E-2, 0.0971E-5, -0.3453E-9}},
        {"H2S", {3.5577, 0.1574E-2, 0.0686E-5, -0.3959E-9}},
        {"C1", {2.3902, 0.6039E-2, 0.1525E-5, -1.3234E-9}},
        {"C2", {0.8293, 2.0752E-2, -0.7699E-5, 0.8756E-9}},
        {"C3", {-0.4861, 3.6629E-2, -1.8895E-5, 3.8143E-9}},
        {"C4", {1.1410, 3.9846E-2, -1.3326E-5, -3.3940E-10}},
        {"iC4", {-0.9511, 4.9999E-2, -2.7651E-5, 5.9982E-9}},
        {"nC4", {0.4755, 4.4650E-2, -2.2041E-5, 4.2068E-9}},
        {"iC5", {-1.9942, 6.6725E-2, -3.9738E-5, 9.1735E-9}},
        {"nC5", {0.8142, 5.4598E-2, -2.6997E-5, 5.0824E-9}},
        {"nC6", {0.8338, 6.6373E-2, -3.444E-5, 6.9342E-9}},
        {"nC7", {-0.6184, 8.1268E-2, -4.388E-5, 9.2037E-9}},
        {"NaCl", {5.526, 0.1963e-2, 0., 0.}},
        {"PC1", {-3.5 / M_R, 0.005764 / M_R, 5.09E-7 / M_R, 0. }},
        {"PC2", {-0.404 / M_R, 0.0006572 / M_R, 5.41E-8 / M_R, 0.}},
        {"PC3", {-6.1 / M_R, 0.01093 / M_R, 1.41E-6 / M_R, 0.}},
        {"PC4", {-4.5 / M_R, 0.008049 / M_R, 1.04E-6 / M_R, 0.}},
    };
}

IdealGas::IdealGas(const std::vector<std::string>& components, const std::vector<std::string>& ions)
{
    this->nc = static_cast<int>(components.size());
    this->ni = static_cast<int>(ions.size());
    this->ns = nc + ni;

    T_0 = ideal_gas::T_0;
    R = M_R;

    gi_00.resize(ns);
    hi_00.resize(ns);
    hi_a.resize(ns, std::vector<double>(4));

    for (int i = 0; i < nc; i++)
    {
        gi_00[i] = ideal_gas::gi_00[components[i]];
        hi_00[i] = ideal_gas::hi_00[components[i]];
        hi_a[i] = ideal_gas::hi_a[components[i]];
    }
    for (int i = 0; i < ni; i++)
    {
        gi_00[nc + i] = ideal_gas::gi_00["NaCl"];
        hi_00[nc + i] = ideal_gas::hi_00["NaCl"];
        hi_a[nc + i] = ideal_gas::hi_a["NaCl"];
    }
}

double IdealGas::cpi(double T, int i)
{
    return hi_a[i][0]
         + hi_a[i][1] * T
         + hi_a[i][2] * std::pow(T, 2)
         + hi_a[i][3] * std::pow(T, 3);
}

double IdealGas::hi(double T, int i) {
    return hi_00[i] / R
         + hi_a[i][0] * (T-T_0) 
         + 1. / 2 * hi_a[i][1] * (std::pow(T, 2)-std::pow(T_0, 2)) 
         + 1. / 3 * hi_a[i][2] * (std::pow(T, 3)-std::pow(T_0, 3))
         + 1. / 4 * hi_a[i][3] * (std::pow(T, 4)-std::pow(T_0, 4));
}

double IdealGas::Hi(double T, int i) {
    // Integral of h(T)/RT^2 dT from T_0 to T
    return (-(hi_00[i] / R
            - hi_a[i][0] * T_0 
            - 1. / 2 * hi_a[i][1] * std::pow(T_0, 2) 
            - 1. / 3 * hi_a[i][2] * std::pow(T_0, 3)
            - 1. / 4 * hi_a[i][3] * std::pow(T_0, 4)) * (1./T - 1./T_0)
            + (hi_a[i][0] * (std::log(T) - std::log(T_0))
            + 1. / 2 * hi_a[i][1] * (T - T_0)
            + 1. / 6 * hi_a[i][2] * (std::pow(T, 2) - std::pow(T_0, 2))
            + 1. / 12 * hi_a[i][3] * (std::pow(T, 3) - std::pow(T_0, 3))));
}

double IdealGas::dHidT(double T, int i)
{
    return this->hi(T, i) / std::pow(T, 2);
}

double IdealGas::si(double p, double T, double N, int i)
{
    return this->cvi(T, i) * std::log(T / T_0) + std::log(this->V(p, T, N) / this->V(p, T_0, N));
}

double IdealGas::G(std::vector<double>::iterator x_it)
{
    double g = 0.;
    for (int i = 0; i < ns; i++)
    {
        double xi = *(x_it + i);
        if (xi > 0.)
        {   
            g += xi * std::log(xi);
        }
    }
    return g;
}

double IdealGas::H(double T, std::vector<double>::iterator x_it)
{
    double h = 0.;
    for (int i = 0; i < ns; i++)
    {
        double xi = *(x_it + i);
        h += xi * this->hi(T, i);
    }
    return h;
}

double IdealGas::S(double T, std::vector<double>::iterator n_it)
{
    double s = 0.;
    double N_inv = 1./std::accumulate(n_it, n_it + ns, 0.);
    for (int i = 0; i < ns; i++)
    {
        double n_i = *(n_it + i);
        double xi = n_i * N_inv;
        double p = 1.;
        s += n_i * this->si(p, T, n_i, i) - R * n_i * std::log(xi);
    }
    return 0.;
}

/*
double IdealGas::A(double T, std::vector<double>::iterator x_it)
{

}

double IdealGas::U(double T, std::vector<double>::iterator x_it)
{

}
*/
