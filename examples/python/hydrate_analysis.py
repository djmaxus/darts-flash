import numpy as np
import matplotlib.pyplot as plt

from dartsflash.hyflash import HyFlash
from dartsflash.libflash import InitialGuess
from dartsflash.mixtures import Mixture, ConcentrationUnits
from dartsflash.diagram import Plot

if 0:
    """CO2 sI equilibrium PT"""
    from dartsflash.libflash import CubicEoS, AQEoS, Ballard

    mix = Mixture(components=["H2O", "CO2"], name="H2O-CO2", np_max=2)
    a = HyFlash(mixture=mix)

    a.add_eos("SRK", CubicEoS(mix.comp_data, CubicEoS.SRK),
              initial_guesses=[InitialGuess.Yi.Wilson])
    a.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012}))
    a.add_hydrate_eos("sI", Ballard(mix.comp_data, "sI"))

    a.init_flash(stabilityflash=False, eos=["AQ", "SRK"], initial_guess=[InitialGuess.Henry_AV])

    ref_T = [[float(i) for i in range(271, 291)]]
    ref_p = [[10.22278855301975, 10.916282619416473, 12.182293454706306, 13.745416208713321, 15.50946687924238,
              17.693363362654498, 19.74557345084955, 22.525432627269492, 25.697251071130427, 28.99550728193967,
              33.07799441785211, 39.00378586433987, 49.67893177327907, 132.37526067025257, 234.64316877596258,
              348.7097695267014, 490.4466824373712, 660.0559213976551, 868.9508558685552, 1169.4294015942921,
              # 1681.419738721254, 2214.590162102342, 2792.3803696708565]),]
              ]]

    p, t = a.calc_equilibrium_curve(composition=[0.5, 0.5], ref_data=ref_p, temperature=ref_T)

    plot = Plot()
    plot.draw_plot(xdata=t, ydata=p, number_of_curves=1,
                   title="CO2-hydrate", xlabel="temperature", ylabel="pressure", logy=True)
    plot.draw_refdata(xref=ref_T, yref=ref_p, number_of_curves=1)

    # Kvamme (2019)
    ref_T = [[273.16, 274.17, 275.13, 276.15, 277.16, 278.17, 279.13, 280.14, 281.16, 282.17, 283.13, 284.14, 285.15, 286.17, 287.13, 288.14, 289.16, 290.00]]
    ref_nH = [[7.26, 7.24, 7.22, 7.20, 7.18, 7.16, 7.14, 7.11, 7.09, 7.07, 7.05, 6.69, 6.67, 6.64, 6.62, 6.59, 6.57, 6.55]]
    ref_dH = [[67.79, 67.24, 66.67, 66.08, 65.50, 64.91, 64.36, 63.77, 63.18, 62.59, 62.03, 60.96, 60.40, 59.86, 59.39, 58.96, 58.68, 58.55]]

    nH, rhoH, dH = a.calc_properties(pressure=p, temperature=t, composition=[[0.5, 0.5]], guest_idx=1)

    plot = Plot()
    plot.draw_plot(xdata=t, ydata=nH, number_of_curves=1,
                   title="CO2-hydrate", xlabel="temperature", ylabel="nH", logy=False)
    plot.draw_refdata(xref=ref_T, yref=ref_nH, number_of_curves=1)

if 1:
    """C1 sI equilibrium PT"""
    from dartsflash.libflash import CubicEoS, AQEoS, Ballard

    mix = Mixture(components=["H2O", "C1"], ions=["Na+", "Cl-"], name="H2O-CO2", np_max=2)
    a = HyFlash(mixture=mix)

    a.add_eos("SRK", CubicEoS(mix.comp_data, CubicEoS.SRK),
              initial_guesses=[InitialGuess.Yi.Wilson])
    a.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012,
                                          AQEoS.ion: AQEoS.Jager2003}))
    a.add_hydrate_eos("sI", Ballard(mix.comp_data, "sI"))

    a.init_flash(stabilityflash=False, eos=["AQ", "SRK"], initial_guess=[InitialGuess.Henry_AV])

    num_curves = 5
    ref_T = [[float(i) for i in range(273, 289)] + [291.86, 293.08, 293.46, 295.08, 295.94, 297.48, 297.58, 298.24, 298.68],
             [280.66, 286.00, 289.42, 291.71, 293.42, 293.51, 295.62, 296.30, 297.42, 298.42, 299.06],
             [279.16, 284.53, 287.50, 288.30, 289.23, 290.55, 292.15, 293.37, 294.58, 295.47, 296.03],
             [274.40, 280.25, 282.33, 284.67, 286.23, 287.47, 288.42, 289.21, 290.37, 291.00],
             [270.66, 275.22, 278.04, 279.20, 281.29, 282.39, 283.49, 284.36, 284.92, 285.76]
             ]
    ref_data = [[25.582, 28.259, 31.226, 34.519, 38.178, 42.251, 46.796, 51.878, 57.576, 63.982, 71.208, 79.389, 88.686, 99.293, 111.44, 125.41]
                + [201.90, 229.10, 242.30, 304.60, 342.80, 434.60, 432.50, 456.40, 504.50],
                [66.00, 137.10, 213.80, 281.80, 359.40, 348.40, 455.20, 487.50, 562.00, 632.50, 678.10],
                [75.10, 140.70, 233.80, 238.80, 271.90, 346.90, 418.50, 490.00, 576.30, 646.00, 715.60],
                [79.20, 143.20, 207.40, 291.20, 359.30, 429.30, 495.30, 564.70, 641.40, 705.60],
                [78.50, 149.40, 229.80, 281.80, 378.60, 422.60, 505.70, 588.50, 640.30, 713.00]
                ]
    molality = np.array([0., 2.001E-2, 3.611E-2, 5.994E-2, 8.014E-2]) * 55.509
    p = [[] for i in range(num_curves)]
    t = [[] for i in range(num_curves)]
    for i, m in enumerate(molality):
        z = np.array([0.5, 0.5])
        ni = mix.calculate_concentrations(z, mole_fractions=True,
                                          concentrations={"NaCl": m}, concentration_unit=ConcentrationUnits.MOLALITY)

        p[i], t[i] = a.calc_equilibrium_curve(composition=ni, ref_data=ref_data[i], temperature=ref_T[i],
                                              )

    plot = Plot()
    plot.draw_plot(xdata=t, ydata=p, number_of_curves=num_curves)
    plot.draw_refdata(xref=ref_T, yref=ref_data, number_of_curves=num_curves)
    plot.add_attributes(suptitle="C1-hydrate", ax_labels=["temperature", "pressure"])

    # nH, rhoH, dH = a.calc_properties(pressure=p, temperature=t, composition=Z, number_of_curves=num_curves)

if 0:
    """C1-C2 equilibrium Pressure"""
    from dartsflash.libflash import CubicEoS, AQEoS, Ballard

    mix = Mixture(components=["H2O", "C1", "C2"], name="H2O-C1-C2", np_max=2)
    a = HyFlash(mixture=mix)

    a.add_eos("SRK", CubicEoS(mix.comp_data, CubicEoS.SRK),
              initial_guesses=[InitialGuess.Yi.Wilson])
    a.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012,
                                          AQEoS.ion: AQEoS.Jager2003}))
    a.add_hydrate_eos("sI", Ballard(mix.comp_data, "sI"))
    a.add_hydrate_eos("sII", Ballard(mix.comp_data, "sII"))

    a.init_flash(stabilityflash=False, eos=["AQ", "SRK"], initial_guess=[InitialGuess.Henry_AV])

    num_curves = 1
    xC1 = [1e-5, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 0.96, 0.97, 0.98, 0.99, 1 - 1e-5]
    nx = len(xC1)
    ref_x = [xC1]
    ref_T = [[277.6] * nx]
    ref_data = [[8., 8.5, 9., 9.5, 10., 11., 12.5, 15., 17.5, 18.5, 20., 22., 26., 28., 30.5, 34., 39., 40.5]]

    Z = [[[] for i in range(nx)]]
    for i, xi in enumerate(xC1):
        Z[0][i][:] = [0.5, 0.5 * xi, 0.5 * (1.-xi)]

    psI, t = a.calc_equilibrium_curve(composition=Z, ref_data=ref_data, temperature=ref_T,
                                    number_of_curves=num_curves,  phase="sI"
                                    )
    psII, t = a.calc_equilibrium_curve(composition=Z, ref_data=ref_data, temperature=ref_T,
                                      number_of_curves=num_curves, phase="sII"
                                      )
    p = [psI[0], psII[0]]
    x = [xC1, xC1]

    plot = Plot()
    plot.draw_plot(xdata=x, ydata=p, number_of_curves=2, logy=True, xlim=[-0.01, 1.01], ylim=[7, 50],
                   title="C1-C2-hydrate", xlabel="x_C1", ylabel="pressure", datalabels=["sI", "sII"])
    plot.draw_refdata(number_of_curves=1, xref=ref_x, yref=ref_data)

if 0:
    """C1-CO2 equilibrium Pressure"""
    from dartsflash.libflash import CubicEoS, AQEoS, Ballard

    mix = Mixture(components=["H2O", "C1", "CO2"], name="H2O-C1-CO2", np_max=2)
    a = HyFlash(mixture=mix)

    a.add_eos("SRK", CubicEoS(mix.comp_data, CubicEoS.SRK),
              initial_guesses=[InitialGuess.Yi.Wilson])
    a.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012,
                                          AQEoS.ion: AQEoS.Jager2003}))
    a.add_hydrate_eos("sI", Ballard(mix.comp_data, "sI"))
    a.add_hydrate_eos("sII", Ballard(mix.comp_data, "sII"))

    a.init_flash(stabilityflash=False, eos=["AQ", "SRK"], initial_guess=[InitialGuess.Henry_AV])

    num_curves = 1
    xC1 = [1e-5, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 0.96, 0.97, 0.98, 0.99, 1 - 1e-5]
    nx = len(xC1)
    ref_x = [xC1]
    ref_T = [[277.6] * nx]
    ref_data = [[8., 8.5, 9., 9.5, 10., 11., 12.5, 15., 17.5, 18.5, 20., 22., 26., 28., 30.5, 34., 39., 40.5]]

    Z = [[[] for i in range(nx)]]
    for i, xi in enumerate(xC1):
        Z[0][i][:] = [0.5, 0.5 * xi, 0.5 * (1.-xi)]

    psI, t = a.calc_equilibrium_curve(composition=Z, ref_data=ref_data, temperature=ref_T,
                                    number_of_curves=num_curves,  phase="sI"
                                    )
    psII, t = a.calc_equilibrium_curve(composition=Z, ref_data=ref_data, temperature=ref_T,
                                      number_of_curves=num_curves, phase="sII"
                                      )
    p = [psI[0], psII[0]]
    x = [xC1, xC1]

    plot = Plot()
    plot.draw_plot(xdata=x, ydata=p, number_of_curves=2, logy=True, xlim=[-0.01, 1.01], ylim=[20, 80],
                   title="C1-CO2-hydrate", xlabel="x_C1", ylabel="pressure", datalabels=["sI", "sII"])
    # plot.draw_refdata(number_of_curves=1, xref=ref_x, yref=ref_data)

plt.show()
