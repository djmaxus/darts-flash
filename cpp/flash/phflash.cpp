#include <iostream>
#include <vector>
#include <cassert>
#include <numeric>

#include "dartsflash/global/ideal.hpp"
#include "dartsflash/flash/phflash.hpp"

#include <Eigen/Dense>

// Input is pressure p, specific enthalpy h_spec and composition z
PhFlash::PhFlash()
{
}

PhFlash::~PhFlash()
{

}

PhFlash::PhFlash(FlashParams& flashparams, int np_max_)
{
	this->flash_params = flashparams;

	this->nc = flash_params.nc;
	this->ni = flash_params.ni;
	this->ns = flash_params.ns;

   this->np_max = np_max_;

   this->z.resize(ns);
   this->eos.reserve(np_max);
   this->nu.reserve(np_max);
   this->X.reserve(np_max*ns);

	this->tol = flash_params.phflash_tol;

}

int PhFlash::evaluate(double p_, double h_spec_, std::vector<double>& z_)
{
	this->init(p_, h_spec_, z_);

	T_out = this->brent_method(T_min, T_max, T_init, tol, tol, 0);

	return error;
}

void PhFlash::init(double p_, double h_spec_, std::vector<double>& z_)
{
	p = p_;
	h_spec = h_spec_;
	z = z_;

	T_max = this->flash_params.T_max;
	T_min = this->flash_params.T_min;
	T_init = this->flash_params.T_init;

}

double PhFlash::fun(double T)
{
	// Initialize stability flash class
	StabilityFlash stab_flash(flash_params, np_max);

	// Perform PT-flash
	error = stab_flash.evaluate(p, T, z);

   // In case of error, return 1
   if (error)
   {
      return 1;
   }

	// Get data from PT-flash
   Flash::Results results = stab_flash.get_flash_results();
	X = results.X;
	nu = results.nu;
   np = static_cast<int>(nu.size());

   // Calculate total enthalpy of mixture
   double H_p = 0.;
	for(int j = 0; j < np; j++)
	{
		// Ideal and residual enthalpy
		double H_j = flash_params.eos_params[results.eos[j]].eos->H_TP(p, T, X, j*ns) * M_R;

      // H = sum_{p=1}^{np}{N_p*H_p}
		H_p += H_j * nu[j];
	}

   return H_p-h_spec;
}

double PhFlash::brent_method(double a, double b, double t, double tol_f, double tol_t, int printIter_flag)
{
   //****************************************************************************
   //
   //  Purpose:
   //
   //    The brent_method function uses the Brent method to find a root of a given
   //    function f within a given interval [a, b] using an initial estimate t.
   //
   //  Discussion:
   //
   //    The Brent method is an iterative root-finding algorithm that combines the
   //    bisection method,  secant method, and inverse quadratic interpolation to f
   //    ind a root of a given function. It is generally an robust algorithm without
   //    evaluating the derivatives.
   //
   //    The current algorithm is slightly modified based on Moncorg� (2022) so that
   //    only one function evaluation is needed to start the algorithm. After the
   //    modification, interval [a, b] should be well-defined to guarantee the root
   //    is bracketed.
   //
   //    Please refer to the original paper of Brent (1971) for other details:
   //    An algorithm with guaranteed convergence for finding a zero of a function.
   //
   //  Parameters:
   //
   //    Input, double a, b, two doubles representing the interval in which to
   //    search for the root.
   //
   //    Input, double t, doubles representing the initial estimate of the
   //    root in the interval [a, b].
   //
   //    Input, double tol_f, tol_t, two doubles representing the tolerances for
   //    the function and the root.
   //
   //    Input, int printIter_flag, one int representing the flag to output the
   //    result at each iteration.
   //
   //    Output, double b, one double representing an approximation of the root of
   //    f within the interval [a, b].
   //
   //****************************************************************************80

   int iter = 1;
   double fa = -1, fb = 1;
   double iter_type = 0;
   (void) iter_type;
   (void) printIter_flag;

   double ft = fun(t);

   // printIter(printIter_flag, ft, t, iter_type, iter);

   if (ft > 0)
   {
      b = t;
      fb = ft;
      fa = (-fb > -1.) ? -1. : -fb;
   }
   else
   {
      a = t;
      fa = ft;
      fb = (-fa < 1.) ? 1. : -fa;
   }

   if (std::fabs(fb) > tol_f)
   {
      // a is the previous value of b and [b, c] always contains the zero.
      double c = a;
      double fc = fa;
      double d = b - a; // the current step
      double e = b - a; // the step at the last step

      while (true)
      {
         if (std::abs(fb) / fb == std::abs(fc) / fc)  // Check if signs of f(b) and f(c) are opposite
         {
            c = a;
            fc = fa;
            d = b - a;
            e = b - a;
         }

         // Swap to insure f(b) is the smallest value so far.
         if (std::abs(fc) < std::abs(fb))
         {
            a = b;
            fa = fb;
            b = c;
            fb = fc;
            c = a;
            fc = fa;
         }

         double tol_m = 2 * std::numeric_limits<double>::epsilon() * std::pow(b, 2) + 1e-14;

         // Midpoint.
         double m = (c - b) * 0.5;

         if (std::abs(m) <= tol_m || std::abs(fb) <= tol_f || std::abs(m * 2) <= tol_t)
         {
            // printIter_flag = 2;
            // printIter(printIter_flag, fb, b, iter_type, iter);
            break; // Exit from the loop and the function here.
         }

         if (std::abs(e) < tol_m || std::abs(fa) <= std::abs(fb))
         {  // bisection
            d = m;
            e = m;
            iter_type = 1;
         }
         else
         {
            double pp, qq, rr;   // intermediate variables
            double s = fb / fa;
            if (a == c)
            {  // secant (interpolation)
               pp = 2 * m * s;
               qq = 1 - s;
               iter_type = 2;
            }
            else
            {  // extrapolation
               qq = fa / fc;
               rr = fb / fc;
               pp = s * (2 * m * qq * (qq - rr) - (b - a) * (rr - 1));
               qq = (qq - 1) * (rr - 1) * (s - 1);
               iter_type = 3;
            }

            if (pp > 0)
            {
               qq = -qq;
            }
            else
            {
               pp = -pp;
            }

            s = e;
            e = d;

            if (2 * pp < 3 * m * qq - std::abs(tol_m * qq) && pp < std::abs(0.5 * s * qq))
            {
               // good short step
               d = pp / qq;
            }
            else
            { // bisection
               d = m;
               e = m;
               iter_type = 1;
            }
         }

         a = b;
         fa = fb;

         if (std::abs(d) > tol_m)
         {
            b = b + d;
         }
         else if (m > 0)
         {
            b = b + tol_m;
         }
         else
         {
            b = b - tol_m;
         }

         fb = fun(b);

         iter++;
      }
   }

   return b;
}
